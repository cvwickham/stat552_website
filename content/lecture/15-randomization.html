---
title: "Randomization/Permutation tests"
subtitle: "ST552 Lecture 15"
author: "Charlotte Wickham"
date: "2019-02-13"
weeks: "6"
output: beamer_presentation
links:
  - ["lecture/15-annotated.pdf", Annotated Slides]
---



<div id="randomization-test" class="section level2">
<h2>Randomization Test</h2>
<p>The F-tests (and t-tests) rely on the Normal error assumption.</p>
<p>In a randomized experiment, the randomization provides a basis for inference (no i.i.d sampling from populations required) and results in <strong>randomization tests</strong>.</p>
<p>The same procedure can be used in observational studies, with the assumption that nature ran the experiment for you, i.e. it’s like units were assigned to values of the explanatory variables at random.</p>
<p>Some people call randomization tests used for observational data, <strong>permutation tests</strong>.</p>
</div>
<div id="your-turn" class="section level2">
<h2>Your Turn</h2>
<p>What are the key ingredients in an hypothesis test?</p>
</div>
<div id="hypothesis-testing" class="section level2">
<h2>Hypothesis Testing</h2>
</div>
<div id="randomization-test-1" class="section level2">
<h2>Randomization Test</h2>
<p><em>(Overall F-test example)</em></p>
<p><strong>Model:</strong> Randomized experiment</p>
<p><strong>Null:</strong> Treatments have no effect on response</p>
<p><strong>Test statistic:</strong> <em>(Up to us)</em> Let’s use overall regression F-statistic.</p>
<p><strong>Null distribution:</strong> Randomization distribution of the test statistic.</p>
</div>
<div id="the-randomized-experiment-model" class="section level2">
<h2>The randomized experiment model</h2>
<div class="columns">
<div class="column">
<p><span class="math inline">\(n\)</span> experimental units
<span class="math display">\[
u_1 \quad u_2 \quad \ldots \quad u_n 
\]</span></p>
</div><div class="column">
<p><span class="math display">\[
X = \begin{pmatrix}
1 &amp; x_{11} &amp; \ldots &amp; x_{1(p-1)} \\
\vdots &amp; \vdots &amp; \ddots &amp; \vdots \\
1 &amp; x_{n1} &amp; \ldots &amp; x_{n(p-1)} 
\end{pmatrix}
\]</span>

<span class="math display">\[
y = \begin{pmatrix}
y_1 \\
\vdots \\ 
y_n
\end{pmatrix}
\]</span>
<span class="math inline">\(y_i\)</span> the observed response for the the unit that was randomly assigned to the <span class="math inline">\(i\)</span>th row of the design matrix.</p>
</div>
</div>
</div>
<div id="example-growing-tomatoes" class="section level2">
<h2>Example: Growing tomatoes</h2>
<div class="columns">
<div class="column">
<p><span class="math inline">\(n\)</span> experimental units</p>
</div><div class="column">
<p><span class="math display">\[
X = \begin{pmatrix}
1 &amp; 1 &amp; 1 \\
1 &amp; 1 &amp; 0 \\
1 &amp; 1 &amp; 0 \\
1 &amp; 0 &amp; 0 \\
1 &amp; 0 &amp; 1 \\
1 &amp; 0 &amp; 1 
\end{pmatrix}
\]</span>

<span class="math display">\[
y = \begin{pmatrix}
8 \\
6 \\
2 \\
9 \\
7 \\
4 
\end{pmatrix}
\]</span></p>
</div>
</div>
</div>
<div id="null-distribution" class="section level2">
<h2>Null distribution</h2>
<p>If the null is true, treatments have no effect on response.</p>
<div class="columns">
<div class="column">
<p><span class="math display">\[
y = \begin{pmatrix}
y_1 \\
\vdots \\ 
y_n
\end{pmatrix}
\]</span></p>
</div><div class="column">
<p><span class="math inline">\(y_i\)</span> the observed response for the the unit that was randomly assigned to the <span class="math inline">\(i\)</span>th row of the design matrix.</p>
</div>
</div>
<p>If the null is true, I see the same set of <span class="math inline">\(y_i\)</span>, just in different order based on the output of my randomizing units to treatment.</p>
<p><strong>Null distribution:</strong> the distribution of the test-statistic for all permutations of <span class="math inline">\(y_i\)</span></p>
</div>
<div id="an-equally-likely-output-of-the-tomato-growing-experiment" class="section level2">
<h2>An equally likely output of the tomato growing experiment</h2>
<div class="columns">
<div class="column">
<p><span class="math inline">\(n\)</span> experimental units</p>
</div><div class="column">
<p><span class="math display">\[
X = \begin{pmatrix}
1 &amp; 1 &amp; 1 \\
1 &amp; 1 &amp; 0 \\
1 &amp; 1 &amp; 0 \\
1 &amp; 0 &amp; 0 \\
1 &amp; 0 &amp; 1 \\
1 &amp; 0 &amp; 1 
\end{pmatrix}
\]</span>

<span class="math display">\[
y = \begin{pmatrix}
6 \\
8 \\
2 \\
9 \\
7 \\
4 
\end{pmatrix}
\]</span></p>
</div>
</div>
</div>
<div id="the-null-distribution" class="section level2">
<h2>The null distribution</h2>
<p>Observed data gives overall F-statistic: 0.132</p>
<p>Equally likely outcomes under the null hypothesis:
<span class="math display">\[
\begin{pmatrix}
8 \\
6 \\
2 \\
9 \\
7 \\
4 
\end{pmatrix},
\begin{pmatrix}
6 \\
8 \\
2 \\
9 \\
7 \\
4 
\end{pmatrix},
\begin{pmatrix}
2 \\
6 \\
8 \\
9 \\
7 \\
4 
\end{pmatrix},
\begin{pmatrix}
6 \\
2 \\
8 \\
9 \\
7 \\
4 
\end{pmatrix},
\quad \text{ +716 other possibilities}
\]</span></p>
<p>Equally likely F-statistics under the null hypothesis:</p>
<p><span class="math display">\[
0.132, \,0.244, \,5.534, \,0.244 \hspace{2in}
\]</span></p>
</div>
<div id="the-null-distribution-1" class="section level2">
<h2>The null distribution</h2>
<p><img src="/lecture/15-randomization_files/figure-html/unnamed-chunk-5-1.png" width="576" /></p>
<p><span class="math inline">\(624/720 = 0.87\)</span></p>
</div>
<div id="faraway-galapagos" class="section level2">
<h2>Faraway: Galapagos</h2>
<p>In lab:</p>

<pre class="r"><code>library(faraway)
lmod_small &lt;- lm(Species ~ Nearest + Scruz, 
  data = gala)
lms &lt;- summary(lmod_small)
obs_fstat &lt;- lms$fstat[1]

nperms &lt;- 4000
fstats &lt;- numeric(nperms)
for (i in 1:nperms){
  lmods &lt;- lm(sample(Species) ~ Nearest + Scruz, 
    data = gala)
  fstats[i] &lt;- summary(lmods)$fstat[1]
}</code></pre>
</div>
<div id="in-practice" class="section level2">
<h2>In practice</h2>
<ul>
<li><p>Easiest to justify when you actually have a randomized experiment</p></li>
<li><p>The choice of test statistic can be important for useful performance and interpretation:</p>
<p>For example, if the treatments affect the variance of the response, not the means, using the overall F-stat may fail to reject the null (treatment has no effect) with high probability even when sample sizes are large.</p>
<p>Sometimes it’s reasonable to add an assumption on the alternative, i.e. treatments have an additive effect.</p></li>
</ul>
</div>
