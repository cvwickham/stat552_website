---
title: "Inference in regression: F-test"
subtitle: "ST552 Lecture 8"
author: "Charlotte Wickham"
weeks: "3"
date: "2019-01-25"
output: beamer_presentation
type: lecture
links:
  - ["worksheet/08-ftest-exercises.pdf", "Exercises"]
  - ["worksheet/08-ftest-exercises-solution.pdf", "Solutions in R"]
  - ["lecture/08-annotated.pdf", "Annotated slides"]
---



<div id="homework-1" class="section level2">
<h2>Homework #1</h2>
<p><a href="https://oregonstate.instructure.com/courses/1711326/pages/homework-solutions">Solutions on canvas</a></p>
<p>I graded the initial data analysis.</p>
<ul>
<li>Everyone was looking at the right things!</li>
<li>But, the writeups could use some improvement</li>
<li>HW #3 gets you to repeat this process on a different data set</li>
</ul>
</div>
<div id="homework-3" class="section level2">
<h2>Homework #3</h2>
<p>I’ve posted an example with some guidelines as <code>01-initial-data-analysis-report</code>, but I started from <code>01-initial-data-analysis-draft</code>.</p>
<p>Key things I’ll be looking for in HW #3:</p>
<ul>
<li>&lt; 2 pages (notice my draft is 10 pages, but report is only 1.5 pages)</li>
<li>you control what output/code is in the final version</li>
<li>plots are labelled and sized appropriately</li>
<li>narrative leads the reader through important findings</li>
</ul>
</div>
<div id="today" class="section level2">
<h2>Today</h2>
<ul>
<li>The F-test</li>
<li>Practice with F-tests</li>
</ul>
</div>
<div id="motivation" class="section level2">
<h2>Motivation</h2>
<p>t-tests on individual parameters only allow us to ask a limited number of questions.</p>
<p>To ask questions about more than one coefficient we need something more complicted.</p>
<p>F-tests do this by comparing nested models. In practice, the hard part is translating a scientific question in a comparison of two models.</p>
</div>
<div id="f-test" class="section level2">
<h2>F-test</h2>
<p>Let <span class="math inline">\(\Omega\)</span> denote a larger model of interest with <span class="math inline">\(p\)</span> parameters<br />
and <span class="math inline">\(\omega\)</span> a smaller model that represents some simplification of <span class="math inline">\(\Omega\)</span> with <span class="math inline">\(q\)</span> parameters.</p>
<p><strong>Intuition:</strong> If both models “fit” as well as each other, we should prefer the simpler model, <span class="math inline">\(\omega\)</span>. If <span class="math inline">\(\Omega\)</span> shows substantially better fit than <span class="math inline">\(\omega\)</span>, that suggests the simplification is not justified.</p>
<p>How do we measure fit? What is substantially better fit?</p>

</div>
<div id="f-statistic" class="section level2">
<h2>F-statistic</h2>
<p><span class="math display">\[
F = \frac{(\RSS{\omega} - \RSS{\Omega})/(p - q)}{\RSS{\Omega}/(n - p)}
\]</span></p>
<p>Null hypothesis: the simplification to <span class="math inline">\(\Omega\)</span> implied by the simpler model, <span class="math inline">\(\omega\)</span>.</p>
<p>Under the null hypothesis, the F-statistic has an F-distribution with <span class="math inline">\(p-q\)</span> and <span class="math inline">\(n-p\)</span> degrees of freedom.</p>
<p>Leads to tests of the form: reject <span class="math inline">\(H_0\)</span> for <span class="math inline">\(F &gt; F_{p-q, n-p}^{(\alpha)}\)</span>.</p>
<p>Deriving this fact is beyond this class (take Linear Models).</p>
</div>
<div id="example-overall-regression-f-test" class="section level2">
<h2>Example: Overall regression F-test</h2>
<p>The overall regression F-test asks if any predictors are related to the response.</p>
<p><strong>Full model:</strong> <span class="math inline">\(Y = X\beta + \epsilon, \quad \epsilon \sim N(0, \sigma^2 I)\)</span><br />
<strong>Reduced model:</strong> <span class="math inline">\(Y = \beta_0 + \epsilon\)</span></p>
<p><strong>Null hypothesis:</strong> <span class="math inline">\(H_0: \beta_1 = \beta_2 = \ldots = \beta_{p-1} = 0\)</span><br />
All the parameters (other than the intercept) are zero.</p>
<p><strong>Alternative hypothesis:</strong> At least one parameter is non-zero.</p>
<p><strong>Exercise</strong>: question #1 on handout</p>
</div>
<div id="section" class="section level2">
<h2></h2>
<p>If there is <strong>evidence against the null hypothesis</strong>:</p>
<ul>
<li>The null is not true, or</li>
<li>the null is true but we got unlucky, or</li>
<li>the full model isn’t true and the F-test is meaningless.</li>
</ul>
<p>If there is <strong>no evidence against the null hypothesis</strong>:</p>
<ul>
<li>The null is true, or</li>
<li>the null is false but we didn’t gather enough evidence to reject it, or</li>
<li>the full model isn’t true and the F-test is meaningless.</li>
</ul>
</div>
<div id="example-one-predictor" class="section level2">
<h2>Example: One predictor</h2>
<p><strong>Null hypothesis</strong>: <span class="math inline">\(\beta_j = 0\)</span></p>
<p>Equivalent to the t-test, reject null if <span class="math display">\[
|t_j| = \left|\frac{\hat{\beta_j}}{\SE{\hat{\beta_j}}}\right| &gt; t_{n-p}^{\alpha/2}
\]</span></p>
<p>In fact, in this case, <span class="math inline">\(F = t_j^2\)</span>.</p>
<p><strong>Exercise</strong>: questions #2 &amp; #3 on handout</p>
</div>
<div id="other-examples" class="section level2">
<h2>Other examples</h2>
<ul>
<li>More than one parameter</li>
<li>A subspace of the parameter space</li>
</ul>
<p><strong>Exercise:</strong> questions #4 &amp; #5 on handout</p>
</div>
<div id="we-cant-do-f-tests-when" class="section level2">
<h2>We can’t do F-tests when</h2>
<ul>
<li>we want to test non-linear hypotheses, e.g. <span class="math inline">\(H_0: \beta_j\beta_k = 1\)</span> (we might be able to make use of the Delta method, though)</li>
<li>we want to compare non-nested models (find an example on the handout)</li>
<li>the models fit use different data (most often comes up when a variable of interest has some missing values)</li>
</ul>
</div>
