---
title: "Bootstrap Confidence Intervals"
subtitle: "ST552 Lecture 13"
author: "Charlotte Wickham"
date: "2019-02-11"
weeks: "6"
output: beamer_presentation
links:
  - ["lecture/14-annotated.pdf", Annotated Slides]
---



<div id="motivation" class="section level2">
<h2>Motivation</h2>
<p>The inferences we’ve covered so far relied on our assumption of Normal errors:
<span class="math display">\[
\epsilon \sim N(0, \sigma^2 I_{n\times n})
\]</span></p>
<p>For example, we’ve seen under this assumption, the least squares estimates are also Normally distributed:
<span class="math display">\[
\hat{\beta} \sim N\left(\beta,\, \sigma^2 \left(X^{T}X \right)^{-1} \right)
\]</span></p>
<p><strong>If the errors aren’t truly Normally distributed, what distribution do the estimates have?</strong></p>
</div>
<div id="warm-up-your-turn" class="section level2">
<h2>Warm-up: Your Turn</h2>
<p>Imagine the errors are in fact <span class="math inline">\(t_{3}\)</span> distributed?</p>
<p>With your neighbour: <strong>design a simulation to understand the distribution of the least squares estimates.</strong></p>
</div>
<div id="section" class="section level2">
<h2></h2>
<!-- ## Simulation -->
<!-- To understand the sampling distribution of $\hat{\beta}$ we could use simulation.  -->
<!-- We *know* distribution of $\epsilon$, we assume some value for $X$ and $\beta$: -->
<!-- 1. Fix $X$ -->
<!-- 2. For $k = 1, \ldots, B$  -->
<!--     i. Simulate errors, $\epsilon_i \overset{i.i.d}\sim t_5$ -->
<!--     ii. Construct $y$, using the model, $y = X\beta + \epsilon$ -->
<!--     iii. Use least squares to find $\hat{\beta}^*_{(k)}$ -->
<!-- 3. Examine the distribution of $\hat{\beta}^*$ and compare to $\beta$.  -->
</div>
<div id="example-1.-fix-n-fix-x" class="section level2">
<h2>Example: 1. Fix n, fix X</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-4-1.png" width="576" height="3in" /></p>
</div>
<div id="example-1.-fix-beta-find-haty" class="section level2">
<h2>Example: 1. Fix <span class="math inline">\(\beta\)</span>, find <span class="math inline">\(\hat{y}\)</span></h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-5-1.png" width="576" height="3in" /></p>
</div>
<div id="example-2.-simulate-errors-find-y" class="section level2">
<h2>Example: 2. Simulate errors, find <span class="math inline">\(y\)</span></h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-7-1.png" width="576" height="3in" /></p>
</div>
<div id="example-3.-find-least-squares-line" class="section level2">
<h2>Example: 3. Find least squares line</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-8-1.png" width="576" height="3in" /></p>
</div>
<div id="example-4.-repeat-2.-and-3.-many-times" class="section level2">
<h2>Example: 4. Repeat #2. and #3. many times</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-9-1.png" width="576" height="3in" /></p>
</div>
<div id="example-examine-distribution-of-estimates" class="section level2">
<h2>Example: Examine distribution of estimates</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-10-1.png" width="384" height="2in" /><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-10-2.png" width="384" height="2in" /></p>
</div>
<div id="example-compared-to-theory" class="section level2">
<h2>Example: Compared to theory</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-12-1.png" width="384" height="2in" /><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-12-2.png" width="384" height="2in" /></p>
</div>
<div id="when-the-errors-arent-normal-clt" class="section level2">
<h2>When the errors aren’t Normal: CLT</h2>
<p>Think of our estimates like linear combinations of the errors. I.e. a sort of average of i.i.d random variables.<br />
Some version of the Central Limit Theorem will apply.</p>
<p>For large samples, even when the errors aren’t Normal,
<span class="math display">\[
\hat{\beta} \dot{\sim} N(\beta,\, \sigma^2 (X^TX)^{-1})
\]</span></p>
</div>
<div id="summary-so-far" class="section level2">
<h2>Summary so far</h2>
<p>If we knew the error distribution and true parameters we could use simulation to understand the sampling distribution the least squares estimates.</p>
<p>Simulation can also be used to demonstrate the CLT at work in regression.</p>
</div>
<div id="bootstrap-confidence-intervals" class="section level2">
<h2>Bootstrap confidence intervals</h2>
<p>In practice, with data in front of us, we don’t know the distribution of the errors (nor the true parameter values).</p>
<p>The bootstrap is one approach to estimate the sampling distribution of <span class="math inline">\(\hat{\beta}\)</span>, by using the simulation idea, and substituting in our best guesses for the things we don’t know.</p>
</div>
<div id="bootstrapping-regression" class="section level2">
<h2>Bootstrapping regression</h2>
<p><em>(Model based resampling)</em></p>
<p>0. Fit model and find estimates, <span class="math inline">\(\hat{\beta}\)</span>, and residuals, <span class="math inline">\(e_i\)</span></p>
<ol style="list-style-type: decimal">
<li>Fix <span class="math inline">\(X\)</span>,</li>
<li>For <span class="math inline">\(k = 1, \ldots, B\)</span>
<ol style="list-style-type: lower-roman">
<li>Generate errors, <span class="math inline">\(\epsilon^*_i\)</span> sampled with replacement from <span class="math inline">\(e_i\)</span></li>
<li>Construct <span class="math inline">\(y\)</span>, using the model, <span class="math inline">\(y = \hat{y} + \epsilon^*\)</span></li>
<li>Use least squares to find <span class="math inline">\(\hat{\beta}^*_{(k)}\)</span></li>
</ol></li>
<li>Examine the distribution of <span class="math inline">\(\hat{\beta}^*\)</span> and compare to <span class="math inline">\(\hat{\beta}\)</span></li>
</ol>
<p>One confidence interval for <span class="math inline">\(\beta_j\)</span> is the 2.5% and 97.5% quantiles of the distribution of <span class="math inline">\(\hat{\beta}_j^*\)</span>.<br />
<em>(Known as the Percentile method, there are other (better?) methods)</em>.</p>
</div>
<div id="example-faraway-galapagos-islands" class="section level2">
<h2>Example: Faraway Galapagos Islands</h2>
<p><em>(I’ll illustrate with simple linear regression, Faraway does multiple case in 3.6)</em></p>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-13-1.png" width="576" height="3in" /></p>
</div>
<div id="bootstrap-1.-find-hatbeta-haty-and-e_i." class="section level2">
<h2>Bootstrap: 1. Find <span class="math inline">\(\hat{\beta}\)</span>, <span class="math inline">\(\hat{y}\)</span>, and <span class="math inline">\(e_i\)</span>.</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-14-1.png" width="576" height="3in" /></p>
</div>
<div id="bootstrap-using-fixed-x-hatbeta-from-observed-data" class="section level2">
<h2>Bootstrap: Using fixed <span class="math inline">\(X\)</span>, <span class="math inline">\(\hat{beta}\)</span> from observed data</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-15-1.png" width="576" height="3in" /></p>
</div>
<div id="bootstrap-2.-resample-residuals-to-construct-bootstrapped-response" class="section level2">
<h2>Bootstrap: 2. Resample residuals to construct bootstrapped response</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-16-1.png" width="576" height="3in" /></p>
</div>
<div id="bootstrap-3.-fit-regression-model-to-bootstrapped-response" class="section level2">
<h2>Bootstrap: 3. Fit regression model to bootstrapped response</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-17-1.png" width="576" height="3in" /></p>
</div>
<div id="bootstrap-3.-repeat-2.-and-3.-many-times" class="section level2">
<h2>Bootstrap: 3. Repeat #2. and #3. many times</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-18-1.png" width="576" height="3in" /></p>
</div>
<div id="examine-distribution-of-estimates" class="section level2">
<h2>Examine distribution of estimates</h2>
<p><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-19-1.png" width="384" height="2in" /><img src="/lecture/14-bootstrap_files/figure-html/unnamed-chunk-19-2.png" width="384" height="2in" /></p>
</div>
<div id="high-level-bootstrap-idea" class="section level2">
<h2>High level: bootstrap idea</h2>
<p>We don’t know the distribution of the errors, but our best guess is probably the empirical c.d.f on the residuals.</p>
<p>Sampling from a random variable with a c.d.f. defined as the empirical c.d.f. of the residuals, boils down to sampling with replacement from residuals.</p>
</div>
<div id="limitations" class="section level2">
<h2>Limitations</h2>
<p>We might rely on bootstrap confidence intervals when we are worried about the assumption of Normal errors. But, there are limitations.</p>
<ul>
<li>We still rely on the assumption that the errors are independent and identically distributed.<br />
</li>
<li>Generally scaled residuals are used (residuals don’t have the same variance, more later)</li>
<li>An alternative bootstrap resamples the <span class="math inline">\((y_i, x_{i1}, \ldots, x_{ip})\)</span> vectors, i.e. resamples the rows of the data, a.k.a <em>resampling cases bootstrap</em>.</li>
</ul>
</div>
