---
title: "Problems with the error"
subtitle: "ST552 Lecture 19"
author: "Charlotte Wickham"
date: "2019-02-22"
output: beamer_presentation
weeks: "7"
links:
  - ["lecture/19-annotated.pdf", "Annotated Slides"]
---



<div id="today" class="section level2">
<h2>Today</h2>
<p>Problems with the errors</p>
<ul>
<li>Generalized Least Squares</li>
<li>Lack of fit F-tests</li>
<li>Robust regression</li>
</ul>
</div>
<div id="generalized-least-squares" class="section level2">
<h2>Generalized Least Squares</h2>
<p><span class="math display">\[ 
Y = X\beta + \epsilon
\]</span></p>
<ul>
<li><p>We have assumed <span class="math inline">\(\Var{\epsilon} = \sigma^2 I\)</span>, but what if we know <span class="math inline">\(\Var{\epsilon} = \sigma^2 \Sigma\)</span>, where <span class="math inline">\(\sigma^2\)</span> is unknown, but <span class="math inline">\(\Sigma\)</span> is known. For example, we know the form of the correlation and/or non-constant variance in the response.</p></li>
<li><p>The usual least squares estimates <span class="math inline">\(\hat{\beta}_{LS}\)</span> are unbiased, but they are no longer BLUE.</p></li>
</ul>
<p>Let <span class="math inline">\(S\)</span> be the matrix square root of <span class="math inline">\(\Sigma\)</span>, i.e. <span class="math inline">\(\Sigma = SS^T\)</span>.</p>
<p>Define a new regression equation by multiplying both sides by <span class="math inline">\(S^{-1}\)</span>:
<span class="math display">\[
\begin{aligned}
S^{-1}Y &amp;= S^{-1}X\beta + S^{-1}\epsilon \\
Y&#39; &amp;= X&#39; \beta + \epsilon&#39;
\end{aligned}
\]</span></p>
</div>
<div id="your-turn" class="section level2">
<h2>Your Turn</h2>
<p>Show <span class="math inline">\(\Var{\epsilon&#39;} = \Var{S^{-1}\epsilon} = \sigma^2 I\)</span>.
</p>
<p>Show the least squares estimates for the new regression equation reduce to:
<span class="math display">\[
\hat{\beta} = (X^T\Sigma^{-1} X)^{-1}X^T\Sigma^{-1} Y
\]</span>
</p>
</div>
<div id="section" class="section level2">
<h2></h2>
<ul>
<li><p>Can also show <span class="math inline">\(\Var{\beta} = (X^T\Sigma^{-1} X)^{-1} \sigma^2\)</span>.</p></li>
<li><p>The estimates: <span class="math inline">\(\hat{\beta} = (X^T\Sigma^{-1} X)^{-1}X^T\Sigma^{-1} Y\)</span> are known as  estimates.</p></li>
<li><p>In practice, <span class="math inline">\(\Sigma\)</span> might only be know up to a few parameters that also need to be estimated.</p></li>
</ul>
</div>
<div id="common-cases-of-gls" class="section level2">
<h2>Common cases of GLS</h2>
<ul>
<li><span class="math inline">\(\Sigma\)</span> defines a temporal or spatial correlation structure
</li>
<li><span class="math inline">\(\Sigma\)</span> defines a grouping structure
</li>
<li><span class="math inline">\(\Sigma\)</span> is diagonal and defines a weighting structure ()
</li>
</ul>
</div>
<div id="in-r" class="section level2">
<h2>In R</h2>
<pre class="r"><code>?lm # use weights argument
library(nlme)
?gls # has weights and/or correlation argument</code></pre>
</div>
<div id="oat-yields" class="section level2">
<h2>Oat yields</h2>
<blockquote>
<p>Data from an experiment to compare 8 varieties of oats. The growing area was heterogeneous and so was grouped into 5 blocks. Each variety was sown once within each block and the yield in grams per 16ft row was recorded.</p>
</blockquote>
<p><span class="math display">\[
\text{yield}_i = \beta_0 + \beta_1 \text{variety}_i + \epsilon_i \quad i=1, \ldots, 40
\]</span>
<span class="math display">\[
\Var{\epsilon_i} = \sigma^2, \quad \text{Cor}(\epsilon_i, \epsilon_j) = 
\begin{cases}
\rho, &amp; \text{block}_i = 
\text{if block}_j \\
0, &amp; \text{otherwise}
\end{cases}
\]</span>
</p>
<pre class="r"><code>library(nlme) 
fit_gls &lt;- gls(yield ~ variety, data = oatvar, 
  correlation = corCompSymm(form = ~ 1 | block))</code></pre>
</div>
<div id="oat-yields-in-r" class="section level2">
<h2>Oat yields in R</h2>

<pre class="r"><code>intervals(fit_gls)</code></pre>
<pre><code>## Approximate 95% confidence intervals
## 
##  Coefficients:
##                  lower  est.       upper
## (Intercept) 291.542999 334.4 377.2570009
## variety2     -4.903898  42.2  89.3038984
## variety3    -18.903898  28.2  75.3038984
## variety4    -94.703898 -47.6  -0.4961016
## variety5     57.896102 105.0 152.1038984
## variety6    -50.903898  -3.8  43.3038984
## variety7    -63.103898 -16.0  31.1038984
## variety8      2.696102  49.8  96.9038984
## attr(,&quot;label&quot;)
## [1] &quot;Coefficients:&quot;
## 
##  Correlation structure:
##          lower      est.     upper
## Rho 0.06596382 0.3959955 0.7493731
## attr(,&quot;label&quot;)
## [1] &quot;Correlation structure:&quot;
## 
##  Residual standard error:
##    lower     est.    upper 
## 33.39319 47.04679 66.28298</code></pre>
</div>
<div id="lack-of-fit-f-tests" class="section level2">
<h2>Lack of fit F-tests</h2>
<ul>
<li><p><span class="math inline">\(\hat{\sigma^2}\)</span> should be (if our model is specified correctly) an unbiased estimate of <span class="math inline">\(\sigma^2\)</span>.</p></li>
<li><p>A “model free” estimate of <span class="math inline">\(\sigma^2\)</span> is available if there are replicates (multiple observations at combinations of the explanatory values).</p></li>
<li><p>If our <span class="math inline">\(\hat{\sigma^2}\)</span> from our model is much bigger than the “model-free” estimate, we have evidence of .</p></li>
</ul>
</div>
<div id="in-practice" class="section level2">
<h2>In practice</h2>
<ul>
<li><p>Fit a saturated model. Compare saturated model to proposed model with an F-test. .</p></li>
<li><p><strong>Saturated</strong>: every combination of explanatory variables is allowed its own mean (i.e. every group of replicates is allowed its own mean). A model that includes every explantory as categorical and every possible interaction between variables.</p></li>
</ul>
</div>
<div id="example" class="section level2">
<h2>Example</h2>

<pre class="r"><code>data(corrosion, package = &quot;faraway&quot;)
lm_cor &lt;- lm(loss ~ Fe, data = corrosion)
lm_sat &lt;- lm(loss ~ factor(Fe), data = corrosion)
anova(lm_cor, lm_sat)</code></pre>
<pre><code>## Analysis of Variance Table
## 
## Model 1: loss ~ Fe
## Model 2: loss ~ factor(Fe)
##   Res.Df     RSS Df Sum of Sq      F   Pr(&gt;F)   
## 1     11 102.850                                
## 2      6  11.782  5    91.069 9.2756 0.008623 **
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1</code></pre>
<pre class="r"><code># significant lack of fit</code></pre>
</div>
<div id="section-1" class="section level2">
<h2></h2>
<p><img src="/lecture/19-errors_files/figure-html/unnamed-chunk-6-1.png" width="576" /></p>
</div>
<div id="robust-regression" class="section level2">
<h2>Robust regression</h2>
<p>Remember to define our least squares estimates we looked for <span class="math inline">\(\beta\)</span> to minimise
<span class="math display">\[
\sum_{i=1}^n \left(y_i - x_i^T\beta \right)^2
\]</span></p>
<p>In practice, since we are squaring residuals, observations with large residuals carry a lot of weight. For, robust regression, we want to downweight the observations with large residuals.</p>
<p>The idea of M-estimators is to extend this to the general situation where we want to find <span class="math inline">\(\beta\)</span> to minimise
<span class="math display">\[
\sum_{i=1}^n  \rho(y_i - x_i^T\beta)
\]</span>
where <span class="math inline">\(\rho()\)</span> is some function we specify.</p>
</div>
<div id="section-2" class="section level2">
<h2></h2>
<p><img src="/lecture/19-errors_files/figure-html/unnamed-chunk-7-1.png" width="384" /></p>
</div>
<div id="section-3" class="section level2">
<h2></h2>
<p><img src="/lecture/19-errors_files/figure-html/unnamed-chunk-8-1.png" width="384" /></p>
</div>
<div id="section-4" class="section level2">
<h2></h2>
<p><span class="math display">\[
\sum_{i=1}^n  \rho(y_i - x_i^T\beta)
\]</span></p>
<ul>
<li>Least squares: <span class="math inline">\(\rho(e_i) = e_i^2\)</span></li>
<li>Least absolute deviation, L<span class="math inline">\(_1\)</span> regression: <span class="math inline">\(\rho(e_i) = |e_i|\)</span></li>
<li>Huber’s method
<span class="math display">\[
\rho(e_i) = \begin{cases}
e_i^2/2 &amp; \text{if } |e_i| \le c\\
c|e_i| - c^2/2 &amp; \text{otherwise}
\end{cases}
\]</span></li>
<li>Tukey’s bisquare
<span class="math display">\[
\rho(e_i) =\begin{cases}
\frac{1}{6}(c^6 - (c^2 - e_i^2)^3) &amp; |e_i| \le c\\
0 &amp; \text{otherwise}
\end{cases}
\]</span></li>
</ul>
<p>The models are usually fit in an iterative process.</p>
</div>
<div id="least-trimmed-squares" class="section level2">
<h2>Least trimmed squares</h2>
<p>Minimise the smallest residuals
<span class="math display">\[
\sum_{i=1}^q e_{(i)}^2
\]</span>
where <span class="math inline">\(q\)</span> is some number smaller than <span class="math inline">\(n\)</span> and <span class="math inline">\(e_{(i)}\)</span> is the ith smallest residual.</p>
<p>One choice, <span class="math inline">\(q=\lfloor n/2 \rfloor + \lfloor (p+1)/2 \rfloor\)</span></p>
</div>
<div id="section-5" class="section level2">
<h2></h2>
<p>Annual numbers of telephone calls in Belgium
<img src="/lecture/19-errors_files/figure-html/unnamed-chunk-9-1.png" width="576" /></p>
</div>
