\documentclass[ignorenonframetext,]{beamer}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\newif\ifbibliography
\hypersetup{
            pdftitle={Inference in regression: F-test},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls

% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom

\AtBeginPart{
  \let\insertpartnumber\relax
  \let\partname\relax
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \let\insertsectionnumber\relax
    \let\sectionname\relax
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \let\insertsubsectionnumber\relax
  \let\subsectionname\relax
  \frame{\subsectionpage}
}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Inference in regression: F-test}
\subtitle{ST552 Lecture 8}
\author{Charlotte Wickham}
\date{2019-01-25}

\begin{document}
\frame{\titlepage}

\begin{frame}{Homework \#1}

\href{https://oregonstate.instructure.com/courses/1711326/pages/homework-solutions}{Solutions
on canvas}

I graded the initial data analysis.

\begin{itemize}
\tightlist
\item
  Everyone was looking at the right things!
\item
  But, the writeups could use some improvement
\item
  HW \#3 gets you to repeat this process on a different data set
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Homework \#3}

I've posted an example with some guidelines as
\texttt{01-initial-data-analysis-report}, but I started from
\texttt{01-initial-data-analysis-draft}.

Key things I'll be looking for in HW \#3:

\begin{itemize}
\tightlist
\item
  \textless{} 2 pages (notice my draft is 10 pages, but report is only
  1.5 pages)
\item
  you control what output/code is in the final version
\item
  plots are labelled and sized appropriately
\item
  narrative leads the reader through important findings
\end{itemize}

\end{frame}

\begin{frame}{Today}

\begin{itemize}
\tightlist
\item
  The F-test
\item
  Practice with F-tests
\end{itemize}

\end{frame}

\begin{frame}{Motivation}

t-tests on individual parameters only allow us to ask a limited number
of questions.

To ask questions about more than one coefficient we need something more
complicted.

F-tests do this by comparing nested models. In practice, the hard part
is translating a scientific question in a comparison of two models.

\end{frame}

\begin{frame}{F-test}

Let \(\Omega\) denote a larger model of interest with \(p\) parameters\\
and \(\omega\) a smaller model that represents some simplification of
\(\Omega\) with \(q\) parameters.

\textbf{Intuition:} If both models ``fit'' as well as each other, we
should prefer the simpler model, \(\omega\). If \(\Omega\) shows
substantially better fit than \(\omega\), that suggests the
simplification is not justified.

How do we measure fit? What is substantially better fit?

\vspace{1in}

\end{frame}

\begin{frame}{F-statistic}

\[
F = \frac{(\RSS{\omega} - \RSS{\Omega})/(p - q)}{\RSS{\Omega}/(n - p)}
\]

Null hypothesis: the simplification to \(\Omega\) implied by the simpler
model, \(\omega\).

Under the null hypothesis, the F-statistic has an F-distribution with
\(p-q\) and \(n-p\) degrees of freedom.

Leads to tests of the form: reject \(H_0\) for
\(F > F_{p-q, n-p}^{(\alpha)}\).

Deriving this fact is beyond this class (take Linear Models).

\end{frame}

\begin{frame}{Example: Overall regression F-test}

The overall regression F-test asks if any predictors are related to the
response.

\textbf{Full model:}
\(Y = X\beta + \epsilon, \quad \epsilon \sim N(0, \sigma^2 I)\)\\
\textbf{Reduced model:} \(Y = \beta_0 + \epsilon\)

\textbf{Null hypothesis:}
\(H_0: \beta_1 = \beta_2 = \ldots = \beta_{p-1} = 0\)\\
All the parameters (other than the intercept) are zero.

\textbf{Alternative hypothesis:} At least one parameter is non-zero.

\textbf{Exercise}: question \#1 on handout

\end{frame}

\begin{frame}{}

If there is \textbf{evidence against the null hypothesis}:

\begin{itemize}
\tightlist
\item
  The null is not true, or
\item
  the null is true but we got unlucky, or
\item
  the full model isn't true and the F-test is meaningless.
\end{itemize}

If there is \textbf{no evidence against the null hypothesis}:

\begin{itemize}
\tightlist
\item
  The null is true, or
\item
  the null is false but we didn't gather enough evidence to reject it,
  or
\item
  the full model isn't true and the F-test is meaningless.
\end{itemize}

\end{frame}

\begin{frame}{Example: One predictor}

\textbf{Null hypothesis}: \(\beta_j = 0\)

Equivalent to the t-test, reject null if \[
|t_j| = \left|\frac{\hat{\beta_j}}{\SE{\hat{\beta_j}}}\right| > t_{n-p}^{\alpha/2}
\]

In fact, in this case, \(F = t_j^2\).

\textbf{Exercise}: questions \#2 \& \#3 on handout

\end{frame}

\begin{frame}{Other examples}

\begin{itemize}
\tightlist
\item
  More than one parameter
\item
  A subspace of the parameter space
\end{itemize}

\textbf{Exercise:} questions \#4 \& \#5 on handout

\end{frame}

\begin{frame}{We can't do F-tests when}

\begin{itemize}
\tightlist
\item
  we want to test non-linear hypotheses, e.g.
  \(H_0: \beta_j\beta_k = 1\) (we might be able to make use of the Delta
  method, though)
\item
  we want to compare non-nested models (find an example on the handout)
\item
  the models fit use different data (most often comes up when a variable
  of interest has some missing values)
\end{itemize}

\end{frame}

\end{document}
