---
title: "Understanding regression models"
subtitle: "ST552 Lecture 10"
author: "Charlotte Wickham"
date: "2019-01-30"
weeks: "4"
output: beamer_presentation
links:
  - ["lecture/10-annotated.pdf", "Annotated notes"]
---



<div id="today" class="section level2">
<h2>Today</h2>
<ul>
<li>Lecture: mathematical strategies for understanding models</li>
<li>Lab: understanding models through visualization</li>
</ul>
</div>
<div id="inference-so-far" class="section level2">
<h2>Inference so far</h2>
<p>We’ve talked about the machinery to perform:</p>
<ul>
<li><p>t-tests, t-based confidence intervals for individual <span class="math inline">\(\beta\)</span>s and linear combinations of <span class="math inline">\(\beta\)</span>s</p></li>
<li><p>F-tests for hypotheses about many <span class="math inline">\(\beta\)</span>s</p></li>
</ul>
<p>But what can we do with this machinery?</p>
</div>
<div id="two-stages-in-understanding-regression-models" class="section level2">
<h2>Two stages in understanding regression models</h2>
<ol style="list-style-type: decimal">
<li><p>Understand a model in the context of a problem</p></li>
<li><p>Define a set of models to answer questions of interest</p></li>
</ol>
<p>Focus on #1 to gain intuition in how to approach #2.</p>
</div>
<div id="example" class="section level2">
<h2>Example</h2>
<p>LA Dodgers (baseball team) sometimes give out “bobbleheads” at home games. They are curious if this increases attendance at games.</p>
<p>81 games in 2012 season, 11 of which bobbleheads were given out.</p>
<p>Have measurements on:</p>
<ul>
<li>attendance at game (number of people)</li>
<li>day of the week the game was played</li>
<li>some other variables, that are probably important, but we will ignore for now</li>
</ul>
<p>(<em>Inspired by Chapter 2 in Modeling Techniques in Predictive Analytics: Business Problems and Solutions with R. Get data from <a href="http://www.informit.com/promotions/modeling-techniques-in-predictive-analytics-141183" class="uri">http://www.informit.com/promotions/modeling-techniques-in-predictive-analytics-141183</a> if you interested.</em>)</p>
</div>
<div id="a-model" class="section level2">
<h2>A model</h2>
<p><span class="math display">\[
\begin{aligned}
\text{attendance}_i &amp;= \beta_0 + \beta_1 1\{\text{bobblehead YES}\}_i + \\
&amp; \beta_2 1\{\text{Tue}\}_i +
\beta_3  1\{\text{Wed}\}_i+ 
\beta_4  1\{\text{Thu}\}_i+ \\
&amp; \beta_5  1\{\text{Fri}\}_i+
\beta_6  1\{\text{Sat}\}_i+
\beta_7  1\{\text{Sun}\}_i+ \epsilon_i
\end{aligned}
\]</span></p>
<p><strong>What does this model say about the relationship between attendance and whether bobbleheads are given out and day of the week</strong>?</p>
</div>
<div id="aside-some-terminology" class="section level2">
<h2>Aside some terminology</h2>
<p><em>variable</em>: a measurement made on the observational units.</p>
<p>E.g. bobblehead (yes/no) and day of week (mon/tue/wed/thu/fri/sat/sun).</p>
<p><em>term</em>: a column of the design matrix</p>
<p>E.g. <span class="math inline">\(\text{bobblehead}\)</span>, <span class="math inline">\(1\{\text{Fri}\}\)</span></p>
</div>
<div id="one-useful-strategy" class="section level2">
<h2>One useful strategy</h2>
<p>Ask about the <strong>effect</strong> of a <em>variable</em>?</p>
<p>What does the model say about the mean response when a variable is varied, holding all other variables constant.</p>
<p><strong>Categorical variable:</strong> Find the mean response for each level and compare.</p>
<p><strong>Continuous variable:</strong> Find the change in mean response if the variable increases by 1 unit.</p>
</div>
<div id="the-effect-of-bobbleheads" class="section level2">
<h2>The effect of bobbleheads</h2>
<p>The variable has two levels: yes, no</p>
<p>We’ll find <span class="math inline">\(\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,}\)</span> and <span class="math inline">\(\E{\text{attendence} | \text{bobblehead} = \text{No}}\)</span> then compare them.</p>
<p><span class="math display">\[
\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,} 
\]</span></p>
<p><span class="math display">\[
\E{\text{attendence} | \, \text{bobblehead} = \text{No}\,} 
\]</span></p>
</div>
<div id="the-effect-of-bobbleheads-1" class="section level2">
<h2>The effect of bobbleheads</h2>
<p><span class="math display">\[
\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,}  - \E{\text{attendence} | \, \text{bobblehead} = \text{No}\,} 
\]</span> </p>
<ul>
<li><p>For a fixed day of the week, the model predicts the mean attendance increases by <span class="math inline">\(\beta_1\)</span> when bobbleheads are given out.</p></li>
<li><p>The model predicts the mean attendance when bobbleheads are given is <span class="math inline">\(\beta_1\)</span>, more than when bobbleheads aren’t given, after accounting for day of the week.</p></li>
</ul>
</div>
<div id="relationship-to-inference" class="section level2">
<h2>Relationship to inference</h2>
<p>If <span class="math inline">\(\beta_1\)</span> is zero, then bobbleheads don’t have an effect on attendance.</p>
<p>We could answer the questions:</p>
<ul>
<li><p>Is the mean attendance higher when bobbleheads are given out? t-test on <span class="math inline">\(\beta_1 = 0\)</span>.</p></li>
<li><p>How much higher is the mean attendance higher when bobbleheads are given out? Confidence interval on <span class="math inline">\(\beta_1\)</span></p></li>
</ul>
<p>But this is an observational study, so we need to be careful with our language!<br />
OK “It is estimated distributing bobbleheads is <strong>associated</strong> with an increased mean attendance of XX”.<br />
Not OK “It is estimated distributing bobbleheads increases the mean attendance by XX”.</p>
</div>
<div id="your-turn-day-of-the-week" class="section level2">
<h2>Your turn: Day of the week</h2>
<p>What does model say about the expected attendance on Monday?</p>
<p>What does model say about the expected attendance on Tuesday?</p>
<p>What does model say about the expected attendance on Wednesday?</p>
</div>
<div id="your-turn-day-of-the-week-1" class="section level2">
<h2>Your turn: Day of the week</h2>
</div>
<div id="day-of-the-week-inference" class="section level2">
<h2>Day of the week: inference</h2>
<p>If <span class="math inline">\(\beta_2 = \beta_3 = \beta_4 = \beta_5 = \beta_6 = \beta_7\)</span> is zero, then day of the week doesn’t have an effect on attendance.</p>
<p>What tools could we use to answer the questions:</p>
<ul>
<li><p>Does day of the week have an effect on mean attendance (after accounting for the bobblehead promotion)?</p></li>
<li><p>How much does the mean attendance differ between Friday and Saturday?</p></li>
</ul>
</div>
<div id="an-example-with-a-continuous-variable" class="section level2">
<h2>An example with a continuous variable</h2>
<p>In lab today: <span class="math display">\[
\text{weight}_i = \beta_0 + \beta_11\{\text{male}\}_i + \beta_2\text{height}_i + \epsilon_i
\]</span></p>
<p>What is the effect of height?</p>
</div>
<div id="what-is-the-effect-of-height" class="section level2">
<h2>What is the effect of height?</h2>
<p>Compare <span class="math inline">\(\E{\text{weight}| \, \text{height} = h \,}\)</span> to <span class="math inline">\(\E{\text{weight}| \, \text{height} = h + 1 \,}\)</span>, holding other variables constant.</p>

</div>
<div id="interactions" class="section level2">
<h2>Interactions</h2>
<p><strong>Interactions</strong>: describe situations where the effect of one variable depends on the level of another explanatory variable.</p>
<p>E.g. <span class="math display">\[
\text{weight}_i = \beta_0 + \beta_11\{\text{male}\}_i + \beta_2\text{height}_i + \beta_3 \left(1\{\text{male}\} \times \text{height}\right)_i + \epsilon_i
\]</span></p>
<p>The same strategy will work.</p>
</div>
