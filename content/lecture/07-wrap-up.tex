\documentclass[ignorenonframetext,]{beamer}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\newif\ifbibliography
\hypersetup{
            pdftitle={Some details to tidy up},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls

% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom

\AtBeginPart{
  \let\insertpartnumber\relax
  \let\partname\relax
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \let\insertsectionnumber\relax
    \let\sectionname\relax
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \let\insertsubsectionnumber\relax
  \let\subsectionname\relax
  \frame{\subsectionpage}
}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Some details to tidy up}
\subtitle{ST552 Lecture 7}
\author{Charlotte Wickham}
\date{2019-01-23}

\begin{document}
\frame{\titlepage}

\begin{frame}{Summary of last week}

For the linear regression model \[
Y = X\beta + \epsilon
\] where \(\E{\epsilon} = 0_{n\times 1}\),
\(\Var{\epsilon} = \sigma^2 I_n\), and the matrix \(X_{n \times p}\) is
fixed with rank \(p\).

The least squares estimates are \[
\hat{\beta} = (X^TX)^{-1}X^TY
\]

Furthermore, the least squares estimates are BLUE, and \[
\E{\hat{\beta}} = \beta, \qquad \Var{\hat{\beta}} = \sigma^2 (X^TX)^{-1}
\]

We have not used any Normality assumptions to show these properties.

\end{frame}

\begin{frame}{Today}

\begin{itemize}
\item
  Verify:

  \[
  \E{\hat{\sigma}^2} = \E{\tfrac{1}{n-p}\sum_{i=1}^n{e_i^2}} = \sigma^2
  \]
\item
  Add Normal assumption to get inference on regression coefficents.
\end{itemize}

\end{frame}

\begin{frame}{Go over the estimation of \(\sigma\)}

\textbf{Strategy}: Write \(e_i^2\) as a linear combination of
uncorrelated variables, \(\epsilon_i\).

\vspace{2in}

\end{frame}

\begin{frame}{Write correlated residuals as combination of uncorrelated
errors}

\textbf{Claim}:

\[
||e||^2 = \epsilon^{T}(I - H)\epsilon
\]

\textbf{Your turn at home}:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Show \((I-H)\epsilon = e\). Hint: substitute
  \(\epsilon = Y - X\beta\), expand and use properties of \(H\).
\item
  Show \(||e||^2 = e^Te = \epsilon^T(I-H)\epsilon\). Hint: substitute in
  \(e = (I-H)\epsilon\) from above and use properties of \((I - H)\).
\end{enumerate}

\end{frame}

\begin{frame}{Find expected value of \(||e||^2\) in terms of
\(\text{trace}(I-H)\)}

\textbf{Show}
\(\E{\epsilon^T(I-H)\epsilon} = \sigma^2 \text{trace}(I-H)\)

\textbf{Hint} \[
x^TAx = \sum_{i = 1}^n\sum_{j = 1}^n x_i x_j A_{ij}
\] where \[
x = \left(x_1, x_2, \ldots, x_n \right)^T, \quad A = \begin{pmatrix}
A_{11}& A_{12}& \ldots \\
A_{21}& A_{22}& \ldots \\
\vdots & & \end{pmatrix}_{n\times n}
\]

\end{frame}

\begin{frame}{Find expected value of \(||e||^2\) in terms of
\(\text{trace}(I-H)\)}

\[
\E{\epsilon^T(I-H)\epsilon} = \phantom{\hspace{3in}}
\] \vspace{3in}

\end{frame}

\begin{frame}{Find \(\text{trace}(I-H)\)}

Show \[\text{trace}(I-H)=n-p\]

Hint: \[
\begin{aligned}
\text{trace}(A + B) &= \text{trace}(A) + \text{trace}(B) \\
\text{trace}(AB) &= \text{trace}(BA)
\end{aligned}
\]

\[
\text{trace}(I-H) = \phantom{aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa}
\] \vspace{2in}

\end{frame}

\begin{frame}{Put it all together}

\[
\E{\hat{\sigma}^2} = \phantom{\hspace{3in}}
\]

\vspace{2in}

\end{frame}

\section{Inference on the regression
coefficients}\label{inference-on-the-regression-coefficients}

\begin{frame}{Normality assumption}

Assume \(\epsilon \sim N(0, \sigma^2 I)\).

Important reminders:

\begin{itemize}
\item
\item
\end{itemize}

Leads to: \[
Y \sim N(\qquad, \qquad)
\]

\[
\hat{\beta} \sim N(\qquad, \quad \qquad)
\]

\end{frame}

\begin{frame}{Inference on individual parameters}

With the addition of the Normal assumption, it can be shown that

\[
\frac{\hat{\beta_j} - \beta_j}{SE(\hat{\beta_j})} \sim t_{n-p}
\]

leads to the usual construction of tests and confidence intervals for
single parameters.

\end{frame}

\begin{frame}{Exercises}

See handout.

\end{frame}

\end{document}
