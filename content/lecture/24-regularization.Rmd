---
title: "Regularization"
subtitle: "ST552 Lecture 24"
author: "Charlotte Wickham"
date: "2019-03-06"
output: beamer_presentation
weeks: "9"
links:
  - ["lecture/24-annotated.pdf", Annotated Slides]
---

```{r, include=FALSE}
knitr::opts_chunk$set(message = FALSE, results = "hide", warning = FALSE, fig.width = 6, fig.height = 4, echo = FALSE)
set.seed(123)
library(tidyverse)
data(meatspec, package = "faraway")
ind <- sample(nrow(meatspec), size = 172)
trainmeat <- meatspec[ind, ]
testmeat <- meatspec[-ind, ]
```

## What to expect on the final?

* Study guide posted along with last year's final.

Three questions worth roughly equal amounts

1. **Calculations, inference and interpretation.**  Like Q2 and Q3 on midterm: t-tests, F-tests, confidence intervals, prediction intervals etc.  Know your formulas, how to use them and how to interpret the results.

2. **Assumptions and diagnostics.**  Examine plots, identify problems, discuss the consequences of the problem, suggest remedies, suggest ways to verify if the suggestions worked.  Or suggest ways to diagnose certain problems.

3.  Everything else, at a conceptual level.

## Lab in Week 10

Some options:

* Revisit a topic (or more depth on a topic).  Which topic?
* Exam review session
* No lab

## Today

* The Bias-Variance tradeoff
* Regularized regression: lasso and ridge


## Bias-Variance tradeoff

For estimates, the mean squared error of an estimate can be broken down into bias and variance terms:
$$
\begin{aligned}
MSE(\hat{\theta}) & = \E{(\hat{\theta} - \theta)^2} =
\E{\left(\hat{\theta} - \E{\hat{\theta}}\right)^2} + \left(\E{\hat{\theta}} - \theta\right)^2 \\
& = \Var{\hat{\theta}} + \text{Bias}\left(\hat{\theta}\right)^2
\end{aligned}
$$

Often in statistics, we focus on estimates that are unbiased (so the second term is zero), and focus on minimising the variance term.

You might argue that you are willing to introduce a little bias, if it reduces the variance enough to reduce the overall mean squared error in the estimate. 

## 
 
There is a similar breakdown for the mean square error in prediction.  Let $\hat{f}(X)$ indicate the regression model for predicting an observation with explanantory values $X$.  

If the true data is generated according to $Y = f(X) + \epsilon$, where $\E{\epsilon} = 0$ and $\Var{\epsilon}= \sigma^2$, then the MSE for a point $x_0$:
$$
\begin{aligned}
&MSE(\hat{f}(X)| X = x_0)  =  \\
\quad &\E{(Y - \hat{f}(X))^2 | X = x_0} \\
\quad &=
\E{\left(\hat{f}(x_0) - \E{\hat{f}(x_0)}\right)^2} + \E{\left(Y - \E{\hat{f}(x_0)}\right)}^2 \\
\quad &= \Var{\hat{f}(x_0)} + \text{Bias}\left(\hat{f}(x_0)^2\right) + \sigma^2
\end{aligned}
$$

Bias captures how far our predictions are from the true mean on average (over repeated samples).

Variance captures how much our predictions vary (over repeated samples).

## Simulated Example

```{r, cache=TRUE}
# true regression function has cubic fit
n <- 100
df <- data.frame(x = runif(n))
true_func <- function(x){
  1 - 0.2*x - (x - 0.5)^2 + 2*(x - 0.25)^3
}
df$y <- with(df, true_func(x) + rnorm(n, sd = 0.04)) 

fit_poly <- function(order){
  df$y <- with(df, true_func(x) + rnorm(n, sd = 0.04)) 
  fit <- lm(y ~ poly(x, order), data = df)
  data.frame(x = df$x, pred = predict(fit), order = order)
}

fits <- map_dfr(1:10, 
  function(order) rerun(100, fit_poly(order = order)) %>% bind_rows(.id = ".n"))
```
```{r}
df %>% 
  ggplot(aes(x, y)) +
  geom_point()
```

## High bias, low variance

```{r}
fits %>%  
  filter(order == 1) %>% 
  ggplot(aes(x, y)) +
    geom_point(data = df) +
    geom_line(aes(y = pred, group = .n), alpha = I(0.2))
```

## Low bias, low variance

```{r}
fits %>%  
  filter(order == 3) %>% 
  ggplot(aes(x, y)) +
    geom_point(data = df) +
    geom_line(aes(y = pred, group = .n), alpha = I(0.2))
```

## Low bias, high variance

```{r}
fits %>%  
  filter(order == 10) %>% 
  ggplot(aes(x, y)) +
    geom_point(data = df) +
    geom_line(aes(y = pred, group = .n), alpha = I(0.2))
```

## Bias Variance tradeoff

```{r, fig.height=3}
metrics <- fits %>% 
  mutate(
    y = true_func(x),
    error = y - pred
  ) %>% 
  group_by(order, x) %>% 
  summarise(
    bias = mean(error),
    variance = var(error),
    mse = mean(error^2)
  ) %>% 
  summarise(
    `Bias^2` = mean(bias^2),
    `Variance` = mean(variance),
    `MSE` = mean(mse)
  ) %>% 
  gather(-order, key = "metric", value = "value")

metrics %>% 
  ggplot(aes(order, value)) +
  geom_point(aes(color = metric)) + 
  geom_line(aes(color = metric), linetype = "dashed") +
  scale_color_brewer("", palette = "Dark2") +
  labs(x = "Order of polynomial", y = "Value")
```


In general, more complex models decrease bias and increase variance. We hunt for the sweet spot where MSE is minimized.  

\scriptsize
There aren't nice partitions into bias and variance for other metrics, but the pattern is usually the same.  Increasing complexity only improves performance to a point, then it decreases performance.

## Regularized regression

One approach that introduces bias into the coefficient estimates, is regularized (a.k.a. penalized) regression.  Instead of minimising
$$
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2
$$
minimise
$$
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2 + \lambda \sum_{j = 1}^{p} f(\beta_j)
$$
When $f(\beta_j) = \beta_j^2$, the method is called **ridge regression**, and when $f(\beta_j) = |\beta_j|$ the method is called **lasso**.

**The general idea:** the first term rewards good fit to the data, the second penalizses for large values on the coefficients.

The result: estimates shrink toward zero (introducing bias) and have smaller variance.

## Alternative view

You can also view ridge and lasso as constrained minimisation, where we minimise
$$
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2
$$
subject to the constraints
$$
\begin{aligned}
\sum_{j = 1}^{p} \beta_j^2 \le t \quad \text{for ridge} \\
\sum_{j = 1}^{p} |\beta_j| \le s \quad \text{for lasso}
\end{aligned}
$$

## Ridge estimates

```{r}
library(MASS)
fit_ridge <- lm.ridge(fat ~ ., trainmeat, 
  lambda = seq(0, 5e-8, len = 21))
matplot(fit_ridge$lambda, coef(fit_ridge), 
  type = "l", xlab = expression(lambda),
  ylab = expression(hat(beta)), col = 1)
```

## Lasso estimates

```{r}
library(lars)
fit_lasso <- lars(as.matrix(trainmeat[ , -101]), trainmeat$fat)
plot(fit_lasso, plottype = c("coefficients"))
```

## Finding the tuning parameter

\scriptsize
```{r, echo = TRUE}
cvout <- cv.lars(as.matrix(trainmeat[ , -101]), trainmeat$fat)
```

## Tuning parameter

\scriptsize
```{r, echo = TRUE, results="markup"}
(best_s <- cvout$index[which.min(cvout$cv)])
```

RMSE for this value:
```{r, echo = TRUE, results="markup"}
testx <- as.matrix(testmeat[,-101])

predlars <- predict(fit_lasso, testx, s=best_s,
  mode="fraction")
sqrt(mean((testmeat$fat - predlars$fit)^2))
```

## Estimated coefficicents

```{r, results="markup"}
predlars <- predict(fit_lasso, s = best_s, type = "coef", mode = "fraction")
plot(predlars$coef, type = "h", ylab = "Coefficient")
sum(predlars$coef != 0)
```

## Key points

* Regularized/Penalized regression models have a tuning parameter that controls the degree of penalization/shrinkage
* The tuning parameter may be chosen to optimize some kind of criterion
* Lasso estimates can be exactly zero (so it performs model selection as well)



