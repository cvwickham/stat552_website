---
title: "Transformations"
subtitle: "ST552 Lecture 20"
author: "Charlotte Wickham"
date: "2019-02-25"
output: beamer_presentation
weeks: "8"
links:
  - ["lecture/20-annotated.pdf", "Annotated Slides"]
---



<div id="transforming-the-response" class="section level2">
<h2>Transforming the response</h2>
<p><strong>Motivation</strong>: generally we are hunting for a transformation that makes the relationship simpler.</p>
<p>We might believe the relationship with the explanatories is linear only after a transformation of the response,
<span class="math display">\[
\E{g(Y)} = X\beta
\]</span>
we might also hope on this transformed scale
<span class="math display">\[
\Var{g(Y)} = \sigma^2 I
\]</span>
What is a good <span class="math inline">\(g\)</span>?</p>
</div>
<div id="transforming-the-predictor" class="section level2">
<h2>Transforming the predictor</h2>
<p><strong>Motivation</strong>: we acknowledge that straight lines might not be appropriate and want to estimate something more flexible.</p>
<p>For example, we believe the model is something like
<span class="math display">\[
Y = f_1(X_1) + f_2(X_2) + \ldots + f_p(X_p) + \epsilon
\]</span>
or even,
<span class="math display">\[
Y = f(X_1, X_2, \ldots, X_p) + \epsilon
\]</span></p>
<p>We are generally interested in estimating <span class="math inline">\(f\)</span>.</p>
</div>
<div id="transforming-the-response-1" class="section level1">
<h1>Transforming the response</h1>
<div id="transforming-the-response-2" class="section level2">
<h2>Transforming the response</h2>
<p>In general, transformations make interpretation harder.</p>
<p>We usually want to make statements about the response (not the transformed) response.</p>
<p>Predicted values are easily back-transformed, as well as the endpoints of confidence intervals.</p>
<p>Parameters often <strong>do not</strong> have nice interpretations on the backtransformed scale.</p>
</div>
<div id="special-case-log-transformed-response" class="section level2">
<h2>Special case: Log transformed response</h2>
<p>Our fitted model on the transformed scale, predicts:
<span class="math display">\[
\hat{\log{y_i}} = \hat{\beta}_0 + \hat{\beta}_1x_{i1} + \ldots + \hat{\beta}_{p}x_{ip}
\]</span>
If we backtransform, by taking exponential of both sides,
<span class="math display">\[
\hat{y_i} = \exp{\hat{\beta}_0}\exp{(\hat{\beta}_1x_{i1})}\ldots\exp{(\hat{\beta}_{p}x_{ip})}
\]</span></p>
<p>So, an increase in <span class="math inline">\(x_1\)</span> of one unit, will result in the predicted response being multiplied by <span class="math inline">\(exp(\beta_1)\)</span>.</p>
</div>
<div id="section" class="section level2">
<h2></h2>
<p>If we are willing to assume that on the transformed scale the distribution of the response is <strong>symmetric</strong>,
<span class="math display">\[
\text{Median}({log(Y)}) = \E{log(Y)} = \beta_0 + \beta_1 x_1 + \ldots + \beta_{p}x_p
\]</span>
and back-transforming gives,
<span class="math display">\[
\exp(\text{Median}({log(Y)})) =  \text{Median}(Y) = \exp(\beta_0)\exp(\beta_1 x_1)\ldots\exp(\beta_{p}x_p)
\]</span></p>
<p>So, an increase in <span class="math inline">\(x_1\)</span> of one unit, will result in the median response being multiplied by <span class="math inline">\(exp(\beta_1)\)</span>.</p>
<p>(For monotone functions <span class="math inline">\(\text{Median}({f(Y)}) = f(\text{Median}(Y))\)</span>,</p>
<p>but <span class="math inline">\(\text{E}({f(Y)}) \ne f(E(Y))\)</span> in general)</p>
</div>
<div id="example" class="section level2">
<h2>Example</h2>

<pre class="r"><code>library(faraway)
data(case0301, package = &quot;Sleuth3&quot;)
head(case0301, 2)</code></pre>
<pre><code>##   Rainfall Treatment
## 1   1202.6  Unseeded
## 2    830.1  Unseeded</code></pre>
<pre class="r"><code>sumary(lm(log(Rainfall) ~ Treatment, data = case0301))</code></pre>
<pre><code>##                   Estimate Std. Error t value Pr(&gt;|t|)
## (Intercept)        5.13419    0.31787 16.1519  &lt; 2e-16
## TreatmentUnseeded -1.14378    0.44953 -2.5444  0.01408
## 
## n = 52, p = 2, Residual SE = 1.62082, R-Squared = 0.11</code></pre>
<p>It is estimated the median rainfall for unseeded clouds is 0.32 times the median rainfall for seeded clouds.</p>
<p>(Assuming log rainfall is symmetric around its mean)</p>
</div>
<div id="box-cox-transformations" class="section level2">
<h2>Box-Cox transformations</h2>
<p>Assume, the response is positive, and
<span class="math display">\[
g(Y) = X\beta + \epsilon, \quad \epsilon \sim N(0, \sigma^2 I)
\]</span>
and that <span class="math inline">\(g\)</span> is of the form
<span class="math display">\[
g_{\lambda}(y) = \begin{cases}
\frac{y^\lambda - 1}{\lambda} &amp; \lambda \ne 0 \\
\log(y) &amp; \lambda  = 0
\end{cases}
\]</span></p>
<p>Estimate <span class="math inline">\(\lambda\)</span> with maximum likelihood.</p>
<p>For prediction, pick <span class="math inline">\(\lambda\)</span> as the MLE.</p>
<p>For explanation, pick “nice” <span class="math inline">\(\lambda\)</span> within 95% CI.</p>
</div>
<div id="example-1" class="section level2">
<h2>Example:</h2>

<pre class="r"><code>library(MASS)
data(savings, package = &quot;faraway&quot;)
lmod &lt;- lm(sr ~ pop15 + pop75 + dpi + ddpi, data = savings)
boxcox(lmod, plotit = TRUE)</code></pre>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-3-1.png" width="672" /></p>
</div>
<div id="your-turn" class="section level2">
<h2>Your turn:</h2>

<pre class="r"><code>data(gala, package = &quot;faraway&quot;)
lmod &lt;- lm(Species ~ Area + Elevation + Nearest + Scruz + Adjacent, 
  data = gala)
boxcox(lmod, plotit = TRUE)</code></pre>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-4-1.png" width="672" /></p>
</div>
</div>
<div id="transforming-the-predictors" class="section level1">
<h1>Transforming the predictors</h1>
<div id="transforming-the-predictors-1" class="section level2">
<h2>Transforming the predictors</h2>
<p>A very general approach is to let the function for each explanatory be represented by a finite set of basis functions. For example, for a single explanatory, X,
<span class="math display">\[
f(X) = \sum_{k = 1}^{K} \beta_k f_k(X)
\]</span>
where <span class="math inline">\(f_k\)</span> are the known basis functions, and <span class="math inline">\(\beta_k\)</span> the unknown basis coefficients.</p>
<p>Then
<span class="math display">\[
\begin{aligned}
y_i &amp;= f(X_i) + \epsilon_i  \\ 
y_i &amp;= \beta_1 f_1(X_i) + \ldots + \beta_K f_K(X_i) + \epsilon_i \\
Y &amp;= X&#39;\beta + \epsilon
\end{aligned}
\]</span>
where the columns of <span class="math inline">\(X&#39;\)</span> are <span class="math inline">\(f_1(X)\)</span>, <span class="math inline">\(f_2(X)\)</span> and we can find the <span class="math inline">\(\beta\)</span> with the usual least squares approach.</p>
</div>
<div id="your-turn-1" class="section level2">
<h2>Your turn:</h2>
<p>What are the columns in the design matrix for the model:</p>
<p><span class="math display">\[
y_i = \beta_1 1\{X_i &lt; 5\} + \beta_2 1\{X_i \ge 5\} + \beta_3 X_i 1\{X_i &lt; 5\} + \beta_4 X_i 
1\{X_i \ge 5\} + \epsilon_i
\]</span></p>
<p>where <span class="math inline">\(X_i = i, i = 1, \ldots, 10\)</span>?</p>
<p>What do the functions <span class="math inline">\(f_k(), k = 1, \ldots, 4\)</span> look like?</p>
</div>
<div id="section-1" class="section level2">
<h2></h2>
</div>
<div id="example-subset-regression" class="section level2">
<h2>Example: subset regression</h2>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-5-1.png" width="576" /></p>
</div>
<div id="example-subset-regression-1" class="section level2">
<h2>Example: subset regression</h2>
<pre class="r"><code>cut &lt;- 35
X &lt;- with(savings, cbind(
  as.numeric(pop15 &lt; cut), 
  as.numeric(pop15 &gt;= cut),
  pop15 * (pop15 &lt; cut),
  pop15 * (pop15 &gt;= cut)))

lmod &lt;- lm(sr ~ X - 1, 
  data = savings)
summary(lmod)</code></pre>
</div>
<div id="example-subset-regression-2" class="section level2">
<h2>Example: subset regression</h2>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-7-1.png" width="576" /></p>
</div>
<div id="broken-stick-regression" class="section level2">
<h2>Broken stick regression</h2>
<p>Broken stick:</p>
<p><span class="math display">\[
f_1(x) = \begin{cases}
c - x &amp; \text{if } x &lt; c \\ 
0 &amp; \text{otherwise } 
\end{cases}
\]</span>
<span class="math display">\[
f_2(x) = \begin{cases}
0 &amp; \text{if } x &lt; c \\ 
x - c &amp; \text{otherwise } 
\end{cases}
\]</span></p>
<p><span class="math display">\[
y_i = \beta_0 + \beta_1 f_1(X_i) + \beta_2 f_2(X_i) +  \epsilon_i
\]</span></p>

</div>
<div id="example-broken-stick-regression" class="section level2">
<h2>Example: broken stick regression</h2>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-8-1.png" width="576" /></p>
</div>
<div id="polynomials" class="section level2">
<h2>Polynomials</h2>
<ul>
<li><p><strong>Polynomials</strong>:
<span class="math display">\[
  f_k(x) = x^k, \quad k = 1, \ldots, K
  \]</span></p></li>
<li><p><strong>Orthogonal polynomials</strong></p></li>
<li><p><strong>Response surface</strong>, of degree <span class="math inline">\(d\)</span>
<span class="math display">\[
  f_{kl}(x, z) = x^{k}z^{l}, \quad k,l \ge 0 \text{ s.t. } k + l = d
  \]</span></p></li>
</ul>
</div>
<div id="cubic-splines" class="section level2">
<h2>Cubic Splines</h2>
<p>Knots: <em>0</em>, <em>0</em>, <em>0</em>, <em>0</em>, <em>0.2</em>, <em>0.4</em>, <em>0.6</em>, <em>0.8</em>, <em>1</em>, <em>1</em>, <em>1</em> and <em>1</em></p>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-10-1.png" width="576" /></p>
</div>
<div id="linear-splines" class="section level2">
<h2>Linear splines</h2>
<p>Knots: <em>0</em>, <em>0</em>, <em>0.2</em>, <em>0.4</em>, <em>0.6</em>, <em>0.8</em>, <em>1</em> and <em>1</em></p>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-12-1.png" width="576" /></p>
</div>
<div id="in-practice-splines-provide-a-flexible-fit" class="section level2">
<h2>In practice splines provide a flexible fit</h2>
<p><img src="/lecture/20-transformations_files/figure-html/unnamed-chunk-13-1.png" width="576" /></p>
</div>
<div id="section-2" class="section level2">
<h2></h2>
<ul>
<li><p><strong>Smoothing splines</strong>: have a large set of basis functions, but penalize against wiggliness</p></li>
<li><p><strong>Generalized Additive Models</strong>: simultaneously estimate</p></li>
</ul>
<p><span class="math display">\[
y_i = f(x_{i1}) + g(x_{i2}) + \ldots + \epsilon_i
\]</span></p>
</div>
<div id="transforming-predictors-with-basis-functions" class="section level2">
<h2>Transforming predictors with basis functions</h2>
<ul>
<li><p>The parameters in these regressions no longer have nice interpretations. The best way to present the results is a plot of the estimated function for each X, (or surfaces if variables interact),</p></li>
<li><p>The significance of a variable can still be assessed with an Extra Sum of Squares F-test, comparing to a model without any of the terms relating to a particular variable.</p></li>
</ul>
</div>
</div>
