---
title: "Model Selection"
subtitle: "ST552 Lecture 21"
author: "Charlotte Wickham"
date: "2019-02-27"
weeks: "8"
output: beamer_presentation
links:
  - ["lecture/21-stepwise.R", "R code"]
  - ["lecture/21-annotated.pdf", "Annotated Slides"]
---


## Overview

The process of selecting a few of many possible variables to include in a regression model is known as **variable** or **model selection**.

Reasons for preferring smaller models:

* Occam's razor
* Variables unrelated to the response in a model result in more noise in our estimates of interest
* May be cheaper to collect future data for fewer predictors
* May be easier to communicate/explain

## Model Selection in regression problems

Model selection doesn't replace thinking hard about a problem.

**Do I want a model that explains/predicts the response well?  In what way?**

* Set down a criteria for a good model.
* Search for models that do well on your criteria.
* You can often learn about the structure of your data, by examining a few of the "good" models.

## 
\small
**Do I want to answer a specific question of interest about the value of parameters in the model? ** 

This generally means you are very interested in a particular p-value and/or confidence interval.  In general, how to do valid inference after model selection is an unsolved problem.  
    
* Thinking hard about the problem beforehand (before seeing data) should elicit a model.  What are important covariates, should terms enter linearly, what terms will interact etc? If you are familiar enough with the field of application you should be able to do this. 
* Model selection will not be done at all.  There may be a small set of prespecified models for comparison. 
* Diagnostics are still important, you want to check your prespecified model is reasonable. 

I think of model selection as:
  
  * a tool for finding predicitve models
  * a tool for exploratory data analysis 

## Respecting heirachy

Some models are heirachical in nature, in that, a lower order term should not be dropped without dropping all higher order terms.

* Polynomials: $y_i = \beta_0 + \beta_1 x_i + \beta_2 x_i^2 + \beta_3 x_i^3 +\epsilon_i$
We wouldn't drop $x_i^2$ without also dropping $x_i^3$, similarly we wouldn't drop $x_i^2$ without dropping $x_i^2$ and $x_i^3$.

* Interactions: $y_i = \beta_0 + \beta_1 x_{i1} + \beta_2 x_{i2} + \beta_3 x_{i1}x_{i2} +\epsilon_i$. We wouldn't drop the main effect, $x_{i1}$, without also dropping the interaction, $x_{i1}x_{i2}$.

* Categorical variables: generally, consider keeping or dropping all the indicator variables for a single categorical variable as a group.

You might argue this isn't important for predictive models, but it removes dependence of models on the scale of variables, and makes comparing models easier.

## Stepwise methods

(Unless best subsets is infeasible, you **should not** use a stepwise method)

Stepwise methods rely on adding or removing a variable one at a time.  Each step chooses the best variable to add/remove based on some criterion, often based on a hypothesis test p-value.

For example, we'll use the p-value from the F-test comparing our current model to the candidate model. 

## Stepwise methods
**Backward Elimination**
Start with full model.  Drop the variable that has the highest p-value above some critical level, $\alpha_{crit}$. Repeat until all variables in the model have p-values below $\alpha_{crit}$.

**Forward Selection**
Start with only a constant mean in the model.  Add the variable that has the lowest p-value below some critical level, $\alpha_{crit}$. Repeat until no variable can be added with a p-value below $\alpha_{crit}$.

**Stepwise Selection** (many variants)
Start with forward selection until there are two terms in the model.  Then consider a backwards step.  Repeat a forwards step and a backwards step until a final model is reached.

$\alpha_{crit}$ does not have to be 0.05.  

## Example from Faraway

Backward elimination
\scriptsize
```{r}
library(faraway)
data(state)
state_data <- data.frame(state.x77)
lmod <- lm(Life.Exp ~ ., data = state_data)
sumary(lmod)
drop1(lmod, test = "F")  # will work better when factors are involved
```

##

```{r}
# one step of backward elimination
lmod <- update(lmod, . ~ . - Area)
sumary(lmod)
```

## Your turn

What would be the next step of backward elimination using $\alpha_{crit} = 0.05$?

## Forward selection

\scriptsize
```{r}
lmod <- lm(Life.Exp ~ 1, data = state_data)
add1(lmod, ~ Population + Income + Illiteracy + Murder + 
    HS.Grad + Frost + Area, 
  test = "F")
```

## 
\scriptsize
```{r}
# one step of forwards selection
lmod <- update(lmod, . ~ . + Murder)
add1(lmod, ~ Population + Income + Illiteracy + Murder + 
    HS.Grad + Frost + Area,
  test = "F")
```

## Your turn

What would be the next step in forward selection process using $\alpha_{crit} = 0.05$?

## Limitations of stepwise methods

1.  The make a very limited search through all possible models, so they may miss an "optimal" one.

2.  p-values will generally overstate the importance of remaining predictors.

3. Inclusion in the model doesn't correspond to important, and exclusion doesn't correspond to unimportant.

4. Tend to pick smaller models than optimal for prediction.

## Next time ... criterion based procedures