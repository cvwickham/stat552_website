\documentclass[ignorenonframetext,]{beamer}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\newif\ifbibliography
\hypersetup{
            pdftitle={Understanding regression models},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls

% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom

\AtBeginPart{
  \let\insertpartnumber\relax
  \let\partname\relax
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \let\insertsectionnumber\relax
    \let\sectionname\relax
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \let\insertsubsectionnumber\relax
  \let\subsectionname\relax
  \frame{\subsectionpage}
}

\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Understanding regression models}
\subtitle{ST552 Lecture 10}
\author{Charlotte Wickham}
\date{2019-01-30}

\begin{document}
\frame{\titlepage}

\begin{frame}{Today}

\begin{itemize}
\tightlist
\item
  Lecture: mathematical strategies for understanding models
\item
  Lab: understanding models through visualization
\end{itemize}

\end{frame}

\begin{frame}{Inference so far}

We've talked about the machinery to perform:

\begin{itemize}
\item
  t-tests, t-based confidence intervals for individual \(\beta\)s and
  linear combinations of \(\beta\)s
\item
  F-tests for hypotheses about many \(\beta\)s
\end{itemize}

But what can we do with this machinery?

\end{frame}

\begin{frame}{Two stages in understanding regression models}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Understand a model in the context of a problem
\item
  Define a set of models to answer questions of interest
\end{enumerate}

Focus on \#1 to gain intuition in how to approach \#2.

\end{frame}

\begin{frame}{Example}

LA Dodgers (baseball team) sometimes give out ``bobbleheads'' at home
games. They are curious if this increases attendance at games.

81 games in 2012 season, 11 of which bobbleheads were given out.

Have measurements on:

\begin{itemize}
\tightlist
\item
  attendance at game (number of people)
\item
  day of the week the game was played
\item
  some other variables, that are probably important, but we will ignore
  for now
\end{itemize}

\scriptsize
(\emph{Inspired by Chapter 2 in Modeling Techniques in Predictive
Analytics: Business Problems and Solutions with R. Get data from
\url{http://www.informit.com/promotions/modeling-techniques-in-predictive-analytics-141183}
if you interested.})

\end{frame}

\begin{frame}{A model}

\[
\begin{aligned}
\text{attendance}_i &= \beta_0 + \beta_1 1\{\text{bobblehead YES}\}_i + \\
& \beta_2 1\{\text{Tue}\}_i +
\beta_3  1\{\text{Wed}\}_i+ 
\beta_4  1\{\text{Thu}\}_i+ \\
& \beta_5  1\{\text{Fri}\}_i+
\beta_6  1\{\text{Sat}\}_i+
\beta_7  1\{\text{Sun}\}_i+ \epsilon_i
\end{aligned}
\]

\textbf{What does this model say about the relationship between
attendance and whether bobbleheads are given out and day of the week}?

\end{frame}

\begin{frame}{Aside some terminology}

\emph{variable}: a measurement made on the observational units.

E.g. bobblehead (yes/no) and day of week (mon/tue/wed/thu/fri/sat/sun).

\emph{term}: a column of the design matrix

E.g. \(\text{bobblehead}\), \(1\{\text{Fri}\}\)

\end{frame}

\begin{frame}{One useful strategy}

Ask about the \textbf{effect} of a \emph{variable}?

What does the model say about the mean response when a variable is
varied, holding all other variables constant.

\textbf{Categorical variable:} Find the mean response for each level and
compare.

\textbf{Continuous variable:} Find the change in mean response if the
variable increases by 1 unit.

\end{frame}

\begin{frame}{The effect of bobbleheads}

The variable has two levels: yes, no

We'll find
\(\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,}\) and
\(\E{\text{attendence} | \text{bobblehead} = \text{No}}\) then compare
them.

\[
\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,} 
\]

\[
\E{\text{attendence} | \, \text{bobblehead} = \text{No}\,} 
\]

\end{frame}

\begin{frame}{The effect of bobbleheads}

\[
\E{\text{attendence} | \, \text{bobblehead} = \text{Yes}\,}  - \E{\text{attendence} | \, \text{bobblehead} = \text{No}\,} 
\] \vspace{1in}

\begin{itemize}
\item
  For a fixed day of the week, the model predicts the mean attendance
  increases by \(\beta_1\) when bobbleheads are given out.
\item
  The model predicts the mean attendance when bobbleheads are given is
  \(\beta_1\), more than when bobbleheads aren't given, after accounting
  for day of the week.
\end{itemize}

\end{frame}

\begin{frame}{Relationship to inference}

If \(\beta_1\) is zero, then bobbleheads don't have an effect on
attendance.

We could answer the questions:

\begin{itemize}
\item
  Is the mean attendance higher when bobbleheads are given out? t-test
  on \(\beta_1 = 0\).
\item
  How much higher is the mean attendance higher when bobbleheads are
  given out? Confidence interval on \(\beta_1\)
\end{itemize}

But this is an observational study, so we need to be careful with our
language!\\
OK ``It is estimated distributing bobbleheads is \textbf{associated}
with an increased mean attendance of XX''.\\
Not OK ``It is estimated distributing bobbleheads increases the mean
attendance by XX''.

\end{frame}

\begin{frame}{Your turn: Day of the week}

What does model say about the expected attendance on Monday?

What does model say about the expected attendance on Tuesday?

What does model say about the expected attendance on Wednesday?

\end{frame}

\begin{frame}{Your turn: Day of the week}

\end{frame}

\begin{frame}{Day of the week: inference}

If \(\beta_2 = \beta_3 = \beta_4 = \beta_5 = \beta_6 = \beta_7\) is
zero, then day of the week doesn't have an effect on attendance.

What tools could we use to answer the questions:

\begin{itemize}
\item
  Does day of the week have an effect on mean attendance (after
  accounting for the bobblehead promotion)?
\item
  How much does the mean attendance differ between Friday and Saturday?
\end{itemize}

\end{frame}

\begin{frame}{An example with a continuous variable}

In lab today: \[
\text{weight}_i = \beta_0 + \beta_11\{\text{male}\}_i + \beta_2\text{height}_i + \epsilon_i
\]

What is the effect of height?

\end{frame}

\begin{frame}{What is the effect of height?}

Compare \(\E{\text{weight}| \, \text{height} = h \,}\) to
\(\E{\text{weight}| \, \text{height} = h + 1 \,}\), holding other
variables constant.

\vspace{2in}

\end{frame}

\begin{frame}{Interactions}

\textbf{Interactions}: describe situations where the effect of one
variable depends on the level of another explanatory variable.

E.g. \[
\text{weight}_i = \beta_0 + \beta_11\{\text{male}\}_i + \beta_2\text{height}_i + \beta_3 \left(1\{\text{male}\} \times \text{height}\right)_i + \epsilon_i
\]

The same strategy will work.

\end{frame}

\end{document}
