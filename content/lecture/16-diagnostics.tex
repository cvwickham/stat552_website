\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[ignorenonframetext,]{beamer}
\usepackage{pgfpages}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom
\setbeamertemplate{part page}{
\centering
\begin{beamercolorbox}[sep=16pt,center]{part title}
  \usebeamerfont{part title}\insertpart\par
\end{beamercolorbox}
}
\setbeamertemplate{section page}{
\centering
\begin{beamercolorbox}[sep=12pt,center]{part title}
  \usebeamerfont{section title}\insertsection\par
\end{beamercolorbox}
}
\setbeamertemplate{subsection page}{
\centering
\begin{beamercolorbox}[sep=8pt,center]{part title}
  \usebeamerfont{subsection title}\insertsubsection\par
\end{beamercolorbox}
}
\AtBeginPart{
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \frame{\subsectionpage}
}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Diagnostics: residual plots},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\newif\ifbibliography
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Diagnostics: residual plots}
\providecommand{\subtitle}[1]{}
\subtitle{ST552 Lecture 16}
\author{Charlotte Wickham}
\date{2019-02-15}

\begin{document}
\frame{\titlepage}

\begin{frame}{Violations of assumptions}
\protect\hypertarget{violations-of-assumptions}{}

In rough order of importance:

\begin{itemize}
\tightlist
\item
  \textbf{Systematic form of the model}, \(\E{Y} = X\beta\). If
  violated, the parameters in the model may be meaningless, estimates
  may be biased.
\item
  \textbf{Independence of errors}, \(\epsilon_i\) independent of
  \(\epsilon_j\) for all \(i\) and \(j\). If violated, estimates are
  still unbiased, but standard errors are generally inappropriate.
\item
  \textbf{Constant variance}, \(\Var{\epsilon_i} = \sigma^2\) for all
  \(i\). If violated, variance in predictions may not be properly
  quantified.
\item
  \textbf{Normality}, \(\epsilon \sim N()\). Can rely on CLT for large
  samples. If violated, prediction intervals are probably
  innappropriate.
\end{itemize}

\end{frame}

\begin{frame}{Using the residuals to diagnose problems}
\protect\hypertarget{using-the-residuals-to-diagnose-problems}{}

\begin{itemize}
\item
  If our model is correct, \(\epsilon \sim N(0, \sigma^2 I)\). But, we
  don't observe the errors.
\item
  Usually, we use the residuals as our best guess for the errors, and
  examine them for problems with the assumptions.
\item
  However, residuals by construction are not equal variance, or
  uncorrelated (you can try to standardize), but in practice the effects
  are small and ignored.
\item
  We can't prove the assumptions are satisfied, but we can look for
  evidence of gross violations.
\end{itemize}

\end{frame}

\begin{frame}{Graphical versus formal inferential methods}
\protect\hypertarget{graphical-versus-formal-inferential-methods}{}

\begin{itemize}
\item
  I am a strong proponent of graphical methods over formal tests for
  assumption checking.
\item
  Tests can only provide quantification of a deviation you are
  expecting, graphics reveal the unexpected.
\item
  Tests tend to make you focus on statistical significance not practical
  significance.

  For example, a large sample of data that is just a little non-Normal,
  will tend to give tiny p-values in a test of Normality, but for our
  purposes it isn't really a problem.
\end{itemize}

\end{frame}

\begin{frame}{Residual plots to examine}
\protect\hypertarget{residual-plots-to-examine}{}

\begin{itemize}
\item
  Residuals versus fitted values
\item
  Residuals versus explanatories (both those included and those excluded
  from the model)
\item
  Normal probability plot (Q-Q plot) of the residuals
\item
  Anything else you can think of that might reveal structure in the
  residuals. For example, if measurements are made over time or space,
  look for temporal or spatial patterns in the residuals.
\end{itemize}

\end{frame}

\begin{frame}{What to look for}
\protect\hypertarget{what-to-look-for}{}

\textbf{Residuals versus fitted or explantories}

An even width band vertically centered around zero as you move left to
right (always put the residuals on the y-axis).

\includegraphics{16-diagnostics_files/figure-beamer/unnamed-chunk-2-1.pdf}

\end{frame}

\begin{frame}{}
\protect\hypertarget{section}{}

\textbf{Q-Q plot of residuals}

Points falling close to a straight line

\includegraphics{16-diagnostics_files/figure-beamer/unnamed-chunk-3-1.pdf}

\end{frame}

\begin{frame}{Your turn: Part One}
\protect\hypertarget{your-turn-part-one}{}

Handout (Charlotte will bring):

\textbf{Part One} Describe what you see in the residual plots that
suggests a violation of assumptions.

\end{frame}

\begin{frame}{Your turn: Part Two}
\protect\hypertarget{your-turn-part-two}{}

\textbf{Part Two} The same five models are examined but in a random
order, with a much smaller sample size. Can you match these diagnostics
to those in Part Two??

\end{frame}

\begin{frame}{Your turn: Part Three}
\protect\hypertarget{your-turn-part-three}{}

\textbf{Part Three} Do you see any violations here?

\end{frame}

\begin{frame}{Common problems and possible solutions}
\protect\hypertarget{common-problems-and-possible-solutions}{}

\begin{itemize}
\item
  Non-constant spread

  \begin{itemize}
  \tightlist
  \item
    transform response (background knowledge, trial \& error, Box-Cox)
  \item
    use more complicated models (glm, gee)
  \end{itemize}
\item
  Non-linearity

  \begin{itemize}
  \tightlist
  \item
    transform response
  \item
    transform predictor
  \item
    allow for curvature (add predictor\(^2\), splines, gam)
  \item
    use a non-linear model
  \end{itemize}
\item
  Non-normality

  \begin{itemize}
  \tightlist
  \item
    transform response
  \item
    use more complicated models (glm)
  \end{itemize}
\item
  Structure when examined against an excluded variable - include it
\end{itemize}

\end{frame}

\end{document}
