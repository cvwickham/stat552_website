\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[ignorenonframetext,]{beamer}
\usepackage{pgfpages}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom
\setbeamertemplate{part page}{
\centering
\begin{beamercolorbox}[sep=16pt,center]{part title}
  \usebeamerfont{part title}\insertpart\par
\end{beamercolorbox}
}
\setbeamertemplate{section page}{
\centering
\begin{beamercolorbox}[sep=12pt,center]{part title}
  \usebeamerfont{section title}\insertsection\par
\end{beamercolorbox}
}
\setbeamertemplate{subsection page}{
\centering
\begin{beamercolorbox}[sep=8pt,center]{part title}
  \usebeamerfont{subsection title}\insertsubsection\par
\end{beamercolorbox}
}
\AtBeginPart{
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \frame{\subsectionpage}
}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Bootstrap Confidence Intervals},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\newif\ifbibliography
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Bootstrap Confidence Intervals}
\providecommand{\subtitle}[1]{}
\subtitle{ST552 Lecture 13}
\author{Charlotte Wickham}
\date{2019-02-11}

\begin{document}
\frame{\titlepage}

\begin{frame}{Motivation}
\protect\hypertarget{motivation}{}

The inferences we've covered so far relied on our assumption of Normal
errors: \[
\epsilon \sim N(0, \sigma^2 I_{n\times n})
\]

For example, we've seen under this assumption, the least squares
estimates are also Normally distributed: \[
\hat{\beta} \sim N\left(\beta,\, \sigma^2 \left(X^{T}X \right)^{-1} \right)
\]

\textbf{If the errors aren't truly Normally distributed, what
distribution do the estimates have?}

\end{frame}

\begin{frame}{Warm-up: Your Turn}
\protect\hypertarget{warm-up-your-turn}{}

Imagine the errors are in fact \(t_{3}\) distributed?

With your neighbour: \textbf{design a simulation to understand the
distribution of the least squares estimates.}

\end{frame}

\begin{frame}{}
\protect\hypertarget{section}{}

\end{frame}

\begin{frame}{Example: 1. Fix n, fix X}
\protect\hypertarget{example-1.-fix-n-fix-x}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-4-1}

\end{frame}

\begin{frame}{Example: 1. Fix \(\beta\), find \(\hat{y}\)}
\protect\hypertarget{example-1.-fix-beta-find-haty}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-5-1}

\end{frame}

\begin{frame}{Example: 2. Simulate errors, find \(y\)}
\protect\hypertarget{example-2.-simulate-errors-find-y}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-7-1}

\end{frame}

\begin{frame}{Example: 3. Find least squares line}
\protect\hypertarget{example-3.-find-least-squares-line}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-8-1}

\end{frame}

\begin{frame}{Example: 4. Repeat \#2. and \#3. many times}
\protect\hypertarget{example-4.-repeat-2.-and-3.-many-times}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-9-1}

\end{frame}

\begin{frame}{Example: Examine distribution of estimates}
\protect\hypertarget{example-examine-distribution-of-estimates}{}

\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-10-1}
\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-10-2}

\end{frame}

\begin{frame}{Example: Compared to theory}
\protect\hypertarget{example-compared-to-theory}{}

\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-12-1}
\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-12-2}

\end{frame}

\begin{frame}{When the errors aren't Normal: CLT}
\protect\hypertarget{when-the-errors-arent-normal-clt}{}

Think of our estimates like linear combinations of the errors. I.e. a
sort of average of i.i.d random variables.\\
Some version of the Central Limit Theorem will apply.

For large samples, even when the errors aren't Normal, \[
\hat{\beta} \dot{\sim} N(\beta,\, \sigma^2 (X^TX)^{-1})
\]

\end{frame}

\begin{frame}{Summary so far}
\protect\hypertarget{summary-so-far}{}

If we knew the error distribution and true parameters we could use
simulation to understand the sampling distribution the least squares
estimates.

Simulation can also be used to demonstrate the CLT at work in
regression.

\end{frame}

\begin{frame}{Bootstrap confidence intervals}
\protect\hypertarget{bootstrap-confidence-intervals}{}

In practice, with data in front of us, we don't know the distribution of
the errors (nor the true parameter values).

The bootstrap is one approach to estimate the sampling distribution of
\(\hat{\beta}\), by using the simulation idea, and substituting in our
best guesses for the things we don't know.

\end{frame}

\begin{frame}{Bootstrapping regression}
\protect\hypertarget{bootstrapping-regression}{}

\emph{(Model based resampling)}

0. Fit model and find estimates, \(\hat{\beta}\), and residuals, \(e_i\)

\begin{enumerate}
\tightlist
\item
  Fix \(X\),
\item
  For \(k = 1, \ldots, B\)

  \begin{enumerate}
  [i.]
  \tightlist
  \item
    Generate errors, \(\epsilon^*_i\) sampled with replacement from
    \(e_i\)
  \item
    Construct \(y\), using the model, \(y = \hat{y} + \epsilon^*\)
  \item
    Use least squares to find \(\hat{\beta}^*_{(k)}\)
  \end{enumerate}
\item
  Examine the distribution of \(\hat{\beta}^*\) and compare to
  \(\hat{\beta}\)
\end{enumerate}

One confidence interval for \(\beta_j\) is the 2.5\% and 97.5\%
quantiles of the distribution of \(\hat{\beta}_j^*\).\\
\emph{(Known as the Percentile method, there are other (better?)
methods)}.

\end{frame}

\begin{frame}{Example: Faraway Galapagos Islands}
\protect\hypertarget{example-faraway-galapagos-islands}{}

\emph{(I'll illustrate with simple linear regression, Faraway does
multiple case in 3.6)}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-13-1}

\end{frame}

\begin{frame}{Bootstrap: 1. Find \(\hat{\beta}\), \(\hat{y}\), and
\(e_i\).}
\protect\hypertarget{bootstrap-1.-find-hatbeta-haty-and-e_i.}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-14-1}

\end{frame}

\begin{frame}{Bootstrap: Using fixed \(X\), \(\hat{beta}\) from observed
data}
\protect\hypertarget{bootstrap-using-fixed-x-hatbeta-from-observed-data}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-15-1}

\end{frame}

\begin{frame}{Bootstrap: 2. Resample residuals to construct bootstrapped
response}
\protect\hypertarget{bootstrap-2.-resample-residuals-to-construct-bootstrapped-response}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-16-1}

\end{frame}

\begin{frame}{Bootstrap: 3. Fit regression model to bootstrapped
response}
\protect\hypertarget{bootstrap-3.-fit-regression-model-to-bootstrapped-response}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-17-1}

\end{frame}

\begin{frame}{Bootstrap: 3. Repeat \#2. and \#3. many times}
\protect\hypertarget{bootstrap-3.-repeat-2.-and-3.-many-times}{}

\includegraphics[height=3in]{14-bootstrap_files/figure-beamer/unnamed-chunk-18-1}

\end{frame}

\begin{frame}{Examine distribution of estimates}
\protect\hypertarget{examine-distribution-of-estimates}{}

\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-19-1}
\includegraphics[height=2in]{14-bootstrap_files/figure-beamer/unnamed-chunk-19-2}

\end{frame}

\begin{frame}{High level: bootstrap idea}
\protect\hypertarget{high-level-bootstrap-idea}{}

We don't know the distribution of the errors, but our best guess is
probably the empirical c.d.f on the residuals.

Sampling from a random variable with a c.d.f. defined as the empirical
c.d.f. of the residuals, boils down to sampling with replacement from
residuals.

\end{frame}

\begin{frame}{Limitations}
\protect\hypertarget{limitations}{}

We might rely on bootstrap confidence intervals when we are worried
about the assumption of Normal errors. But, there are limitations.

\begin{itemize}
\tightlist
\item
  We still rely on the assumption that the errors are independent and
  identically distributed.\\
\item
  Generally scaled residuals are used (residuals don't have the same
  variance, more later)
\item
  An alternative bootstrap resamples the
  \((y_i, x_{i1}, \ldots, x_{ip})\) vectors, i.e.~resamples the rows of
  the data, a.k.a \emph{resampling cases bootstrap}.
\end{itemize}

\end{frame}

\end{document}
