---
title: "Simple Linear Regression"
subtitle: "ST552 Lecture 2"
author: "Charlotte Wickham"
date: '2019-01-09'
weeks: "1"
type: lecture
output: beamer_presentation
links:
  - ["lecture/02-doc-cam.pdf", "Annotated Slides"]
---



<div id="high-level-review" class="section level2">
<h2>High level review</h2>
<p>Since simple linear regression is a special case of multiple linear regression, we’ll leave the “whys?” to when we cover multiple linear regression.</p>
<p>Today:</p>
<ul>
<li>the simple linear regression model</li>
<li>interpretation of parameters</li>
<li>assumptions</li>
<li>how the estimates are found</li>
<li>properties of the estimates</li>
</ul>
</div>
<div id="the-simple-linear-regression-model" class="section level2">
<h2>The simple linear regression model</h2>
<p><span class="math inline">\(n\)</span> observations are collected in pairs, <span class="math inline">\((x_i, y_i), i = 1, \ldots, n\)</span> where the <span class="math inline">\(y_i\)</span> are generated according to the model, <span class="math display">\[
y_i = \beta_0 + \beta_1 x_i + \epsilon_i 
\]</span></p>
<p><strong>What is random?</strong></p>

<p>where <span class="math inline">\(\epsilon_i\)</span> are independent and identically distributed with expected value zero, and variance <span class="math inline">\(\sigma^2\)</span>.</p>
<p>For inference, we often also assume the <span class="math inline">\(\epsilon_i\)</span> are Normally distributed, <span class="math display">\[
\epsilon_i \overset{i.i.d}{\sim} N(0, \sigma^2)
\]</span></p>
</div>
<div id="the-assumptions-in-words" class="section level2">
<h2>The assumptions in words</h2>
<ul>
<li>Linearity: the mean response is a straight line function of the explanatory variable</li>
<li>Constant spread: the standard deviation around the mean response is the same at all values of the explanatory variable</li>
<li>Normality: the deviations from the mean response, the errors, are Normally distributed.</li>
<li>Independence: the deviations from the mean response are independent.</li>
</ul>
</div>
<div id="example-weightlifting-birds" class="section level2">
<h2>Example: Weightlifting birds</h2>
<pre class="r"><code>library(Sleuth3)
?ex0727</code></pre>
<blockquote>
<p>Black wheatears are small birds in Spain and Morocco. Males of the species demonstrate an exaggerated sexual display by carrying many heavy stones to nesting cavities. This 35–gram bird transports, on average, 3.1 kg of stones per nesting season! Different males carry somewhat different sized stones, prompting a study on whether larger stones may be a signal of higher health status. Soler et al. calculated the average stone mass (g) carried by each of 21 male black wheatears, along with T-cell response measurements reflecting their immune systems’ strengths.</p>
</blockquote>
</div>
<div id="your-turn" class="section level2">
<h2>Your turn</h2>
<p>There are two variables measured on 21 individual Black wheatears:</p>
<ul>
<li><code>Mass</code> the average mass of stones carried by the bird</li>
<li><code>Tcell</code> the T-cell response, a measure of the birds immune response</li>
</ul>
<p>Discuss with your neighbour:</p>
<ul>
<li><p>Which variable would you use as the response? Which variable is the explanatory variable? Why?</p></li>
<li><p>What parameter would you look at in your model to answer question of interest?</p></li>
</ul>
</div>
<div id="section" class="section level2">
<h2></h2>
<p><img src="/lecture/02-simple-linear-regression_files/figure-html/unnamed-chunk-3-1.png" width="2in" style="display: block; margin: auto;" /><img src="/lecture/02-simple-linear-regression_files/figure-html/unnamed-chunk-3-2.png" width="2in" style="display: block; margin: auto;" /></p>
</div>
<div id="interpretation-of-the-parameters" class="section level2">
<h2>Interpretation of the parameters</h2>
<p><strong>Intercept</strong>, <span class="math inline">\(\beta_0\)</span>,<br />
When the explanatory variable is zero, the mean response is <span class="math inline">\(\beta_0\)</span>.</p>
<p><strong>Slope</strong>, <span class="math inline">\(\beta_1\)</span>,<br />
An increase in the explanatory variable of one unit is associated with a change in mean response of <span class="math inline">\(\beta_1\)</span>.</p>
<p>(Careful with causal language…is it justified?)</p>
<p>But we don’t know <span class="math inline">\(\beta_0\)</span> and <span class="math inline">\(\beta_1\)</span>…</p>
</div>
<div id="the-least-squares-estimates" class="section level2">
<h2>The least squares estimates</h2>
<p>Find <span class="math inline">\(\hat{\beta_0}\)</span> and <span class="math inline">\(\hat{\beta_1}\)</span> so that the sum of squared residuals <span class="math display">\[
\sum_{i=1}^{n} \left( y_i - (\hat{\beta_0} + \hat{\beta_1}x_i) \right)^2
\]</span> is minimised.</p>
<p>Fitted values: <span class="math inline">\(\hat{y_i} = \hat{\beta_0} + \hat{\beta_1}x_i\)</span><br />
Residuals: <span class="math inline">\(e_i = y_i - \hat{y_i}\)</span></p>
<p>We don’t require any properties of random variables to derive these estimates.</p>
<p>There are formulae for <span class="math inline">\(\hat{\beta_0}\)</span> and <span class="math inline">\(\hat{\beta_1}\)</span>, how do you derive them?</p>
</div>
<div id="properties-of-the-least-squares-estimates" class="section level2">
<h2>Properties of the least squares estimates</h2>
<p>Using the moment assumptions of <span class="math inline">\(\epsilon_i\)</span>, the least squares estimates can be shown to be unbiased. You can derive their variances, , , , but they depend on the unknown <span class="math inline">\(\sigma\)</span>.</p>
<p>An unbiased estimate of <span class="math inline">\(\sigma\)</span> is <span class="math display">\[
\hat{\sigma}^2 = \frac{ 1 }{n - 2} \sum_{i=1}^n e_i^2
\]</span></p>
<p>Intuition: <span class="math inline">\(\tfrac{1}{n} \sum_{i=1}^n e_i^2\)</span> seems a reasonable place to start to estimate the variance of the errors, but this tends to underestimate the variance because we picked our estimates to make the sum of squared errors as small as possible.</p>
</div>
<div id="in-r" class="section level2">
<h2>In R</h2>
<p><span class="math display">\[
\text{Mass}_i = \beta_0 + \beta_1 \text{Tcell}_i + \epsilon_i, \quad i = 1, \ldots, 21
\]</span></p>

<pre class="r"><code>slr &lt;- lm(Mass ~ Tcell, data = ex0727)</code></pre>
<pre class="r"><code>summary(slr)
#&gt; 
#&gt; Call:
#&gt; lm(formula = Mass ~ Tcell, data = ex0727)
#&gt; 
#&gt; Residuals:
#&gt;     Min      1Q  Median      3Q     Max 
#&gt; -3.1429 -0.7327  0.3448  0.7472  3.2736 
#&gt; 
#&gt; Coefficients:
#&gt;             Estimate Std. Error t value Pr(&gt;|t|)   
#&gt; (Intercept)    3.911      1.112   3.517  0.00230 **
#&gt; Tcell         10.165      3.296   3.084  0.00611 **
#&gt; ---
#&gt; Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
#&gt; 
#&gt; Residual standard error: 1.426 on 19 degrees of freedom
#&gt; Multiple R-squared:  0.3336, Adjusted R-squared:  0.2986 
#&gt; F-statistic: 9.513 on 1 and 19 DF,  p-value: 0.006105</code></pre>
</div>
