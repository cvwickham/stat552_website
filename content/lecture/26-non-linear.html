---
title: "Non-linear regression"
subtitle: "ST552 Lecture 26"
author: "Charlotte Wickham"
date: "2019-03-11"
weeks: "x10"
output: beamer_presentation
links:
  - ["lecture/26-annotated.pdf", "Annotated slides"]
---



<div id="announcements" class="section level2">
<h2>Announcements</h2>
<ul>
<li><p><strong>Extra</strong> office hours:</p>
<p>Fri (3/15) 1-2pm,<br />
Mon (3/18) 11am-12:30pm</p></li>
</ul>
</div>
<div id="non-linear-regression" class="section level2">
<h2>Non-linear regression</h2>
<p><span class="math display">\[
y_i = \eta(\mathbf{x_i}, \pmb{\beta}) + \epsilon_i \quad  \epsilon_i \sim N(0, \sigma^2)
\]</span></p>
<p>where <span class="math inline">\(\eta\)</span> is a known function <span class="math inline">\(\mathbf{x}_i\)</span> is a vector of covariates and <span class="math inline">\(\pmb{\beta}\)</span> a vector of <span class="math inline">\(p\)</span> unknown parameters.</p>
<p>Distinctions:</p>
<ul>
<li>If <span class="math inline">\(\eta(\mathbf{x}, \pmb{\beta}) = \mathbf{x}^T\pmb{\beta}\)</span> then we are in the usual linear regression setting.</li>
<li>If <span class="math inline">\(\eta(\mathbf{x}, \pmb{\beta})\)</span> is unknown but we are willing to represent it using basis functions, we are in the smooth regression setting.</li>
</ul>
</div>
<div id="example-masswtloss" class="section level2">
<h2>Example: <code>MASS::wtloss</code></h2>
<blockquote>
<p>The data frame gives the weight, in kilograms, of an obese patient at 52 time points over an 8 month period of a weight rehabilitation programme.</p>
</blockquote>
<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-2-1.png" width="576" /></p>
</div>
<div id="example-a-potential-model" class="section level2">
<h2>Example: A potential model</h2>
<p><span class="math display">\[
y_t = \beta_0 + \beta_1 2^{-t/\theta} + \epsilon_t
\]</span>
<span class="math inline">\(\beta_0\)</span>, <span class="math inline">\(\beta_1\)</span> are linear parameters, <span class="math inline">\(\theta\)</span> is a non-linear parameter.</p>
<p><strong>Your turn</strong></p>
<ul>
<li>When <span class="math inline">\(t = 0\)</span>, what is <span class="math inline">\(\E{y_i}\)</span>?<br />
</li>
<li>When <span class="math inline">\(t = \infty\)</span>, what is <span class="math inline">\(\E{y_i}\)</span>?<br />
</li>
<li>If <span class="math inline">\(\E{y_i} = \beta_0 + \tfrac{1}{2}\beta_1\)</span>, what is <span class="math inline">\(t\)</span>?</li>
</ul>
<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-3-1.png" width="576" /></p>
</div>
<div id="fitting-non-linear-models" class="section level2">
<h2>Fitting non-linear models</h2>
<p>Under the Normal error assumption, the MLE of <span class="math inline">\(\pmb{\beta}\)</span>, minimizes
<span class="math display">\[
\sum_{i = 1}^n (y_i - \eta(\mathbf{x_i}, \pmb{\beta}))^2
\]</span>
(the sum of squared residuals) a.k.a non-linear least squares.</p>
<ul>
<li><p>There isn’t in general a closed form solution, so iterative procedures are used.</p></li>
<li><p>This means you need to provide starting values from the parameters and check that the procedure converged.</p></li>
</ul>
</div>
<div id="your-turn" class="section level2">
<h2>Your turn</h2>
<p>What might be reasonable values for <span class="math inline">\(\beta_0\)</span>, <span class="math inline">\(\beta_1\)</span> and <span class="math inline">\(\theta\)</span>?</p>
<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-4-1.png" width="576" /></p>
</div>
<div id="in-r-nls" class="section level2">
<h2>In R: <code>nls()</code></h2>
<pre class="r"><code>wtloss.st &lt;- c(b0 = 100, b1 = 80, th = 100)
fit_nls &lt;- nls(Weight ~ b0 + b1*2^(-Days/th),
  data = wtloss, start = wtloss.st)
fit_nls</code></pre>
<pre><code>## Nonlinear regression model
##   model: Weight ~ b0 + b1 * 2^(-Days/th)
##    data: wtloss
##     b0     b1     th 
##  81.37 102.68 141.91 
##  residual sum-of-squares: 39.24
## 
## Number of iterations to convergence: 4 
## Achieved convergence tolerance: 5.259e-08</code></pre>
</div>
<div id="section" class="section level2">
<h2></h2>

<pre class="r"><code>summary(fit_nls)</code></pre>
<pre><code>## 
## Formula: Weight ~ b0 + b1 * 2^(-Days/th)
## 
## Parameters:
##    Estimate Std. Error t value Pr(&gt;|t|)    
## b0   81.374      2.269   35.86   &lt;2e-16 ***
## b1  102.684      2.083   49.30   &lt;2e-16 ***
## th  141.910      5.295   26.80   &lt;2e-16 ***
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
## 
## Residual standard error: 0.8949 on 49 degrees of freedom
## 
## Number of iterations to convergence: 4 
## Achieved convergence tolerance: 5.259e-08</code></pre>
</div>
<div id="section-1" class="section level2">
<h2></h2>
<pre class="r"><code>ggplot(wtloss, aes(Days, Weight)) +
  geom_line(aes(y = fitted(fit_nls)))</code></pre>
<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-7-1.png" width="576" /></p>
</div>
<div id="example-massmuscle" class="section level2">
<h2>Example: <code>MASS::muscle</code></h2>

<blockquote>
<p>The purpose of this experiment was to assess the influence of calcium in solution on the contraction of heart muscle in rats. The left auricle of 21 rat hearts was isolated and on several occasions a constant-length strip of tissue was electrically stimulated and dipped into various concentrations of calcium chloride solution, after which the shortening of the strip was accurately measured as the response.</p>
</blockquote>
<table style="width:46%;">
<colgroup>
<col width="12%" />
<col width="11%" />
<col width="9%" />
<col width="12%" />
</colgroup>
<thead>
<tr class="header">
<th align="center"> </th>
<th align="center">Strip</th>
<th align="center">Conc</th>
<th align="center">Length</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="center"><strong>3</strong></td>
<td align="center">S01</td>
<td align="center">1</td>
<td align="center">15.8</td>
</tr>
<tr class="even">
<td align="center"><strong>4</strong></td>
<td align="center">S01</td>
<td align="center">2</td>
<td align="center">20.8</td>
</tr>
<tr class="odd">
<td align="center"><strong>5</strong></td>
<td align="center">S01</td>
<td align="center">3</td>
<td align="center">22.6</td>
</tr>
<tr class="even">
<td align="center"><strong>6</strong></td>
<td align="center">S01</td>
<td align="center">4</td>
<td align="center">23.8</td>
</tr>
<tr class="odd">
<td align="center"><strong>9</strong></td>
<td align="center">S02</td>
<td align="center">1</td>
<td align="center">20.6</td>
</tr>
<tr class="even">
<td align="center"><strong>10</strong></td>
<td align="center">S02</td>
<td align="center">2</td>
<td align="center">26.8</td>
</tr>
</tbody>
</table>
</div>
<div id="example-massmuscle-1" class="section level2">
<h2>Example: <code>MASS::muscle</code></h2>

<pre class="r"><code>ggplot(muscle, aes(Conc, log(Length))) + 
  geom_point() + 
  facet_wrap(~ Strip)</code></pre>
<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-9-1.png" width="672" /></p>
</div>
<div id="example-the-same-parameters-for-every-strip" class="section level2">
<h2>Example: The same parameters for every strip</h2>
<p><span class="math display">\[
\log{y_{ij}} = \alpha + \beta \rho^{x_{ij}} + \epsilon_{ij}
\]</span>
<span class="math inline">\(j\)</span> indexes Strip, <span class="math inline">\(i\)</span> the ith measurement on a strip.</p>

<pre class="r"><code>fit_all &lt;- nls(log(Length) ~ cbind(1, rho^Conc), muscle,
  start = list(rho = 0.05), algorithm = &quot;plinear&quot;)

pred_grid &lt;- with(muscle, expand.grid(Conc = seq(0, 4, 0.1),
  Strip = unique(Strip)))
pred_grid$fit_all &lt;- predict(fit_all, pred_grid)</code></pre>
<p>The <code>plinear</code> algorithm takes advantage of the linear parameters.</p>
</div>
<div id="example-the-same-parameters-for-every-strip-1" class="section level2">
<h2>Example: The same parameters for every strip</h2>

<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-11-1.png" width="672" /></p>
</div>
<div id="example-different-parameters-for-every-strip" class="section level2">
<h2>Example: Different parameters for every strip</h2>
<p><span class="math display">\[
\log{y_{ij}} = \alpha_j + \beta_j \rho^{x_{ij}} + \epsilon_{ij}
\]</span>
</p>
<pre class="r"><code>fit_each &lt;- nls(log(Length) ~ alpha[Strip] + beta[Strip] * rho^Conc,
  muscle,
  start = list(rho = coef(fit_all)[1], alpha = rep(coef(fit_all)[2], 21),
    beta = rep(coef(fit_all)[3], 21)))

pred_grid$fit_each &lt;- predict(fit_each, pred_grid)</code></pre>
</div>
<div id="example-different-parameters-for-every-strip-1" class="section level2">
<h2>Example: Different parameters for every strip</h2>

<p><img src="/lecture/26-non-linear_files/figure-html/unnamed-chunk-13-1.png" width="672" /></p>
</div>
