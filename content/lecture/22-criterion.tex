\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[ignorenonframetext,]{beamer}
\usepackage{pgfpages}
\setbeamertemplate{caption}[numbered]
\setbeamertemplate{caption label separator}{: }
\setbeamercolor{caption name}{fg=normal text.fg}
\beamertemplatenavigationsymbolsempty
% Prevent slide breaks in the middle of a paragraph:
\widowpenalties 1 10000
\raggedbottom
\setbeamertemplate{part page}{
\centering
\begin{beamercolorbox}[sep=16pt,center]{part title}
  \usebeamerfont{part title}\insertpart\par
\end{beamercolorbox}
}
\setbeamertemplate{section page}{
\centering
\begin{beamercolorbox}[sep=12pt,center]{part title}
  \usebeamerfont{section title}\insertsection\par
\end{beamercolorbox}
}
\setbeamertemplate{subsection page}{
\centering
\begin{beamercolorbox}[sep=8pt,center]{part title}
  \usebeamerfont{subsection title}\insertsubsection\par
\end{beamercolorbox}
}
\AtBeginPart{
  \frame{\partpage}
}
\AtBeginSection{
  \ifbibliography
  \else
    \frame{\sectionpage}
  \fi
}
\AtBeginSubsection{
  \frame{\subsectionpage}
}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
\usetheme[]{metropolis}
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdftitle={Model Selection: Criterion Based methods},
            pdfauthor={Charlotte Wickham},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\newif\ifbibliography
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother

\definecolor{white}{rgb}{1,1,1}
\setbeamercolor{background canvas}{bg=white}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\title{Model Selection: Criterion Based methods}
\providecommand{\subtitle}[1]{}
\subtitle{ST552 Lecture 22}
\author{Charlotte Wickham}
\date{2019-03-01}

\begin{document}
\frame{\titlepage}

\begin{frame}{Stepwise methods}
\protect\hypertarget{stepwise-methods}{}

Stepwise methods consider a single path through the models (and only
ever one model of each size).

An alternative is to consider all possible models (a.k.a Best Subset),
but we need a way to compare models (F-tests don't help for non-nested
models).

General strategy of best subsets: calculate metric for all possible
models. Choose model with the ``best'' value of the metric

\end{frame}

\begin{frame}{}
\protect\hypertarget{section}{}

Some common metrics, in the regression setting \[
\begin{aligned}
\text{Akaike Information Criterion} = \text{AIC} &= n\log{(RSS / n)} + 2p  \\
\text{Bayesian Information Criterion} =  \text{BIC} &= n\log{(RSS / n)} + p\log{n}  \\
\text{Mallow's Cp} &= \frac{RSS} {\hat{\sigma^2}} + 2p - n \\
& \quad \text{where } \hat{\sigma^2} \text{ is from the full model}\\
\text{Adjusted R}^2 &= 1 - \frac{n-1}{n-p}(1-R^2)  
\end{aligned}
\]

Small is good for AIC, BIC and Mallow's Cp.

Large is good for adjusted \(R^2\).

Which is best? It depends\ldots{}

\end{frame}

\begin{frame}{AIC}
\protect\hypertarget{aic}{}

Arises from considering how to estimate the distance of a candidate
model from the true model.

In particular using the Kullback-Leibler information to measure distance
\[
I(f, g) = \int f(x) \log{\left( \frac{f(x)}{g(x|\theta)} \right)} dx
\] In general, can estimate the expected value, at the maximum
likelihood estimate of \(\theta\) with \[
- \log{L(\hat{\theta})} + p + \text{constant}
\]

Akaike multiplied this by 2. We can ignore any constants that are the
same for a given dataset and assumed error distribution for regression
variable selection. Care should be taken in other contexts.

\end{frame}

\begin{frame}{}
\protect\hypertarget{section-1}{}

\textbf{BIC}

Arises from trying to find the model with the highest posterior
probability.

\textbf{Mallow's Cp}

Tries to estimate the mean square prediction error.

\textbf{Adjusted R-squared}

Find a model with the highest \(R^2\), but \(R^2\) always increases when
you add a variable. Adjusted \(R^2\) penalises for more variables.

\end{frame}

\begin{frame}{Return to example in R}
\protect\hypertarget{return-to-example-in-r}{}

\end{frame}

\begin{frame}{Limitations of best subsets methods (or criterion based
methods as Faraway calls them)}
\protect\hypertarget{limitations-of-best-subsets-methods-or-criterion-based-methods-as-faraway-calls-them}{}

\begin{enumerate}
\item
  p-values will generally overstate the importance of remaining
  predictors
\item
  Inclusion in the model doesn't correspond to important, and exlcusion
  doesn't correspond to unimportant.
\end{enumerate}

\end{frame}

\begin{frame}{Comments}
\protect\hypertarget{comments}{}

These metrics are estimates, and like all estimates are subject to
variability.

The ranking of models for one dataset might be different to another
generated from the same data generating process.

There are some assymptotic results. Two common types:

\begin{itemize}
\tightlist
\item
  consistent for model selection: if you have enough data you will get
  the right model
\item
  optimal for prediction: if you have enough data you will get the best
  predictions (in the sense of squared error)
\end{itemize}

Alternative estimate of model performance: use external test set, and
estimate your desired metric directly. (The idea behind cross
validation)

\end{frame}

\end{document}
