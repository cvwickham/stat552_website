---
title: "Regularization"
subtitle: "ST552 Lecture 24"
author: "Charlotte Wickham"
date: "2019-03-06"
output: beamer_presentation
weeks: "9"
links:
  - ["lecture/24-annotated.pdf", Annotated Slides]
---



<div id="what-to-expect-on-the-final" class="section level2">
<h2>What to expect on the final?</h2>
<ul>
<li>Study guide posted along with last year’s final.</li>
</ul>
<p>Three questions worth roughly equal amounts</p>
<ol style="list-style-type: decimal">
<li><p><strong>Calculations, inference and interpretation.</strong> Like Q2 and Q3 on midterm: t-tests, F-tests, confidence intervals, prediction intervals etc. Know your formulas, how to use them and how to interpret the results.</p></li>
<li><p><strong>Assumptions and diagnostics.</strong> Examine plots, identify problems, discuss the consequences of the problem, suggest remedies, suggest ways to verify if the suggestions worked. Or suggest ways to diagnose certain problems.</p></li>
<li><p>Everything else, at a conceptual level.</p></li>
</ol>
</div>
<div id="lab-in-week-10" class="section level2">
<h2>Lab in Week 10</h2>
<p>Some options:</p>
<ul>
<li>Revisit a topic (or more depth on a topic). Which topic?</li>
<li>Exam review session</li>
<li>No lab</li>
</ul>
</div>
<div id="today" class="section level2">
<h2>Today</h2>
<ul>
<li>The Bias-Variance tradeoff</li>
<li>Regularized regression: lasso and ridge</li>
</ul>
</div>
<div id="bias-variance-tradeoff" class="section level2">
<h2>Bias-Variance tradeoff</h2>
<p>For estimates, the mean squared error of an estimate can be broken down into bias and variance terms:
<span class="math display">\[
\begin{aligned}
MSE(\hat{\theta}) &amp; = \E{(\hat{\theta} - \theta)^2} =
\E{\left(\hat{\theta} - \E{\hat{\theta}}\right)^2} + \left(\E{\hat{\theta}} - \theta\right)^2 \\
&amp; = \Var{\hat{\theta}} + \text{Bias}\left(\hat{\theta}\right)^2
\end{aligned}
\]</span></p>
<p>Often in statistics, we focus on estimates that are unbiased (so the second term is zero), and focus on minimising the variance term.</p>
<p>You might argue that you are willing to introduce a little bias, if it reduces the variance enough to reduce the overall mean squared error in the estimate.</p>
</div>
<div id="section" class="section level2">
<h2></h2>
<p>There is a similar breakdown for the mean square error in prediction. Let <span class="math inline">\(\hat{f}(X)\)</span> indicate the regression model for predicting an observation with explanantory values <span class="math inline">\(X\)</span>.</p>
<p>If the true data is generated according to <span class="math inline">\(Y = f(X) + \epsilon\)</span>, where <span class="math inline">\(\E{\epsilon} = 0\)</span> and <span class="math inline">\(\Var{\epsilon}= \sigma^2\)</span>, then the MSE for a point <span class="math inline">\(x_0\)</span>:
<span class="math display">\[
\begin{aligned}
&amp;MSE(\hat{f}(X)| X = x_0)  =  \\
\quad &amp;\E{(Y - \hat{f}(X))^2 | X = x_0} \\
\quad &amp;=
\E{\left(\hat{f}(x_0) - \E{\hat{f}(x_0)}\right)^2} + \E{\left(Y - \E{\hat{f}(x_0)}\right)}^2 \\
\quad &amp;= \Var{\hat{f}(x_0)} + \text{Bias}\left(\hat{f}(x_0)^2\right) + \sigma^2
\end{aligned}
\]</span></p>
<p>Bias captures how far our predictions are from the true mean on average (over repeated samples).</p>
<p>Variance captures how much our predictions vary (over repeated samples).</p>
</div>
<div id="simulated-example" class="section level2">
<h2>Simulated Example</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-3-1.png" width="576" /></p>
</div>
<div id="high-bias-low-variance" class="section level2">
<h2>High bias, low variance</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-4-1.png" width="576" /></p>
</div>
<div id="low-bias-low-variance" class="section level2">
<h2>Low bias, low variance</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-5-1.png" width="576" /></p>
</div>
<div id="low-bias-high-variance" class="section level2">
<h2>Low bias, high variance</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-6-1.png" width="576" /></p>
</div>
<div id="bias-variance-tradeoff-1" class="section level2">
<h2>Bias Variance tradeoff</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-7-1.png" width="576" /></p>
<p>In general, more complex models decrease bias and increase variance. We hunt for the sweet spot where MSE is minimized.</p>

<p>There aren’t nice partitions into bias and variance for other metrics, but the pattern is usually the same. Increasing complexity only improves performance to a point, then it decreases performance.</p>
</div>
<div id="regularized-regression" class="section level2">
<h2>Regularized regression</h2>
<p>One approach that introduces bias into the coefficient estimates, is regularized (a.k.a. penalized) regression. Instead of minimising
<span class="math display">\[
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2
\]</span>
minimise
<span class="math display">\[
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2 + \lambda \sum_{j = 1}^{p} f(\beta_j)
\]</span>
When <span class="math inline">\(f(\beta_j) = \beta_j^2\)</span>, the method is called <strong>ridge regression</strong>, and when <span class="math inline">\(f(\beta_j) = |\beta_j|\)</span> the method is called <strong>lasso</strong>.</p>
<p><strong>The general idea:</strong> the first term rewards good fit to the data, the second penalizses for large values on the coefficients.</p>
<p>The result: estimates shrink toward zero (introducing bias) and have smaller variance.</p>
</div>
<div id="alternative-view" class="section level2">
<h2>Alternative view</h2>
<p>You can also view ridge and lasso as constrained minimisation, where we minimise
<span class="math display">\[
\sum_{i = 1}^n \left(y_i - \hat{y_i}\right)^2
\]</span>
subject to the constraints
<span class="math display">\[
\begin{aligned}
\sum_{j = 1}^{p} \beta_j^2 \le t \quad \text{for ridge} \\
\sum_{j = 1}^{p} |\beta_j| \le s \quad \text{for lasso}
\end{aligned}
\]</span></p>
</div>
<div id="ridge-estimates" class="section level2">
<h2>Ridge estimates</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-8-1.png" width="576" /></p>
</div>
<div id="lasso-estimates" class="section level2">
<h2>Lasso estimates</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-9-1.png" width="576" /></p>
</div>
<div id="finding-the-tuning-parameter" class="section level2">
<h2>Finding the tuning parameter</h2>

<pre class="r"><code>cvout &lt;- cv.lars(as.matrix(trainmeat[ , -101]), trainmeat$fat)</code></pre>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-10-1.png" width="576" /></p>
</div>
<div id="tuning-parameter" class="section level2">
<h2>Tuning parameter</h2>

<pre class="r"><code>(best_s &lt;- cvout$index[which.min(cvout$cv)])</code></pre>
<pre><code>## [1] 0.02020202</code></pre>
<p>RMSE for this value:</p>
<pre class="r"><code>testx &lt;- as.matrix(testmeat[,-101])

predlars &lt;- predict(fit_lasso, testx, s=best_s,
  mode=&quot;fraction&quot;)
sqrt(mean((testmeat$fat - predlars$fit)^2))</code></pre>
<pre><code>## [1] 2.062124</code></pre>
</div>
<div id="estimated-coefficicents" class="section level2">
<h2>Estimated coefficicents</h2>
<p><img src="/lecture/24-regularization_files/figure-html/unnamed-chunk-13-1.png" width="576" /></p>
<pre><code>## [1] 27</code></pre>
</div>
<div id="key-points" class="section level2">
<h2>Key points</h2>
<ul>
<li>Regularized/Penalized regression models have a tuning parameter that controls the degree of penalization/shrinkage</li>
<li>The tuning parameter may be chosen to optimize some kind of criterion</li>
<li>Lasso estimates can be exactly zero (so it performs model selection as well)</li>
</ul>
</div>
