# === Estimating MSE on "held-out" set ====
source("http://stat552.cwick.co.nz/lecture/fortify-leaps.R")
library(tidyverse)
library(leaps)

data(meatspec, package = "faraway")
str(meatspec)

set.seed(19128)
ind <- sample(nrow(meatspec), size = 172)
trainmeat <- meatspec[ind, ]
testmeat <- meatspec[-ind, ]

# fit model with training set
fit_meat <- lm(fat ~ ., data = trainmeat)

# predict on "unseen" data, the test set
testmeat$pred <- predict(fit_meat, testmeat)

# average squared error
mse <- with(testmeat, mean((fat - pred)^2))
# usually reported on root scale
sqrt(mse)

# === Can we do better by using model selection? ====

# Your turn....might be a starting point?
fit_ss <- regsubsets(fat ~ ., data = trainmeat, 
  nbest = 5,  # keep the best 5 models of each size
  nvmax = 101, # maximum number of parameters in the model 
  method = "backward")  # strategy 

models <- fortify(fit_ss) 
glimpse(models)

# why BIC? Why not something else? 
# Does a good BIC correspond to low mean squared prediction error?
models %>% 
  ggplot(aes(size,bic)) + 
  geom_point() 

models %>% 
  ggplot(aes(size, adjr2)) + 
    geom_point() 

# zoom in
models %>% 
  filter(size < 25, size > 15) %>% 
  ggplot(aes(size, bic)) + 
  geom_point() 


models %>% 
  as_tibble() %>% 
  arrange(bic) %>% 
  select(size, model_words, bic) %>% 
  slice(1) %>% 
  pull(model_words)

# Find the estimated root mean square prediction error for your
# candidate model.

fit_best <- lm(fat ~ V1+V17+V19+V20+V21+V24+V31+V35+V37+V39+V40+V41+V44+V47+V48+
    V51+V53+V57,
  data = trainmeat)
# predict on "unseen" data, the test set
testmeat$pred_best <- predict(fit_best, testmeat)

# average squared error
mse_best <- with(testmeat, mean((fat - pred_best)^2))
# usually reported on root scale
sqrt(mse_best)
