---
title: "Midterm"
date: "2019-02-08"
weeks: "5"
no_link: true
links:
  - [lecture/midterm19-solution.pdf, Midterm Solutions]
---