\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Week 2},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\newcommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{Week 2}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{}
    \preauthor{}\postauthor{}
    \date{}
    \predate{}\postdate{}
  
\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\renewcommand*\familydefault{\sfdefault}
\setlength{\headsep}{0.5in}

\makeatletter
\@ifclassloaded{exam}
  { \pagestyle{headandfoot}
    \headrule
    }
  { \usepackage{fancyhdr}
    \pagestyle{fancy}
    \let\ps@plain\ps@fancy 
    }%
\makeatother

\lhead{\footnotesize \inserttitle}
\rhead{\footnotesize ST552: Statistical Methods II, Winter 2019}
\cfoot{\thepage}

\makeatletter
\let\inserttitle\@title
\makeatother

\begin{document}
\maketitle

\subsection{Mon}\label{mon}

\subsubsection{Learning Objectives}\label{learning-objectives}

By the end of class a student should be able to:

\begin{itemize}
\item
  State the multiple regression model in matrix form along with the
  assumptions on the errors and design matrix.
\item
  Describe the entries in the design matrix given a model and study
  description.
\end{itemize}

\subsubsection{Outline}\label{outline}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  \textbf{Matrix warmup} Have the warmup worksheet on projector as
  students arrive. At 3:00pm, introduce yourself, then give another
  5mins for warmup and students to compare answers.

  You don't need to go over all the answers but remind students:

  \begin{itemize}
  \tightlist
  \item
    Dimensions must be the same for addition.
  \item
    Dimension must conform for multiplication (i.e.~inner dimensions are
    the same)
  \item
    Order matters with matrix multiplication: \(bX \ne Xb\) in general.
  \item
    \(IX = X\) for any \(X\) as long as dimensions work
  \item
    \(Y_{n \times n}^{-1}\) doesn't always exist! Exists if
    \(\det(Y) \ne 0 \iff Y\) is full rank.
  \end{itemize}
\item
  \textbf{Multiple linear regression} Write simple linear regression
  model on board: \[
  y_i = \beta_0 + \beta_1 x_i + \epsilon_i \quad i = 1, \ldots, n
  \] Now we want to add more than one explanatory, relabel \(x_i\) as
  \(x_{i1}\), erase \(\epsilon_i\), add \(\beta_2x_{i2}\), add more (dot
  dot dot) up to \(\beta_{p-1}x_{i(p-1)}\), add back on \(\epsilon_i\).

  Also explain:

  \begin{itemize}
  \tightlist
  \item
    We have \(p-1\) explanatories, so with intercept, we have \(p\)
    regression coefficients.
  \item
    Same assumptions on \(\epsilon_i\) as simple linear regression -
    i.i.d expected value 0, variance \(\sigma^2\).
  \end{itemize}
\item
  \textbf{Concrete example} Go over Galapogos example in slides.
  Emphasize keeping track of which index corresponds to which
  explanatory is hard, so often just use variable name.

  Give 2mins for students to answer ``Your turn'' questions with
  neighbour. Go over answers together:

  \begin{itemize}
  \tightlist
  \item
    What does \(i\) index? Islands.
  \item
    What is the value of \(n\)? 30, since there are 30 islands.
  \item
    What is the value of \(p\)? \(p = 6\), six parameters, five
    explanatory variables plus an intercept.
  \end{itemize}
\item
  \textbf{Matrix form} Either on board, or using slide on doc cam, put
  up multiple linear regression model in full matrix form (i.e.~with all
  rows and columns).

  Work through first row, show that it is the same as previous model for
  \(i = 1\). Give abbreviated form \(y = X\beta + \epsilon\), go through
  matrix dimensions, and names (repsone, random errors, paramter vector,
  design matrix).
\item
  \textbf{Galapogos Matrix form} Go through concrete example of matrix
  form for this example. Label columns with variable names.
\item
  \textbf{Other models} Give students 5-6 minutes to work on your turn.
  Go over answers together:

  \[
  X_{30\times 3} = \left(
  \begin{array}{rrr}
    1 & 25.09 & 0.6\\ 
   1 & 1.24 & 0.6\\ 
   1 & 0.21 & 2.8\\ 
   1 & 0.1 & 1.9\\ 
   1 & 0.05 & 1.9 \\
    \vdots & \vdots &  \vdots  \\
  \end{array}
  \right)
  \] Easy, just a subset of the columns in the previous example.

  \[
  X_{30\times 2} = \left(
  \begin{array}{rrr}
    25.09 & 629.5081\\ 
   1.24 & 1.5376\\ 
   0.21 & 0.0441\\ 
   0.1 & 0.01\\ 
   0.05 & 0.0025 \\
    \vdots & \vdots \\
  \end{array}
  \right)
  \] We don't have to have an intercept, but be careful if you don't,
  \(p\) is the same as the number of explanatory variables, \(\beta\)
  starts indexing at 1.

  We are also allowed to transform variables to make new ones. In this
  case allowing a quadratic relationship to area.

  \[
  X_{30\times 2} = \left(
  \begin{array}{rrr}
    1 & 1\\ 
   1 & 1\\ 
   1 & 0\\ 
   1 & 0\\ 
   1 & 0 \\
    \vdots & \vdots \\
  \end{array}
  \right)
  \] Indicators are OK too, and is how we'll handle categorical
  variables later. In this example, we've allowed in a different mean
  number of species for islands with area greater than 1.
\item
  \textbf{Fitted values and residuals} If we had estimate for \(\beta\),
  define fitted value and residual vectors. Get students to workout
  dimensions if you have time.
\item
  \textbf{Wrap up} Motivate the rest of week:

  \begin{itemize}
  \tightlist
  \item
    How will we estimate \(\beta\)?
  \item
    What properties will the estimates have.
  \end{itemize}
\end{enumerate}

\newpage 

\subsection{Wed}\label{wed}

\subsubsection{Learning Objectives}\label{learning-objectives-1}

By the end of class a student should be able to:

\begin{itemize}
\item
  Derive the least squares estimates.
\item
  Define fitted values and residuals.
\item
  Describe the difference between random errors and residuals.
\end{itemize}

\subsubsection{Outline}\label{outline-1}

The handout provided to students has a mixture of short notes and
exercises. I suggest getting the student's into small groups. Then
alternate between you setting up the next exercise with the whole class
(i.e.~go over the note part), and the student's spending some time
working on the exercise (filling in the boxes).

Alternatively, you might just unleash them on the handout, and gradually
fill in the blanks as they progress (without stopping the class, but
where they can see it), and if they have questions either help groups
indivdually or go over a step with the class.

There is a lot on the handout! I expect you'll definitely get up to
``The Hat Matrix''. If you don't get through the whole thing, just
finish it up on Friday.

I'll post a version with all the solutions after class.

\textbf{Before class ends} Emphasize:

\begin{itemize}
\item
  Finding the least squares estimates is just algebra and calculus
  (albeit slightly out of the normal algebra and calculus). We didn't
  make any statistical assumptions to get them.
\item
  The estimates have a closed form
  \(\hat{\beta} = \left(X^TX \right)^{-1}X^Ty\) and they should memorize
  it.
\item
  Because \(\epsilon\) is a random vector, \(y\) is a random vector, and
  hence our estimates are random vectors and we can talk about their
  statistical properties.
\end{itemize}

\newpage

\subsection{Fri}\label{fri}

\subsubsection{Learning Objectives}\label{learning-objectives-2}

By the end of class a student should be able to:

\begin{itemize}
\item
  Derive the mean and variance-covariance matrix of the least squares
  estimates in multiple linear regression.
\item
  State the Gauss-Markov theorem and discuss it's consequences in
  practice.
\item
  State the form and properties of the estimate for the variance of the
  errors.
\end{itemize}

\subsubsection{Outline}\label{outline-2}

Run Friday just like Wednesday.

Depending on how time goes you might fill the in blanks for them when
deriving the variance-covariance matrix of the estimates.

\textbf{Before class ends} Emphasize the properties we derived: least
squares estimates are \textbf{unbiased} and we have a form for their
variance-covariance matrix.


\end{document}
