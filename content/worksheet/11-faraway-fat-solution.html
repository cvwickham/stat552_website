---
title: Predicting Body Fat
author: Charlotte Wickham
date: "2019-02-01"
---



<p>Based on code from <a href="http://www.maths.bath.ac.uk/~jjf23/LMR/scripts2/prediction.R" class="uri">http://www.maths.bath.ac.uk/~jjf23/LMR/scripts2/prediction.R</a>, also found in Section 4.2 of Linear Models with R</p>
<pre class="r"><code>library(faraway)
library(tidyverse)</code></pre>
<pre><code>## ── Attaching packages ────────────────────────────────────────────── tidyverse 1.2.1 ──</code></pre>
<pre><code>## ✔ ggplot2 3.1.0     ✔ purrr   0.2.5
## ✔ tibble  2.0.1     ✔ dplyr   0.7.8
## ✔ tidyr   0.8.2     ✔ stringr 1.3.1
## ✔ readr   1.1.1     ✔ forcats 0.3.0</code></pre>
<pre><code>## Warning: package &#39;tibble&#39; was built under R version 3.5.2</code></pre>
<pre><code>## ── Conflicts ───────────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()</code></pre>
<pre class="r"><code>data(fat, package = &quot;faraway&quot;)</code></pre>
<div id="background" class="section level2">
<h2>Background</h2>
<p><strong>Q1</strong>: Take a quick read through of the documentation on this dataset, <code>?fat</code>. We will be using the <code>brozek</code> variable, a hard to obtain but accurate measure of body fat percentage, as our response.</p>
<p>In context of the data (discuss with your neighbours):</p>
<p><code>y</code>: body fat percentage (based on Brozek’s eqn.) <code>x_0</code>: a specific value for age, weight, height and the 10 body circumference.</p>
<ul>
<li>What would a confidence interval on the mean response tell us? When might it be useful?</li>
</ul>
<p>For men with characteristic given by <code>x_0</code>, the 95% confidence interval for <em>average</em> body fat percentage. Public health scenario…</p>
<ul>
<li>What would a prediction interval on a response tell us? When might it be useful?</li>
</ul>
<p>For a man with characteristics given by <code>x_0</code>, 95% prediction interval for body percentage. E.g. Nick wants to know his body fat. Doctor who wants to estimate body fat for a specific male patient in front of them.</p>
<p>Faraway says:</p>
<blockquote>
<p>“Normally, we would start with an exploratory analysis of the data and a detailed consideration of what model to use but let’s be rash and just fit a model and start predicting.”</p>
</blockquote>
<pre class="r"><code>lmod &lt;- lm(brozek ~ age + weight + height + neck + chest + 
    abdom + hip + thigh + knee + ankle + biceps + forearm + wrist, data = fat)

X &lt;- model.matrix(lmod)</code></pre>
</div>
<div id="prediction-for-a-typical-man" class="section level2">
<h2>Prediction for a “typical” man</h2>
<p>This is Faraway’s definition of an average man:</p>
<pre class="r"><code>(x0 &lt;- apply(X, 2, median))  # </code></pre>
<pre><code>## (Intercept)         age      weight      height        neck       chest 
##        1.00       43.00      176.50       70.00       38.00       99.65 
##       abdom         hip       thigh        knee       ankle      biceps 
##       90.95       99.30       59.00       38.50       22.80       32.05 
##     forearm       wrist 
##       28.70       18.30</code></pre>
<p>He decided he wanted a prediction for an “average man”. We needed an <code>x0</code> to play with.</p>
<p>We can then predict bodyfat for a man with these physical characteristics:</p>
<pre class="r"><code>(y0 &lt;- sum(x0 * coef(lmod))) # x_0^T \hat{\beta}</code></pre>
<pre><code>## [1] 17.49322</code></pre>
<p>Or let R do the work:</p>
<pre class="r"><code>(pred &lt;- predict(lmod, new = data.frame(t(x0)), se = TRUE))</code></pre>
<pre><code>## $fit
##        1 
## 17.49322 
## 
## $se.fit
## [1] 0.278665
## 
## $df
## [1] 238
## 
## $residual.scale
## [1] 3.987973</code></pre>
<pre class="r"><code># se.fit is the standard error in the predicted *mean* response.
# residual.scale is estimate of sigma</code></pre>
<p><strong>Q2</strong>: Find the <code>se.fit</code> value by using matrix algebra i.e. <span class="math inline">\(\hat{\sigma}\sqrt{x_0^T(X^TX)^{-1}x_0}\)</span> and <code>residual.scale</code> instead.</p>
<pre class="r"><code>pred$residual.scale * sqrt( t(x0) %*% solve(t(X) %*% X) %*% x0   )</code></pre>
<pre><code>##          [,1]
## [1,] 0.278665</code></pre>
<p><code>predict()</code> will construct a prediction interval if you ask for it:</p>
<pre class="r"><code>predict(lmod, new = data.frame(t(x0)), interval = &quot;prediction&quot;)</code></pre>
<pre><code>##        fit     lwr      upr
## 1 17.49322 9.61783 25.36861</code></pre>
<p><strong>Q3</strong> Using the values from Q2 replicate the calculation of this interval.</p>
<pre class="r"><code>se_mean &lt;- pred$residual.scale * 
  sqrt(t(x0) %*% solve(t(X) %*% X) %*% x0)
se_pred &lt;- pred$residual.scale * 
  sqrt(1 + t(x0) %*% solve(t(X) %*% X) %*% x0)

y0 + c(-1, 1) * qt( 0.975, df = 238) * as.numeric(se_pred)</code></pre>
<pre><code>## [1]  9.61783 25.36861</code></pre>
<p>If you ask for a <code>&quot;confidence&quot;</code> interval instead you’ll get the interval for the <strong>mean</strong> response:</p>
<pre class="r"><code>predict(lmod, new = data.frame(t(x0)), interval = &quot;confidence&quot;) </code></pre>
<pre><code>##        fit      lwr      upr
## 1 17.49322 16.94426 18.04219</code></pre>
</div>
<div id="width-of-intervals" class="section level2">
<h2>Width of intervals</h2>
<p>Intervals are wider the further we are from the average explanatory values. Let’s look at a prediction for someone who is at the (sample) 95th percentile on all variables:</p>
<pre class="r"><code>(x1 &lt;- apply(X, 2, quantile, probs = 0.95))</code></pre>
<pre><code>## (Intercept)         age      weight      height        neck       chest 
##       1.000      67.000     225.650      74.500      41.845     116.340 
##       abdom         hip       thigh        knee       ankle      biceps 
##     110.760     112.125      68.545      42.645      25.445      37.200 
##     forearm       wrist 
##      31.745      19.800</code></pre>
<p>Compare the width of these intervals to those above:</p>
<pre class="r"><code>predict(lmod, new = data.frame(t(x1)), interval = &quot;prediction&quot;)</code></pre>
<pre><code>##        fit      lwr      upr
## 1 30.01804 21.92407 38.11202</code></pre>
<pre class="r"><code>predict(lmod, new = data.frame(t(x1)), interval = &quot;confidence&quot;)</code></pre>
<pre><code>##        fit      lwr      upr
## 1 30.01804 28.07072 31.96537</code></pre>
</div>
<div id="aside-on-other-packages" class="section level2">
<h2>Aside on other packages</h2>
<p>The <code>add_predictions()</code> function we saw in lab doesn’t calculate standard errors so it isn’t very useful in this scenario. However <code>data_grid()</code> may be. If you don’t provide variables it will pick “typical” values for all those needed in <code>.model</code>:</p>
<pre class="r"><code>library(modelr)
data_grid(fat, .model = lmod)</code></pre>
<pre><code>## # A tibble: 1 x 13
##     age weight height  neck chest abdom   hip thigh  knee ankle biceps
##   &lt;int&gt;  &lt;dbl&gt;  &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt;  &lt;dbl&gt;
## 1    43   176.     70    38  99.6  91.0  99.3    59  38.5  22.8   32.0
## # … with 2 more variables: forearm &lt;dbl&gt;, wrist &lt;dbl&gt;</code></pre>
<p>It returns a tibble so it works nicely with <code>predict()</code> without having to do any <code>t()</code> or <code>data.frame()</code> calls:</p>
<pre class="r"><code>typical_man &lt;- data_grid(fat, .model = lmod)
predict(lmod, newdata = typical_man, interval = &quot;prediction&quot;)</code></pre>
<pre><code>##        fit     lwr      upr
## 1 17.49322 9.61783 25.36861</code></pre>
<p>The broom package has an <code>augment()</code> function that will add predictions and their standard errors and return then in tibble form:</p>
<pre class="r"><code>library(broom)</code></pre>
<pre><code>## 
## Attaching package: &#39;broom&#39;</code></pre>
<pre><code>## The following object is masked from &#39;package:modelr&#39;:
## 
##     bootstrap</code></pre>
<pre class="r"><code>augment(lmod, newdata = typical_man)</code></pre>
<pre><code>##   age weight height neck chest abdom  hip thigh knee ankle biceps forearm
## 1  43  176.5     70   38 99.65 90.95 99.3    59 38.5  22.8  32.05    28.7
##   wrist  .fitted  .se.fit
## 1  18.3 17.49322 0.278665</code></pre>
<p>Notice the new columns <code>.fitted</code> and <code>.se.fit</code> which you could use to construct intervals.</p>
</div>
