\documentclass[]{exam}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Least squares estimates of the regression parameters},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\newcommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{Least squares estimates of the regression parameters}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{}
    \preauthor{}\postauthor{}
      \predate{\centering\large\emph}
  \postdate{\par}
    \date{2019-01-16}

\newcommand{\sumi}[1]{\ensuremath{\sum_{i=1}^n #1 }}
\newcommand{\E}[1]{\ensuremath{\text{E}\left( #1 \right)}}
\newcommand{\Var}[1]{\ensuremath{\text{Var}\left( #1 \right)}}
\newcommand{\Cov}[1]{\ensuremath{\text{Cov}\left( #1 \right)}}
\newcommand{\VarH}[1]{\ensuremath{\widehat{\text{Var}}\left( #1 \right)}}
\newcommand{\SE}[1]{\ensuremath{\text{SE}\left( #1 \right)}}
\newcommand{\RSS}[1]{\ensuremath{\text{RSS}_#1}}
\newcommand{\df}[1]{\ensuremath{\text{d.f.}_#1}}

\renewcommand*\familydefault{\sfdefault}
\setlength{\headsep}{0.5in}

\makeatletter
\@ifclassloaded{exam}
  { \pagestyle{headandfoot}
    \headrule
    }
  { \usepackage{fancyhdr}
    \pagestyle{fancy}
    \let\ps@plain\ps@fancy 
    }%
\makeatother

\lhead{\footnotesize \inserttitle}
\rhead{\footnotesize ST552: Statistical Methods II, Winter 2019}
\cfoot{\thepage}

\makeatletter
\let\inserttitle\@title
\makeatother

\begin{document}
\maketitle

\subsection{Warmup}\label{warmup}

Recall from last time we can set up a multiple linear regression model
in the matrix form: \[
y = X\beta + \epsilon  
\]

\textbf{Give the name and dimensions of each term}

\begin{solutionorbox}[1.5in]
$y_{n \times 1}$ the response vector, $X_{n \times p}$ the design matrix, $\beta_{p \times 1}$ the parameter vector, $\epsilon_{n \times 1}$ the error vector
\end{solutionorbox}

\subsection{Today's goal}\label{todays-goal}

Derive the form of the estimates for the parameter vector \(\beta\).

\subsection{Least Squares}\label{least-squares}

Just like in simple linear regression, we'll estimate \(\beta\) by
\textbf{least squares}. In simple linear regression this involved
finding \(\hat{\beta_0}\) and \(\hat{\beta_1}\) to minimise the sum of
squared residuals: \[
\text{sum of squared residuals SLR} = \sum_{i = 1}^{n} e_i^2 = \sum_{i = 1}^{n}\left( y_i - (\hat{\beta}_0 + \hat{\beta}_1 x_i) \right)^2
\]

\textbf{Your turn}: What procedure do you use to minimise a function?
E.g. if \(f(x)\) is a function of a single real value \(x\), how do you
find the \(x\) that minimises \(f(x)\)?

(\emph{2 min discussion})

\begin{solutionorbox}[1.5in]
1. Find derivative of $f(x)$ with respect to $x$, $\frac{\partial f(x)}{\partial x}$.  

2. Set the derivative to zero, and solve for $x$.

3. Check second derivative at minimum is positive, i.e. that this is a minimum.

\end{solutionorbox}\newpage

For multiple linear regression the least squares estimate of the
\(\beta\) is the \textbf{vector} \(\hat{\beta}\) that minimizes the sum
of squared residuals: \[
\text{sum of squared residuals MLR} = \sum_{i = 1}^{n} e_i^2 = ||e||^2 = \left(y - X\hat{\beta}\right)^T \left(y - X\hat{\beta}\right) 
\]

\textbf{Your turn} Expand the matrix product on the right into four
terms. Be careful with the order of matrix multiplication and recall
\(\left(uX\right)^T = X^Tu^T\).

\[
\sum_{i = 1}^{n} e_i^2 = ||e||^2 = \left(y - X\hat{\beta}\right)^T \left(y - X\hat{\beta}\right)
\]

\begin{solutionorbox}[1.5in]
$$
\sum_{i = 1}^{n} e_i^2 = ||e||^2 = y^Ty - \hat{\beta}^TX^Ty - y^TX\hat{\beta} + \hat{\beta}^T X^TX\hat{\beta}
$$
\end{solutionorbox}

Consider the terms: \[
-\hat{\beta}^TX^Ty \quad \text{and} - y^TX\hat{\beta}
\]

\textbf{Argue that these can be combined into the single term} \[
-2\hat{\beta}^TX^Ty
\]

(Hint: consider the dimensions of these terms)

\begin{solutionorbox}[1.5in]
Note the the dimensions of the terms
$$
 y^T_{1 \times n} X_{n \times n}\hat{\beta}_{p \times 1} 
$$
so the result is $1 \times 1$, a scalar, and therefore 
$$
y^TX\hat{\beta} = \left(y^TX\hat{\beta}  \right)^T = \hat{\beta}^TX^Ty
$$
\end{solutionorbox}

\subsection{Finding the minimum}\label{finding-the-minimum}

Now our objective is to find \(\hat{\beta}\) that minimises: \[
y^Ty - 2\hat{\beta}^TX^Ty + \hat{\beta}^T X^TX\hat{\beta}
\]

The usual procedure would be to take derivative with respect to
\(\hat{\beta}\), set to zero and solve for \(\hat{\beta}\).
\textbf{Except} \(\hat{\beta}\) is a \textbf{vector}! We need to use
vector calculus.

\subsection{Vector calculus}\label{vector-calculus}

You should be familiar with the usual differentiation rules for scalar
\(a\) and \(x\):

\begin{itemize}
\tightlist
\item
  \(\frac{\partial}{\partial x} a = 0\)
\item
  \(\frac{\partial}{\partial x} ax= a\)
\item
  \(\frac{\partial}{\partial x} ax^2= 2ax\)
\end{itemize}

There are analogs when we want to take derivative with respect to a
vector \(\mathbf{x}\):

\begin{itemize}
\tightlist
\item
  \(\frac{\partial}{\partial \mathbf{x}} a = 0\), where \(a\) is a
  scalar
\item
  \(\frac{\partial}{\partial \mathbf{x}} \mathbf{x}^Tu = u\), where
  \(u\) is a vector
\item
  \(\frac{\partial}{\partial \mathbf{x}} \mathbf{x}^TA\mathbf{x} = (A + A^T)\mathbf{x}\),
  where \(A\) is a matrix
\end{itemize}

\textbf{Use the rules above to take the derivative of the sum of squared
residuals with respect to the vector \(\hat{\beta}\)}

\[
\begin{aligned}
\frac{\partial}{\partial \hat{\beta}} \left( y^Ty - 2\hat{\beta}^TX^Ty  + \hat{\beta}^T X^TX\hat{\beta} \right) &= 
\end{aligned}
\]

\begin{solutionorbox}[1.5in]
$$
\begin{aligned}
\frac{\partial}{\partial \hat{\beta}} \left( y^Ty - 2\hat{\beta}^TX^Ty  + \hat{\beta}^T X^TX\hat{\beta} \right) &= 0 - 2X^Ty + (X^TX + (X^TX)^T)\hat{\beta} \\
 &= -2X^Ty + (X^TX + X^TX)\hat{\beta} \\
 &= -2X^Ty + 2X^TX\hat{\beta}\\
\end{aligned}
$$
\end{solutionorbox}

\subsection{Normal Equations}\label{normal-equations}

Setting the above derivative to zero leads to the \textbf{Normal
Equations}. The least squares estimates satisfy: \[
X^Ty = X^TX \hat{\beta}
\]

If \(X^TX\) is invertible, the least squares estimates are (\textbf{fill
me in}): \[
\hat{\beta} = \left(\phantom{X^T}\phantom{X} \right)^{-1}\phantom{X}^Ty
\]

If \(X\) has rank \(p\) then \(X^TX\) will be invertible.

\subsection{Fitted Values and
Residuals}\label{fitted-values-and-residuals}

\textbf{Plug in the least squares estimate for \(\hat{\beta}\) to find
the fitted values and residuals} \[
\begin{aligned}
\hat{y} = X\hat{\beta} = \\
\hat{\epsilon} = e = y - X\hat{\beta} =
\end{aligned}
\]

\begin{solutionorbox}[1.5in]
$$
\begin{aligned}
\hat{y} &= X\hat{\beta} \\
& = X(X^TX)^{-1}X^Ty \\
\hat{\epsilon} = e &= y - X\hat{\beta} \\
&= y - X(X^TX)^{-1}X^Ty \\ 
&= \left(I - X(X^TX)^{-1}X^T\right)y
\end{aligned}
$$
\end{solutionorbox}

\subsection{Hat matrix}\label{hat-matrix}

The hat matrix is: \[
H = X\left(X^TX\right)^{-1}X^T
\] named because it puts ``hats'' on the response, i.e.~multiplying the
response by the hat matrix gives the fitted values: \[
Hy = \hat{y}
\]

\textbf{Your Turn:} Show \(\left(I- H\right)X = \pmb{0}\)

\begin{solutionorbox}[1.5in]
$$
\begin{aligned}
\left(I - H\right)X &= \left(I -  X\left(X^TX\right)X^T\right)X \quad \text{expand }H \\
&= X -  X\left(X^TX\right)^{-1}X^TX \quad \text{distribute }X \\
&= X -  XI \quad \text{since } A^{-1}A = I \\
&= 0
\end{aligned}
$$
\end{solutionorbox}

Other properties of \(H\)

\begin{itemize}
\tightlist
\item
  \(H\) is symmetric, so is \((I-H)\)
\item
  \(H\) is idempotent (\(H^2 = H\)), and so is \(I-H\)
\item
  \(X\) is invariant under \(H\) (i.e. \(HX = X\))
\item
  \((I-H)H = H(I-H) = 0\)
\end{itemize}

You can use these results to argue that the residuals are orthogonal to
the columns of \(X\), i.e.~show \(e^TX = \pmb{0}\) \[
\begin{aligned}
e^TX &= ((I - H)Y)^TX \quad \text{plug in form for residuals} \\
 &= Y^T(I - H)^T X \quad \text{distribute transpose} \\
 &= Y^T(I - H) X \quad \text{symmetry} \\
 &= Y^T 0 \quad \text{from above} \\
 & = 0
\end{aligned}
\]

\subsection{Next time}\label{next-time}

What are the properties of the least squares estimates?


\end{document}
