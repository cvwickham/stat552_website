---
title: "Least squares estimates of the regression parameters"
date: "2019-01-16"
output: pdf_document
documentclass: exam
params:
  make_solutions: false
weeks: '2'
links:
  - ["worksheet/05-least-squares-solution.pdf", "Solutions"]
---



<div id="warmup" class="section level2">
<h2>Warmup</h2>
<p>Recall from last time we can set up a multiple linear regression model in the matrix form: <span class="math display">\[
y = X\beta + \epsilon  
\]</span></p>
<p><strong>Give the name and dimensions of each term</strong></p>

</div>
<div id="todays-goal" class="section level2">
<h2>Today’s goal</h2>
<p>Derive the form of the estimates for the parameter vector <span class="math inline">\(\beta\)</span>.</p>
</div>
<div id="least-squares" class="section level2">
<h2>Least Squares</h2>
<p>Just like in simple linear regression, we’ll estimate <span class="math inline">\(\beta\)</span> by <strong>least squares</strong>. In simple linear regression this involved finding <span class="math inline">\(\hat{\beta_0}\)</span> and <span class="math inline">\(\hat{\beta_1}\)</span> to minimise the sum of squared residuals: <span class="math display">\[
\text{sum of squared residuals SLR} = \sum_{i = 1}^{n} e_i^2 = \sum_{i = 1}^{n}\left( y_i - (\hat{\beta}_0 + \hat{\beta}_1 x_i) \right)^2
\]</span></p>
<p><strong>Your turn</strong>: What procedure do you use to minimise a function? E.g. if <span class="math inline">\(f(x)\)</span> is a function of a single real value <span class="math inline">\(x\)</span>, how do you find the <span class="math inline">\(x\)</span> that minimises <span class="math inline">\(f(x)\)</span>?</p>
<p>(<em>2 min discussion</em>)</p>

<p>For multiple linear regression the least squares estimate of the <span class="math inline">\(\beta\)</span> is the <strong>vector</strong> <span class="math inline">\(\hat{\beta}\)</span> that minimizes the sum of squared residuals: <span class="math display">\[
\text{sum of squared residuals MLR} = \sum_{i = 1}^{n} e_i^2 = ||e||^2 = \left(y - X\hat{\beta}\right)^T \left(y - X\hat{\beta}\right) 
\]</span></p>
<p><strong>Your turn</strong> Expand the matrix product on the right into four terms. Be careful with the order of matrix multiplication and recall <span class="math inline">\(\left(uX\right)^T = X^Tu^T\)</span>.</p>
<p><span class="math display">\[
\sum_{i = 1}^{n} e_i^2 = ||e||^2 = \left(y - X\hat{\beta}\right)^T \left(y - X\hat{\beta}\right)
\]</span></p>

<p>Consider the terms: <span class="math display">\[
-\hat{\beta}^TX^Ty \quad \text{and} - y^TX\hat{\beta}
\]</span></p>
<p><strong>Argue that these can be combined into the single term</strong> <span class="math display">\[
-2\hat{\beta}^TX^Ty
\]</span></p>
(Hint: consider the dimensions of these terms)

</div>
<div id="finding-the-minimum" class="section level2">
<h2>Finding the minimum</h2>
<p>Now our objective is to find <span class="math inline">\(\hat{\beta}\)</span> that minimises: <span class="math display">\[
y^Ty - 2\hat{\beta}^TX^Ty + \hat{\beta}^T X^TX\hat{\beta}
\]</span></p>
<p>The usual procedure would be to take derivative with respect to <span class="math inline">\(\hat{\beta}\)</span>, set to zero and solve for <span class="math inline">\(\hat{\beta}\)</span>. <strong>Except</strong> <span class="math inline">\(\hat{\beta}\)</span> is a <strong>vector</strong>! We need to use vector calculus.</p>
</div>
<div id="vector-calculus" class="section level2">
<h2>Vector calculus</h2>
<p>You should be familiar with the usual differentiation rules for scalar <span class="math inline">\(a\)</span> and <span class="math inline">\(x\)</span>:</p>
<ul>
<li><span class="math inline">\(\frac{\partial}{\partial x} a = 0\)</span></li>
<li><span class="math inline">\(\frac{\partial}{\partial x} ax= a\)</span></li>
<li><span class="math inline">\(\frac{\partial}{\partial x} ax^2= 2ax\)</span></li>
</ul>
<p>There are analogs when we want to take derivative with respect to a vector <span class="math inline">\(\mathbf{x}\)</span>:</p>
<ul>
<li><span class="math inline">\(\frac{\partial}{\partial \mathbf{x}} a = 0\)</span>, where <span class="math inline">\(a\)</span> is a scalar</li>
<li><span class="math inline">\(\frac{\partial}{\partial \mathbf{x}} \mathbf{x}^Tu = u\)</span>, where <span class="math inline">\(u\)</span> is a vector</li>
<li><span class="math inline">\(\frac{\partial}{\partial \mathbf{x}} \mathbf{x}^TA\mathbf{x} = (A + A^T)\mathbf{x}\)</span>, where <span class="math inline">\(A\)</span> is a matrix</li>
</ul>
<p><strong>Use the rules above to take the derivative of the sum of squared residuals with respect to the vector <span class="math inline">\(\hat{\beta}\)</span></strong></p>
<p><span class="math display">\[
\begin{aligned}
\frac{\partial}{\partial \hat{\beta}} \left( y^Ty - 2\hat{\beta}^TX^Ty  + \hat{\beta}^T X^TX\hat{\beta} \right) &amp;= 
\end{aligned}
\]</span></p>

</div>
<div id="normal-equations" class="section level2">
<h2>Normal Equations</h2>
<p>Setting the above derivative to zero leads to the <strong>Normal Equations</strong>. The least squares estimates satisfy: <span class="math display">\[
X^Ty = X^TX \hat{\beta}
\]</span></p>
<p>If <span class="math inline">\(X^TX\)</span> is invertible, the least squares estimates are (<strong>fill me in</strong>): <span class="math display">\[
\hat{\beta} = \left(\phantom{X^T}\phantom{X} \right)^{-1}\phantom{X}^Ty
\]</span></p>
<p>If <span class="math inline">\(X\)</span> has rank <span class="math inline">\(p\)</span> then <span class="math inline">\(X^TX\)</span> will be invertible.</p>
</div>
<div id="fitted-values-and-residuals" class="section level2">
<h2>Fitted Values and Residuals</h2>
<p><strong>Plug in the least squares estimate for <span class="math inline">\(\hat{\beta}\)</span> to find the fitted values and residuals</strong> <span class="math display">\[
\begin{aligned}
\hat{y} = X\hat{\beta} = \\
\hat{\epsilon} = e = y - X\hat{\beta} =
\end{aligned}
\]</span></p>

</div>
<div id="hat-matrix" class="section level2">
<h2>Hat matrix</h2>
<p>The hat matrix is: <span class="math display">\[
H = X\left(X^TX\right)^{-1}X^T
\]</span> named because it puts “hats” on the response, i.e. multiplying the response by the hat matrix gives the fitted values: <span class="math display">\[
Hy = \hat{y}
\]</span></p>
<p><strong>Your Turn:</strong> Show <span class="math inline">\(\left(I- H\right)X = \pmb{0}\)</span></p>

<p>Other properties of <span class="math inline">\(H\)</span></p>
<ul>
<li><span class="math inline">\(H\)</span> is symmetric, so is <span class="math inline">\((I-H)\)</span></li>
<li><span class="math inline">\(H\)</span> is idempotent (<span class="math inline">\(H^2 = H\)</span>), and so is <span class="math inline">\(I-H\)</span></li>
<li><span class="math inline">\(X\)</span> is invariant under <span class="math inline">\(H\)</span> (i.e. <span class="math inline">\(HX = X\)</span>)</li>
<li><span class="math inline">\((I-H)H = H(I-H) = 0\)</span></li>
</ul>
<p>You can use these results to argue that the residuals are orthogonal to the columns of <span class="math inline">\(X\)</span>, i.e. show <span class="math inline">\(e^TX = \pmb{0}\)</span> <span class="math display">\[
\begin{aligned}
e^TX &amp;= ((I - H)Y)^TX \quad \text{plug in form for residuals} \\
 &amp;= Y^T(I - H)^T X \quad \text{distribute transpose} \\
 &amp;= Y^T(I - H) X \quad \text{symmetry} \\
 &amp;= Y^T 0 \quad \text{from above} \\
 &amp; = 0
\end{aligned}
\]</span></p>
</div>
<div id="next-time" class="section level2">
<h2>Next time</h2>
<p>What are the properties of the least squares estimates?</p>
</div>
