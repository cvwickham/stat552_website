---
title: "Case influence: examples"
date: "2019-02-15"
output: pdf_document
params:
  show_solutions: false
---

```{r, include = FALSE, eval = FALSE}
library(here)
rmarkdown::render(
  here("content", "worksheet", "17-examples.Rmd"),
  output_file = here("content", "worksheet", "17-examples-solution.pdf"),
  params = list(show_solutions = TRUE))
```
  
```{r, include = FALSE}
# rmarkdown::render("17-examples.Rmd", "pdf_document")
knitr::opts_chunk$set(echo=FALSE, message = FALSE, results = "hide",dev = 'pdf', warning = FALSE, fig.width = 3.5, fig.height = 3.5)
library(tidyverse)
```

```{r, include = !params$show_solutions}
set.seed(18171)
defs <- list(theme_bw(),
  ylab("Response"), 
  xlab("Explanatory"))


n <- 30
df <- data.frame(x1 = rnorm(n, mean = 10, sd = 4),
  x2 = runif(n, 0, 20))

df_1 <- df
df_1$y <- df_1$x1 + rnorm(n)
p1 <- qplot(x1, y, data = df_1, alpha = I(0.8)) +
  annotate("point", 10, 10, size = 10, shape = "+") +
  defs 
p1
p1a <- p1 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed") +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_1, c(10, 10, 10)))

df_2 <- df
df_2$y <- -df_2$x1 + rnorm(n)
p2 <- qplot(x1, y, data = df_2, alpha = I(0.8)) +
  annotate("point", 10, 0, size = 10, shape = "+") +
  defs
p2
p2a <- p2 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed") +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_2, c(10, 10, 0)))
```

```{r, include = !params$show_solutions}
df_3 <- df
df_3$y <- -df_3$x2 + rnorm(n, sd = 3)
p3 <- qplot(x2, y, data = df_3, alpha = I(0.8)) +
  annotate("point", -5, 5, size = 10, shape = "+") +
  defs
p3
p3a <- p3 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed",
  fullrange = TRUE) +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_3, c(-5, -5, 5)),
  fullrange = TRUE)

df_4 <- df
df_4$y <- df_4$x2 + rnorm(n, sd = 3)
p4 <- qplot(x2, y, data = df_4, alpha = I(0.8)) +
  annotate("point", 30, 5, size = 10, shape = "+") +
  defs
p4
p4a <- p4 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed",
  fullrange = TRUE) +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_4, c(30, 30, 5)),
  fullrange = TRUE)
```

```{r, include = !params$show_solutions}
df_5 <- df_4
p5 <- qplot(x2, y, data = df_5, alpha = I(0.8)) +
  annotate("point", 30, 5, size = 10, shape = "+") +
  annotate("point", 30, 7) +
  annotate("point", 30, 3) +
  defs+
  ylim(0, 30)
p5
p5a <- p5 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed",
  fullrange = TRUE,
  data = rbind(df_5, c(30, 30, 6), c(30, 30, 4))) +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_5, c(30, 30, 5), c(30, 30, 6), c(30, 30, 4)),
  fullrange = TRUE) 

df_6 <- data.frame(x1 = rnorm(1000, mean = 10, sd = 4))
df_6$y <- 0.5*df_6$x1 + rnorm(1000, sd = 2)
p6 <- qplot(x1, y, data = df_6, alpha = I(0.8)) +
  annotate("point", 30, 5, size = 10, shape = "+") +
  defs
p6
p6a <- p6 + geom_smooth(method ="lm", se = FALSE, linetype = "dashed",
  fullrange = TRUE) +
  geom_smooth(method ="lm", se = FALSE, 
    data = rbind(df_6, c(30, 5)),
  fullrange = TRUE)
```

```{r, include = params$show_solutions}
p1a
p2a

p3a
p4a

p5a
p6a
```