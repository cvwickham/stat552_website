---
title: Week 2  
draft: true
---
  
  
## Mon

### Learning Objectives

By the end of class a student should be able to:

* State the multiple regression model in matrix form along with the assumptions on the errors and design matrix.

* Describe the entries in the design matrix given a model and study description.

### Outline

1. **Matrix warmup** Have the warmup worksheet on projector as students arrive. At 3:00pm, introduce yourself, then give another 5mins for warmup and students to compare answers.
    
    You don't need to go over all the answers but remind students: 
    * Dimensions must be the same for addition.
    * Dimension must conform for multiplication (i.e. inner dimensions are the same)
    * Order matters with matrix multiplication: $bX \ne Xb$ in general.
    * $IX = X$ for any $X$ as long as dimensions work
    * $Y_{n \times n}^{-1}$ doesn't always exist! Exists if $\det(Y) \ne 0 \iff Y$ is full rank.
    
    
2. **Multiple linear regression** Write simple linear regression model on board:
    $$
    y_i = \beta_0 + \beta_1 x_i + \epsilon_i \quad i = 1, \ldots, n
    $$
    Now we want to add more than one explanatory,  relabel $x_i$ as $x_{i1}$, erase $\epsilon_i$, add $\beta_2x_{i2}$, add more (dot dot dot) up to $\beta_{p-1}x_{i(p-1)}$, add back on $\epsilon_i$.
       
    Also explain:
    
    * We have $p-1$ explanatories, so with intercept, we have $p$ regression coefficients.
    * Same assumptions on $\epsilon_i$ as simple linear regression - i.i.d expected value 0, variance $\sigma^2$.
   
3. **Concrete example** Go over Galapogos example in slides. Emphasize keeping track of which index corresponds to which explanatory is hard, so often just use variable name.  
    
    Give 2mins for students to answer "Your turn" questions with neighbour.  Go over answers together:
    
    * What does $i$ index? Islands. 
    * What is the value of $n$? 30, since there are 30 islands.
    * What is the value of $p$? $p = 6$, six parameters, five explanatory variables plus an intercept.

4. **Matrix form** Either on board, or using slide on doc cam, put up multiple linear regression model in full matrix form (i.e. with all rows and columns).
    
    Work through first row, show that it is the same as previous model for $i = 1$.  Give abbreviated form $y = X\beta + \epsilon$, go through matrix dimensions, and names (repsone, random errors, paramter vector, design matrix).
    
5.  **Galapogos Matrix form**  Go through concrete example of matrix form for this example.  Label columns with variable names.  

6. **Other models** Give students 5-6 minutes to work on your turn.  Go over answers together:
    ```{r, include = FALSE}
    library(faraway)
    matrix_inner <- function(X){
      paste(apply(X, 1, paste0, collapse = " & "),
        collapse = "\\\\ \n ")
    }
    X_1 <- model.matrix(Species ~ Area + Nearest, 
      data = gala)[1:5, ]
    X_2 <- model.matrix(Species ~ 0 + Area + I(Area^2),
      data = gala)[1:5, ]
    X_3 <- model.matrix(Species ~ I(Area > 1), 
      data = gala)[1:5, ]
    ```
    
    $$
    X_{30\times 3} = \left(
    \begin{array}{rrr}
      `r matrix_inner(X_1)` \\
      \vdots & \vdots &  \vdots  \\
    \end{array}
    \right)
    $$
    Easy, just a subset of the columns in the previous example.
    
    $$
    X_{30\times 2} = \left(
    \begin{array}{rrr}
      `r matrix_inner(X_2)` \\
      \vdots & \vdots \\
    \end{array}
    \right)
    $$
    We don't have to have an intercept, but be careful if you don't, $p$ is the same as the number of explanatory variables, $\beta$ starts indexing at 1.
    
    We are also allowed to transform variables to make new ones.  In this case allowing a quadratic relationship to area.

    $$
    X_{30\times 2} = \left(
    \begin{array}{rrr}
      `r matrix_inner(X_3)` \\
      \vdots & \vdots \\
    \end{array}
    \right)
    $$
    Indicators are OK too, and is how we'll handle categorical variables later.  In this example, we've allowed in a different mean number of species for islands with area greater than 1.

6. **Fitted values and residuals** If we had estimate for $\beta$, define fitted value and residual vectors. Get students to workout dimensions if you have time.

7. **Wrap up** Motivate the rest of week:
    
    * How will we estimate $\beta$? 
    * What properties will the estimates have.

\newpage 

## Wed

### Learning Objectives

By the end of class a student should be able to:

* Derive the least squares estimates.

* Define fitted values and residuals.

* Describe the difference between random errors and residuals.


### Outline

The handout provided to students has a mixture of short notes and exercises.  I suggest getting the student's into small groups.  Then alternate between you setting up the next exercise with the whole class (i.e. go over the note part), and the student's spending some time working on the exercise (filling in the boxes). 

Alternatively, you might just unleash them on the handout, and gradually fill in the blanks as they progress (without stopping the class, but where they can see it), and if they have questions either help groups indivdually or go over a step with the class.

There is a lot on the handout! I expect you'll definitely get up to "The Hat Matrix".  If you don't get through the whole thing, just finish it up on Friday.  

I'll post a version with all the solutions after class.

**Before class ends** Emphasize:

* Finding the least squares estimates is just algebra and calculus (albeit slightly out of the normal algebra and calculus).  We didn't make any statistical assumptions to get them.

* The estimates have a closed form $\hat{\beta} = \left(X^TX \right)^{-1}X^Ty$ and they should memorize it.

* Because $\epsilon$ is a random vector, $y$ is a random vector, and hence our estimates are random vectors and we can talk about their statistical properties.

\newpage

## Fri

### Learning Objectives

By the end of class a student should be able to:

* Derive the mean and variance-covariance matrix of the least squares estimates in multiple linear regression.

* State the Gauss-Markov theorem and discuss it's consequences in practice.

* State the form and properties of the estimate for the variance of the errors.

### Outline

Run Friday just like Wednesday.

Depending on how time goes you might fill the in blanks for them when deriving the variance-covariance matrix of the estimates.

**Before class ends** Emphasize the properties we derived: least squares estimates are **unbiased** and we have a form for their variance-covariance matrix.





