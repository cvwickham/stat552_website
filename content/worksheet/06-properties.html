---
title: "Properties of the least squares estimates"
date: "2019-01-18"
output: pdf_document
documentclass: exam
params:
  make_solutions: false
weeks: '2'
links:
  - ["worksheet/06-properties-solution.pdf", "Solutions"]
---



<div id="warmup" class="section level2">
<h2>Warmup</h2>
<p>Let <span class="math inline">\(a\)</span> and <span class="math inline">\(b\)</span> be scalar constants, and <span class="math inline">\(X\)</span> be a scalar random variable.</p>
<p><strong>Fill in the blanks</strong> <span class="math display">\[
\begin{aligned}
\E{aX + b} &amp;= \underline{\phantom{a \E{X} + b}} \\
\Var{aX + b} &amp;= \underline{\phantom{a^2 \Var{X}}}
\end{aligned}
\]</span></p>

</div>
<div id="goal" class="section level2">
<h2>Goal</h2>
<p>Recall that the least squares estimates are: <span class="math display">\[
\hat{\beta}_{p\times1} = \left( X^TX \right)^{-1} X^Ty
\]</span></p>
<p>Our goal today is to learn about the statistical properties of these estimates, in particular their expectation and variance.</p>
</div>
<div id="random-vectors" class="section level2">
<h2>Random Vectors</h2>
<p><span class="math inline">\(\hat{\beta}\)</span> is a vector-valued random variable, so we first need to cover a little more background.</p>
<p>Let <span class="math inline">\(U_1, \ldots, U_n\)</span> be scalar random variables. Then the vector <span class="math display">\[
\mathbf{U} = \left( U_1, \ldots, U_n \right)^T
\]</span> is a vector valued random variable, a.k.a. a <strong>random vector</strong>.</p>
<p>The expectation of <span class="math inline">\(\mathbf{U}\)</span> is the vector of expectations, <span class="math display">\[
\E{\mathbf{U}} = \left(\E{U_1}, \ldots, \E{U_n} \right)^T
\]</span></p>
<p>And the variance-covariance matrix of <span class="math inline">\(\mathbf{U}\)</span> is <span class="math display">\[
\Var{\mathbf{U}} = \Cov{\mathbf{U}} = \begin{pmatrix}
\Var{U_1} &amp; \Cov{U_1, U_2} &amp; \cdots &amp; \Cov{U_1, U_n} \\
\Cov{U_2, U_1} &amp; \Var{U_2} &amp; \cdots &amp; \Cov{U_2, U_n} \\
\vdots &amp; \vdots  &amp; \ddots &amp; \vdots  \\
\Cov{U_n, U_1} &amp; \Cov{U_n, U_2}  &amp; \cdots &amp; \Var{U_n} 
\end{pmatrix}_{n\times n}
\]</span></p>
<p>For example, the errors in multiple linear regression, <span class="math inline">\(\epsilon_i,\, i = 1, \ldots, n\)</span> are independent with mean 0 and variance <span class="math inline">\(\sigma^2\)</span>.</p>
<p>Then, <span class="math display">\[
\epsilon = \begin{pmatrix}
\epsilon_1 \\
\epsilon_2 \\
\vdots \\
\epsilon_n 
\end{pmatrix}, \quad
\E{\epsilon} = \begin{pmatrix}
0 \\
0 \\
\vdots \\
0 
\end{pmatrix} = \mathbf{0}, \quad
\Var{\epsilon} = \begin{pmatrix}
\sigma^2 &amp; 0 &amp; \cdots &amp; 0 \\
0 &amp; \sigma^2 &amp; \cdots &amp; 0 \\
\vdots &amp; \vdots &amp; \ddots &amp; \vdots\\
0 &amp; 0 &amp; 0 &amp; \sigma^2
\end{pmatrix} = \sigma^2 I_{n}
\]</span></p>
</div>
<div id="properties-of-expectation-and-variance-for-random-vectors" class="section level2">
<h2>Properties of Expectation and Variance for random vectors</h2>
<p>Let:</p>
<ul>
<li><span class="math inline">\(\mathbf{U}_{n \times 1}\)</span> be a random vector</li>
<li><span class="math inline">\(A_{m \times n}\)</span> be a constant matrix</li>
<li><span class="math inline">\(b_{m \times 1}\)</span> be a constant vector</li>
</ul>
<p>Then: <span class="math display">\[
\begin{aligned}
\E{A\mathbf{U} + b} = A\E{\mathbf{U}} + b  \\
\Var{A\mathbf{U} + b} = A\Var{\mathbf{U}}A^T
\end{aligned}
\]</span></p>
<p>These are the vector analogs of the properties you wrote down in the warmup.</p>
<p><strong>Find <span class="math inline">\(\E{y}\)</span> and <span class="math inline">\(\Var{y}\)</span></strong> where <span class="math inline">\(y_{n \times 1}\)</span> satisfies the multiple linear regression equation: <span class="math display">\[
y = X\beta + \epsilon
\]</span> and <span class="math inline">\(\E{\epsilon} = \mathbf{0}\)</span>, <span class="math inline">\(\Var{\epsilon} = \sigma^2I\)</span></p>


</div>
<div id="expectation-of-the-least-squares-estimates" class="section level2">
<h2>Expectation of the least squares estimates</h2>
<p>Assume the regression set up (with the usual dimensions): <span class="math display">\[
y = X\beta + \epsilon
\]</span> where <span class="math inline">\(X\)</span> is fixed with rank <span class="math inline">\(p\)</span>, <span class="math inline">\(\E{\epsilon} = 0\)</span>, and <span class="math inline">\(\Var{\epsilon} = \sigma^2 I_n\)</span>.</p>
<p><strong>Fill in the blanks to show the least squares estimates are unbiased</strong></p>
<p><span class="math display">\[
\begin{aligned}
\E{\hat{\beta}} &amp; = \E{\left(X^TX\right)^{-1}X^Ty} \\[10mm]
&amp; = \E{ \left(X^TX\right)^{-1}X^T \left(\phantom{X\beta + \epsilon} \right)}  \quad \text{plug in the regression equation for } y \\[10mm]
&amp; = \E{ \phantom{\left(X^TX\right)^{-1}X^T X \beta} + 
        \phantom{\left(X^TX\right)^{-1}X^T \epsilon }} \quad \text{expand} \\[10mm]
&amp; = \E{  \phantom{\beta} + 
        \phantom{\left(X^TX\right)^{-1}X^T \epsilon }} \quad \text{simplify term on the left } A^{-1}A = I \\[10mm]
&amp; =  \phantom{\beta} + 
        \phantom{\left(X^TX\right)^{-1}X^T} \E{ \phantom{\epsilon} } \quad \text{property of expectation} \\[10mm]
&amp; = \phantom{\beta + 
        \left(X^TX\right)^{-1}X^T \mathbf{0}} \quad \text{regression assumptions} \\[10mm]
&amp; = \beta
\end{aligned}
\]</span></p>


</div>
<div id="variance-covariance-matrix-of-the-least-square-estimates" class="section level2">
<h2>Variance-covariance matrix of the least square estimates</h2>
<p><strong>Fill in the blanks to find the variance covariance matrix of least squares estimates</strong></p>
<span class="math display">\[
\begin{aligned}
\Var{\hat{\beta}} &amp;= \Var{\left(X^TX\right)^{-1}X^Ty} \\ 
&amp;= \Var{\left(X^TX\right)^{-1}X^T X \beta + 
        \left(X^TX\right)^{-1}X^T \epsilon} \quad \text{plug in reg. eqn. and expand}\\[10mm]
&amp;= 0 + \bigl(\phantom{\left(X^TX\right)^{-1}X^T}\bigr) \Var{\epsilon} \bigl(\phantom{\left(X^TX\right)^{-1}X^T} \bigr)^T \quad \text{property of Var}\\[10mm]
&amp; = \bigl(\phantom{\left(X^TX\right)^{-1}X^T}\bigr) \phantom{\sigma^2 I} \bigl(\phantom{\left(X^TX\right)^{-1}X^T} \bigr)^T \quad \text{regression assumption}\\[10mm]
&amp; = \phantom{\sigma^2}\bigl(\phantom{\left(X^TX\right)^{-1}X^T}\bigr)  \bigl(\phantom{\left(X^TX\right)^{-1}X^T} \bigr)^T  \quad \text{move scalar to front}\\[10mm]
&amp; = \sigma^2 \bigl(\phantom{\left(X^TX\right)^{-1}X^T}\bigr) \phantom{X \left(X^TX\right)^{-1}}  \quad \text{distribute transpose}\\[10mm]
&amp;= \sigma^2 \phantom{\left(X^TX\right)^{-1}}   \quad \text{since } A^{-1}A = I\\[10mm]
&amp; = \sigma^2 \left(X^TX\right)^{-1}
\end{aligned}
\]</span>

<p>We can pull out the variance of a particular parameter estimate, say <span class="math inline">\(\hat{\beta_i}\)</span>, from the diagonal of the matrix: <span class="math display">\[
\Var{\beta_i} = \sigma^2(X^TX)^{-1}_{i+1i+1}
\]</span> where <span class="math inline">\(A_{ij}\)</span> indicates the element in the i’th row and j’th column of the matrix <span class="math inline">\(A\)</span>.</p>
<p><strong>Why <span class="math inline">\(i+1\)</span></strong>?</p>
<p>The off diagonal terms tell us about the covariance between parameter estimates.</p>
</div>
<div id="estimating-sigma" class="section level2">
<h2>Estimating <span class="math inline">\(\sigma\)</span></h2>
<p>To make use of the variance-covariance results we need to be able to estimate <span class="math inline">\(\sigma^2\)</span>.</p>
<p>An unbiased estimate is: <span class="math display">\[
\hat{\sigma}^2 = \frac{1}{n-p}\sum_{i =1}^{n}{e_i^2} = \frac{||e||^2}{n-p}
\]</span></p>
<p>The denominator <span class="math inline">\(n-p\)</span> is known as the model <strong>degrees of freedom</strong>.</p>
</div>
<div id="standard-errors-of-particular-parameters" class="section level2">
<h2>Standard errors of particular parameters</h2>
<p>The standard error of a particular parameter is then the squareroot of the variance replacing <span class="math inline">\(\sigma^2\)</span> with its estimate: <span class="math display">\[
\SE{\beta_i} = \hat{\sigma}^2 \sqrt{(X^TX)^{-1}_{i+1i+1}}
\]</span></p>
</div>
<div id="gauss-markov-theorem" class="section level2">
<h2>Gauss Markov Theorem</h2>
<p>You might wonder if we can find estimates with better properties. The Guass-Markov theorem says the least squares estimates are BLUE (<strong>B</strong>est <strong>L</strong>inear <strong>U</strong>nbiased <strong>E</strong>stimator).</p>
<blockquote>
<p>Of all linear, unbiased estimates, the least squares estimates have the smallest variance.</p>
</blockquote>
<p>Of course if you are willing to have a non-linear estimate and/or, a biased estimate you might be able to find an estimate with smaller variance.</p>
<p><em>Proof, see Section 2.8 in Faraday</em></p>

</div>
<div id="summary" class="section level2">
<h2>Summary</h2>
<p>For the linear regression model <span class="math display">\[
Y = X\beta + \epsilon
\]</span> where <span class="math inline">\(\E{\epsilon} = 0_{n\times 1}\)</span>, <span class="math inline">\(\Var{\epsilon} = \sigma^2 I_n\)</span>, and the matrix <span class="math inline">\(X_{n \times p}\)</span> is fixed with rank <span class="math inline">\(p\)</span>.</p>
<p>The least squares estimates are <span class="math display">\[
\hat{\beta} = (X^TX)^{-1}X^TY
\]</span></p>
<p>Furthermore, the least squares estimates are BLUE, and <span class="math display">\[
\begin{aligned}
\E{\hat{\beta}} &amp;= \beta, \qquad \Var{\hat{\beta}} = \sigma^2 (X^TX)^{-1} \\
\E{\hat{\sigma}^2} &amp;= \E{\tfrac{1}{n-p}\sum_{i =1}^{n}{e_i^2}} = \sigma^2
\end{aligned}
\]</span></p>
<p>We have not used any Normality assumptions to show these properties.</p>
</div>
