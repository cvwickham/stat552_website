---
title: Lab 3
date: "2019-01-23"
weeks: "3"
weeks_weight: 2
types: "lab"
---



<pre class="r"><code>library(tidyverse)
library(broom)
library(gganimate)</code></pre>
<div id="learning-objectives" class="section level2">
<h2>Learning Objectives</h2>
<ul>
<li>Use simulation to demonstrate the properties of the least squares estimates</li>
<li>Explore the relationship between the covariance of the least squares estimates and the design matrix</li>
</ul>
</div>
<div id="general-idea" class="section level2">
<h2>General idea</h2>
<p>In this lab you’ll explore some of the properties of the least squares estimates that we have discussed in class through simulation.</p>
<p>Our general approach will be to:</p>
<ul>
<li><p>Fix the sample size <span class="math inline">\(n\)</span>, the design matrix <span class="math inline">\(X\)</span> and a model (values for <span class="math inline">\(\beta\)</span> and <span class="math inline">\(\sigma^2\)</span>) i.e. fix a simulation setting</p></li>
<li><p><strong>Repeat many times</strong>:</p>
<ul>
<li>simulate data by simulating the errors then generating the response according to the model</li>
<li>fit the regression model and extract the parameter estimates</li>
</ul></li>
<li><p>Examine the parameter estimates over the many simulated datasets</p></li>
</ul>
<p>In this lab we’ll focus on an <strong>ideal setting</strong>, that is, our errors will be i.i.d Normal and the model we fit will have the same form as the true model.</p>
</div>
<div id="simulating-one-example" class="section level2">
<h2>Simulating one example</h2>
<pre class="r"><code>n &lt;- 30</code></pre>
<p>Let’s walk through the simulation once before doing it many times. Let’s consider an experiment where we have 30 seedlings, which we randomly assign to receive either 2, 7, 12, 17 or 22 hours of artificial <code>sunlight</code> in a day. After four weeks, the plants are harvested, dried and weighed (in grams) to give a biomass <code>yield</code> response.</p>
<p>(<em>If you are working in the Rmarkdown document, you’ll see I defined <code>n</code> in a code chunk and then referenced it in the text with <code>`r n`</code>. This is one way to allow changes to the simulation set up that get directly reflected in the narrative.</em>)</p>
<pre class="r"><code>growth &lt;- tibble(
  sunlight = rep(seq(2, 22, by = 5), length = n)
)</code></pre>
<p>We’ll imagine that this simple linear regresssion model is appropriate in this setting: <span class="math display">\[
\text{yield}_i = \beta_0 + \beta_1\text{sunlight}_i + \epsilon_i \quad i = 1, \ldots, 30
\]</span> where <span class="math inline">\(\epsilon_i \sim_{\text{i.i.d}} N(0, \sigma^2)\)</span>.</p>
<pre class="r"><code>beta_0 &lt;- 5
beta_1 &lt;- 0.5
sigma_sq &lt;- 4</code></pre>
<p>For a simulation we’ll need to set values for <span class="math inline">\(\beta_0\)</span>, <span class="math inline">\(\beta_1\)</span> and <span class="math inline">\(\sigma^2\)</span>, let’s say <span class="math display">\[
\beta_0 = 5, \quad \beta_1 = 0.5, \quad \sigma^2 = 4
\]</span></p>
<p>(<em>You might argue if these are realistic…you are also welcome to suggest a better example</em>).</p>
<p>We can then simulate a single example of the experiment outcome:</p>
<pre class="r"><code>sim_1 &lt;- growth %&gt;% 
  mutate(
    error = rnorm(n, mean = 0, sd = sqrt(sigma_sq)),
    yield = beta_0 + beta_1 * sunlight + error
  )</code></pre>
<p>And take a look at the simulated data:</p>
<pre class="r"><code>ggplot(sim_1, aes(sunlight, yield)) +
  geom_point() +
  geom_smooth(method = &quot;lm&quot;, se = FALSE)</code></pre>
<p><img src="/lab/lab-3_files/figure-html/unnamed-chunk-6-1.png" width="576" /></p>
<p>We can fit a model to our data:</p>
<pre class="r"><code>fit_lm &lt;- lm(yield ~ sunlight, data = sim_1)</code></pre>
<p>And look at the result:</p>
<pre class="r"><code>summary(fit_lm)</code></pre>
<p>Our estimates obviously don’t match the truth, the question is: <strong>over many such experiments how do our estimates behave?</strong> We’ve derived the answer in class, but I think it helps to see it through simulation too.</p>
<p>It will be useful to use the <code>broom::tidy()</code> function which takes a model and produces a tidy tibble of the regression coefficient estiamtes</p>
<pre class="r"><code>tidy(fit_lm)</code></pre>
<pre><code>##          term  estimate std.error statistic      p.value
## 1 (Intercept) 4.7179088 0.5548150  8.503571 3.030119e-09
## 2    sunlight 0.5237286 0.0398334 13.147976 1.675672e-13</code></pre>
<p>We’ll just need the estimates and their labels which we can grab with <code>dplyr::select()</code></p>
<pre class="r"><code>tidy(fit_lm) %&gt;% 
  select(term, estimate)</code></pre>
<pre><code>##          term  estimate
## 1 (Intercept) 4.7179088
## 2    sunlight 0.5237286</code></pre>
</div>
<div id="simulating-many-times" class="section level2">
<h2>Simulating many times</h2>
<p>There are lot’s of different ways (code-wise) we could repeat this process many times. I tend to like the tidyverse style of using list columns in combination with <code>purrr:map()</code> and <code>dplyr::mutate()</code>, maybe you prefer for loops. I’ve got some code below (in the Rmarkdown) to repeat our simulation many times, but it’s not too important to understand all the steps.</p>
<p>It’s more important to me that you understand the process of the simulation. To help you visualize what is happening I’ve constructed this animation: <img src="/lab/lab-3_files/figure-html/unnamed-chunk-12-1.gif" /><!-- --></p>
<p>Each frame has one simulated data set on it (the black points), and the resulting model fit (the blue line). I’ve added the true model as the red line, and previous fits remain as pale grey lines. Your should see that although any individual line may not be that close to the true line, but on average, they tend to be good (i.e. the fitted lines are unbiased).</p>
<p><strong>Your turn</strong></p>
<p>In my simulation, the important piece of output is <code>sim_coefs</code>:</p>
<pre class="r"><code>sim_coefs</code></pre>
<pre><code>## # A tibble: 200 x 3
##      sim term        estimate
##    &lt;int&gt; &lt;chr&gt;          &lt;dbl&gt;
##  1     1 (Intercept)    5.07 
##  2     1 sunlight       0.486
##  3     2 (Intercept)    6.40 
##  4     2 sunlight       0.421
##  5     3 (Intercept)    6.65 
##  6     3 sunlight       0.400
##  7     4 (Intercept)    4.99 
##  8     4 sunlight       0.505
##  9     5 (Intercept)    5.31 
## 10     5 sunlight       0.526
## # ... with 190 more rows</code></pre>
<p>It has a row for each coefficient estimate, where <code>sim</code> indentifies which simulation the estimate came from. So for example, the estimates from the first simulation are:</p>
<pre class="r"><code>sim_coefs %&gt;% 
  filter(sim == 1)</code></pre>
<pre><code>## # A tibble: 2 x 3
##     sim term        estimate
##   &lt;int&gt; &lt;chr&gt;          &lt;dbl&gt;
## 1     1 (Intercept)    5.07 
## 2     1 sunlight       0.486</code></pre>
<p>We can investigate the parameters individually by looking at histograms of the estimates:</p>
<pre class="r"><code>ggplot(sim_coefs, aes(estimate)) +
  geom_histogram() +
  facet_wrap(~ term, scale = &quot;free_x&quot;)</code></pre>
<pre><code>## `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.</code></pre>
<p><img src="/lab/lab-3_files/figure-html/unnamed-chunk-15-1.png" width="576" /></p>
<p><strong>Your turn</strong> What does the theory say these should look like? Do they look reasonable?</p>
<p>We can also look at the relationship between the esitmate for the slope and intercept by plotting them on a scatterplot:</p>
<pre class="r"><code>sim_coefs %&gt;% 
  spread(term, estimate) %&gt;% 
  ggplot(aes(`(Intercept)`, sunlight)) +
    geom_point()</code></pre>
<p><img src="/lab/lab-3_files/figure-html/unnamed-chunk-16-1.png" width="576" /></p>
<p><strong>Your turn</strong> How would describe the relationship between the two estimates? Go back to the animation, can you give intuition as to why this happens?</p>
<p>Recall from class this covariance depends on <span class="math inline">\(\sigma^2\)</span> and <span class="math inline">\(X\)</span> through <span class="math display">\[
\text{Var}\left(\hat{\beta}\right) = \sigma^2 \left(X^T X \right)^{-1}
\]</span></p>
<p>We can get the design matrix <span class="math inline">\(X\)</span> from the original fit in our first simulation:</p>
<pre class="r"><code>X &lt;- model.matrix(fit_lm)</code></pre>
<p><strong>Your turn</strong>:</p>
<ul>
<li><p>Use the formula above to calculate the true variance-covariance matrix for the estimates.</p></li>
<li><p>Convert it to a correlation matrix with the <code>cov2cor()</code> function.</p></li>
<li><p>Does it agree with your observation from the scatterplot?</p></li>
</ul>
</div>
<div id="centering" class="section level2">
<h2>Centering</h2>
<p>We have some control over the form of <span class="math inline">\(X\)</span> from how we set up our regression model. In this section, you’ll explore the effect of centering the explanatory variable of the relationship between the estimates of the slope and intercept.</p>
<p>Centering the explanatory variable means subtracting the mean of the explanatory model before fitting, or in other words changing our model to: <span class="math display">\[
\text{yield}_i = \gamma_0 + \gamma_1\left(\text{sunlight}_i - \overline{\text{sunlight}}\right) + \epsilon_i \quad i = 1, \ldots, 30
\]</span></p>
<p>Visually, this is the new model:</p>
<pre class="r"><code>ggplot(sim_1, aes(sunlight - mean(sunlight), yield)) +
  geom_point() +
  geom_smooth(method = &quot;lm&quot;, se = FALSE)</code></pre>
<p><img src="/lab/lab-3_files/figure-html/unnamed-chunk-19-1.png" width="576" /></p>
<p>Notice we’ve just shifted the x-axis, otherwise everything is identical.</p>
<p>We can repeat the simulation now fitting this new centered model (see Rmarkdown for code).</p>
<p>Resulting in an similar object with the fitted coefficients:</p>
<pre class="r"><code>sim_coefs_centered</code></pre>
<pre><code>## # A tibble: 200 x 3
##      sim term                         estimate
##    &lt;int&gt; &lt;chr&gt;                           &lt;dbl&gt;
##  1     1 (Intercept)                    10.9  
##  2     1 I(sunlight - mean(sunlight))    0.486
##  3     2 (Intercept)                    11.5  
##  4     2 I(sunlight - mean(sunlight))    0.421
##  5     3 (Intercept)                    11.5  
##  6     3 I(sunlight - mean(sunlight))    0.400
##  7     4 (Intercept)                    11.1  
##  8     4 I(sunlight - mean(sunlight))    0.505
##  9     5 (Intercept)                    11.6  
## 10     5 I(sunlight - mean(sunlight))    0.526
## # ... with 190 more rows</code></pre>
<p><strong>Your turn</strong> How do the <span class="math inline">\(\gamma\)</span>s relate to the <span class="math inline">\(\beta\)</span>s in the orginal model? You might verify your intuition by comparing the values in <code>sim_coefs</code> to <code>sims_coefs_centered</code>.</p>
<p><strong>Your turn</strong>:</p>
<ul>
<li><p>Make a scatterplot of the estimated slope and intercept, how is the relationship different in this model?</p></li>
<li><p>Verify your observation by deriving the correlation between the estimates from the variance-covariance matrix.</p></li>
</ul>
<p>Setting up design matrices to have orthogonal columns is a large part of experimental design. It allows the the estimates of certain factors to be less dependent on which other factors are included in the model.</p>
</div>
<div id="further-exploration" class="section level2">
<h2>Further exploration</h2>
<p><strong>Your turn</strong></p>
<p>The following code pulls out the estimate of <span class="math inline">\(\sigma\)</span> for each simulation:</p>
<pre class="r"><code>sims_sigma_hats &lt;- sims %&gt;% 
  mutate(model_summary = map(fit, glance)) %&gt;% 
  unnest(model_summary) %&gt;% 
  select(sim, sigma, r.squared)</code></pre>
<p><strong>Can you verify <span class="math inline">\(\hat{\sigma^2}\)</span> is an unbiased estimate of <span class="math inline">\(\sigma^2\)</span>?</strong></p>
</div>
