---
title: Lab 4 - Visualizing models
date: "2019-01-30"
weeks: "4"
weeks_weight: 2
types: "lab"
---



<div id="learning-objectives" class="section level2">
<h2>Learning Objectives</h2>
<ul>
<li>Visualize a model by making predictions on a grid of explanatories.</li>
</ul>
<p>This is one useful strategy for understanding a model. A complementary strategy is to examine the model form mathematically, we’ll see a few examples in class on Friday.</p>
</div>
<div id="warmup" class="section level2">
<h2>Warmup</h2>
<pre class="r"><code>library(tidyverse)
library(modelr)</code></pre>
<p>Read more at: <a href="https://r4ds.had.co.nz/model-basics.html#visualising-models" class="uri">https://r4ds.had.co.nz/model-basics.html#visualising-models</a></p>
<p>For this lab we’ll work with some data from the openintro package. The dataset <code>bdims</code> contains body measurements of 507 physically active individuals. For the purposes of the lab we’ll look at trying to predict a physically active individuals weight.</p>
<pre class="r"><code>data(bdims, package = &quot;openintro&quot;) 
bdims &lt;- bdims %&gt;% 
  mutate(sex = as.factor(sex) %&gt;% fct_recode(male = &quot;1&quot;, female = &quot;0&quot;))
(bdims &lt;- as_tibble(bdims))</code></pre>
<p>Read more about the data at: <code>?openintro::bdims</code></p>
<p>Let’s start with the naive model: <span class="math display">\[
\text{weight}_i = \beta_0 + \beta_1 \text{height}_i + \epsilon_i, \quad i = 1, \ldots, 507
\]</span></p>
<p>Taking a quick look at the data:</p>
<pre class="r"><code>ggplot(bdims, aes(hgt, wgt)) +
  geom_point() </code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-3-1.png" width="576" /></p>
<p>This seems like a reasonable place to start.</p>
<p>We can fit the model to the data with:</p>
<pre class="r"><code>fit_hgt &lt;- lm(wgt ~ hgt, data = bdims)
summary(fit_hgt)</code></pre>
<pre><code>## 
## Call:
## lm(formula = wgt ~ hgt, data = bdims)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -18.743  -6.402  -1.231   5.059  41.103 
## 
## Coefficients:
##               Estimate Std. Error t value Pr(&gt;|t|)    
## (Intercept) -105.01125    7.53941  -13.93   &lt;2e-16 ***
## hgt            1.01762    0.04399   23.14   &lt;2e-16 ***
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
## 
## Residual standard error: 9.308 on 505 degrees of freedom
## Multiple R-squared:  0.5145, Adjusted R-squared:  0.5136 
## F-statistic: 535.2 on 1 and 505 DF,  p-value: &lt; 2.2e-16</code></pre>
<p><strong>Now, how do we visualize this fitted model in the context of the data?</strong></p>
<p>There are two common approaches:</p>
<ol style="list-style-type: decimal">
<li><p>Extract the coefficients and add a line with the corresponding slope and intercept:</p>
<pre class="r"><code>coefs &lt;- coef(fit_hgt)

ggplot(bdims, aes(hgt, wgt)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_abline(intercept = coefs[1], slope = coefs[2], color = &quot;#377EB8&quot;)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-5-1.png" width="576" /></p></li>
<li><p>Or, use the model to predict the response on a grid of explanatory values:</p>
<pre class="r"><code>(pred_linear &lt;- data_grid(bdims, hgt) %&gt;% 
  add_predictions(fit_hgt))</code></pre>
<pre><code>## # A tibble: 147 x 2
##      hgt  pred
##    &lt;dbl&gt; &lt;dbl&gt;
##  1  147.  44.8
##  2  150.  47.1
##  3  150.  47.5
##  4  151.  48.8
##  5  152   49.7
##  6  152.  50.1
##  7  153.  51.1
##  8  154.  52.1
##  9  154.  52.2
## 10  155.  52.6
## # ... with 137 more rows</code></pre>
<p>Then join the predictions with a line (Charlotte will build this plot up):</p>
<pre class="r"><code>ggplot(bdims, aes(hgt, wgt)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred), data = pred_linear, color = &quot;#377EB8&quot;) </code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-7-1.png" width="576" /></p></li>
</ol>
<p>The second approach tends to generalize to more complicated models more easily, and is the approach I’d recommend.</p>
</div>
<div id="modelr-provides-functions-to-help-with-this-approach" class="section level2">
<h2>modelr provides functions to help with this approach</h2>
<p>Let’s take a closer look at the code used in the second approach:</p>
<pre class="r"><code>pred_linear &lt;- data_grid(bdims, hgt) %&gt;% 
      add_predictions(fit_hgt)</code></pre>
<p>Two things are happening in this chunk: <code>data_grid()</code> from modelr is being used to create a tibble containing values at which we will make predictions, and <code>add_predictions()</code> also from modelr, is adding a column for the predictions based on our model at these values. The result is just a tibble with we can plot using our usual tools:</p>
<pre class="r"><code>pred_linear</code></pre>
<pre><code>## # A tibble: 147 x 2
##      hgt  pred
##    &lt;dbl&gt; &lt;dbl&gt;
##  1  147.  44.8
##  2  150.  47.1
##  3  150.  47.5
##  4  151.  48.8
##  5  152   49.7
##  6  152.  50.1
##  7  153.  51.1
##  8  154.  52.1
##  9  154.  52.2
## 10  155.  52.6
## # ... with 137 more rows</code></pre>
<p>Let’s explore <code>data_grid()</code> and <code>add_predictions()</code> in turn. <code>data_grid()</code> takes a data frame and one or more variable names. If you provide a single variable name, it returns the unique values that variable takes in the data:</p>
<pre class="r"><code>data_grid(data = bdims, hgt) </code></pre>
<pre><code>## # A tibble: 147 x 1
##      hgt
##    &lt;dbl&gt;
##  1  147.
##  2  150.
##  3  150.
##  4  151.
##  5  152 
##  6  152.
##  7  153.
##  8  154.
##  9  154.
## 10  155.
## # ... with 137 more rows</code></pre>
<p>Alternatively you can use <code>seq_range()</code> to get an evenly spaced grid of values:</p>
<pre class="r"><code>data_grid(bdims, hgt = seq_range(hgt, by = 10)) </code></pre>
<pre><code>## # A tibble: 6 x 1
##     hgt
##   &lt;dbl&gt;
## 1  147.
## 2  157.
## 3  167.
## 4  177.
## 5  187.
## 6  197.</code></pre>
<p>If you provide more than one variable, you’ll get all possible combinations of the two:</p>
<pre class="r"><code>(grid_hgt_sex &lt;- data_grid(bdims, 
  hgt = seq_range(hgt, by = 10), sex))</code></pre>
<pre><code>## # A tibble: 12 x 2
##      hgt sex   
##    &lt;dbl&gt; &lt;fct&gt; 
##  1  147. female
##  2  147. male  
##  3  157. female
##  4  157. male  
##  5  167. female
##  6  167. male  
##  7  177. female
##  8  177. male  
##  9  187. female
## 10  187. male  
## 11  197. female
## 12  197. male</code></pre>
<p>(<em>We’ll use this data grid shortly so I’m keeping it in <code>grid_hgt_sex</code></em>)</p>
<p>This grid specifies the point at which we want to make predictions, <code>add_predictions()</code> also from modelr, will add them if we specify a model:</p>
<pre class="r"><code>add_predictions(data = grid_hgt_sex, model = fit_hgt)</code></pre>
<pre><code>## # A tibble: 12 x 3
##      hgt sex     pred
##    &lt;dbl&gt; &lt;fct&gt;  &lt;dbl&gt;
##  1  147. female  44.8
##  2  147. male    44.8
##  3  157. female  55.0
##  4  157. male    55.0
##  5  167. female  65.1
##  6  167. male    65.1
##  7  177. female  75.3
##  8  177. male    75.3
##  9  187. female  85.5
## 10  187. male    85.5
## 11  197. female  95.7
## 12  197. male    95.7</code></pre>
<p>Notice the predictions end up in a columns called <code>pred</code> and our original columns are preserved.</p>
<p><em>(This is equivalent to <code>predict(fit_height, newdata = data_hgt_sex))</code> but returns a data frame with the original columns intact).</em></p>
<p>Plotting this data alone isn’t particularly useful:</p>
<pre class="r"><code>pred_hgt_sex &lt;- add_predictions(data = grid_hgt_sex, model = fit_hgt)

ggplot(pred_hgt_sex, aes(hgt, pred)) +
  geom_point()</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-13-1.png" width="576" /></p>
<p>But adding to a layer in our plot of the data helps us understand the model in the context of the data. Putting it all together:</p>
<pre class="r"><code>grid_hgt_sex &lt;- data_grid(bdims, hgt = seq_range(hgt, by = 10), sex) 
pred_hgt_sex &lt;- add_predictions(grid_hgt_sex, model = fit_hgt) 

ggplot(bdims, aes(hgt, wgt)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred), data = pred_hgt_sex)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-14-1.png" width="576" /></p>
</div>
<div id="your-turn---a-non-linear-relationship" class="section level2">
<h2>Your turn - a non-linear relationship</h2>
<p>A quadratic model in height: <span class="math display">\[
\text{weight}_i = \beta_0 + \beta_1 \text{height}_i + \beta_2 \text{height}_i^2 + \epsilon_i, \quad i = 1, \ldots, 507
\]</span></p>
<p>can be fit with:</p>
<pre class="r"><code>fit_height_quad &lt;- lm(wgt ~ hgt + I(hgt^2), data = bdims)</code></pre>
<p><strong>Use <code>add_predictions()</code> with <code>grid_hgt_sex</code> to add the fitted model to the plot of the data</strong></p>
<pre class="r"><code>grid_hgt_sex &lt;- data_grid(bdims, hgt = seq_range(hgt, n = 1000), sex) 
fit_height_poly &lt;- lm(wgt ~ poly(hgt, 10), data = bdims)
pred_poly &lt;- add_predictions(grid_hgt_sex, model = fit_height_poly) 

ggplot(bdims, aes(hgt, wgt)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred), data = pred_poly)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-16-1.png" width="576" /></p>
<pre class="r"><code># geom_fun</code></pre>
<p>(<em>How did I find a color?</em>)</p>
<pre class="r"><code>RColorBrewer::display.brewer.all() # choose a palette
RColorBrewer::brewer.pal(3, &quot;Set1&quot;) # extract one of the colors</code></pre>
</div>
<div id="visualizing-more-dimensions" class="section level2">
<h2>Visualizing more dimensions</h2>
<p>So far we’ve ignored <code>sex</code> but you might imagine it plays a role in the relationship between height and weight. Our current model predicts the same straight line relationship for both male and female:</p>
<pre class="r"><code>fit_hgt_sex_1 &lt;- lm(wgt ~ hgt * sex, data = bdims)
fit_hgt_sex_2 &lt;- lm(wgt ~ hgt + sex, data = bdims)

summary(fit_hgt_sex_1)</code></pre>
<pre><code>## 
## Call:
## lm(formula = wgt ~ hgt * sex, data = bdims)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -20.187  -5.957  -1.439   4.955  43.355 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(&gt;|t|)    
## (Intercept) -43.81929   13.77877  -3.180  0.00156 ** 
## hgt           0.63334    0.08351   7.584 1.63e-13 ***
## sexmale     -17.13407   19.56250  -0.876  0.38152    
## hgt:sexmale   0.14923    0.11431   1.305  0.19233    
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
## 
## Residual standard error: 8.795 on 503 degrees of freedom
## Multiple R-squared:  0.5682, Adjusted R-squared:  0.5657 
## F-statistic: 220.7 on 3 and 503 DF,  p-value: &lt; 2.2e-16</code></pre>
<pre class="r"><code>summary(fit_hgt_sex_2)</code></pre>
<pre><code>## 
## Call:
## lm(formula = wgt ~ hgt + sex, data = bdims)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -20.184  -5.978  -1.356   4.709  43.337 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(&gt;|t|)    
## (Intercept) -56.94949    9.42444  -6.043 2.95e-09 ***
## hgt           0.71298    0.05707  12.494  &lt; 2e-16 ***
## sexmale       8.36599    1.07296   7.797 3.66e-14 ***
## ---
## Signif. codes:  0 &#39;***&#39; 0.001 &#39;**&#39; 0.01 &#39;*&#39; 0.05 &#39;.&#39; 0.1 &#39; &#39; 1
## 
## Residual standard error: 8.802 on 504 degrees of freedom
## Multiple R-squared:  0.5668, Adjusted R-squared:  0.5651 
## F-statistic: 329.7 on 2 and 504 DF,  p-value: &lt; 2.2e-16</code></pre>
<pre class="r"><code>pred_sex_1 &lt;- add_predictions(grid_hgt_sex, model = fit_hgt_sex_1)
pred_sex_2 &lt;- add_predictions(grid_hgt_sex, model = fit_hgt_sex_2)

ggplot(bdims, aes(hgt, wgt, color = sex)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred), data = pred_sex_1)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-18-1.png" width="576" /></p>
<pre class="r"><code>ggplot(bdims, aes(hgt, wgt, color = sex)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred), data = pred_sex_2)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-18-2.png" width="576" /></p>
<p>If you want to put both models in one plot:</p>
<pre class="r"><code>pred_sex &lt;- add_predictions(grid_hgt_sex, model = fit_hgt_sex_1, &quot;pred_1&quot;) %&gt;% 
  add_predictions(fit_hgt_sex_2, &quot;pred_2&quot;)

ggplot(bdims, aes(hgt, wgt, color = sex)) +
  geom_point(size = 0.5, alpha = 0.5) +
  geom_line(aes(y = pred_1, linetype = &quot;M1&quot;), data = pred_sex) +
  geom_line(aes(y = pred_2, linetype = &quot;M2&quot;), data = pred_sex) 

## Or alternatively
pred_sex %&gt;% 
  gather(&quot;model&quot;, &quot;pred&quot;, pred_1, pred_2) %&gt;% 
  ggplot(aes(hgt, color = sex)) +
    geom_point(aes(y = wgt), size = 0.5, alpha = 0.5, data = bdims) +
    geom_line(aes(y = pred, linetype = model))</code></pre>
<p>Two possible ways of including <code>sex</code> in the regression model are:</p>
<pre class="r"><code>fit_hgt_sex_1 &lt;- lm(wgt ~ hgt * sex, data = bdims)
fit_hgt_sex_2 &lt;- lm(wgt ~ hgt + sex, data = bdims)</code></pre>
<p><strong>Your turn:</strong> Visualize the two models and describe the difference between them. Try to write down the models that are being fit.</p>
</div>
<div id="visualizing-residuals" class="section level2">
<h2>Visualizing residuals</h2>
<p>Visualizing predictions only tells us about the patterns the model has captured, but not the ones that it has failed to capture. Residuals are one way to provide this insight.</p>
<p>The modelr function <code>add_residuals()</code> takes two inputs like <code>add_predictions()</code> but now the input data is the observed data:</p>
<pre class="r"><code>add_residuals(bdims, fit_hgt_sex_1)</code></pre>
<pre><code>## # A tibble: 507 x 26
##    bia.di bii.di bit.di che.de che.di elb.di wri.di kne.di ank.di sho.gi
##     &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;  &lt;dbl&gt;
##  1   42.9   26     31.5   17.7   28     13.1   10.4   18.8   14.1   106.
##  2   43.7   28.5   33.5   16.9   30.8   14     11.8   20.6   15.1   110.
##  3   40.1   28.2   33.3   20.9   31.7   13.9   10.9   19.7   14.1   115.
##  4   44.3   29.9   34     18.4   28.2   13.9   11.2   20.9   15     104.
##  5   42.5   29.9   34     21.5   29.4   15.2   11.6   20.7   14.9   108.
##  6   43.3   27     31.5   19.6   31.3   14     11.5   18.8   13.9   120.
##  7   43.5   30     34     21.9   31.7   16.1   12.5   20.8   15.6   124.
##  8   44.4   29.8   33.2   21.8   28.8   15.1   11.9   21     14.6   120.
##  9   43.5   26.5   32.1   15.5   27.5   14.1   11.2   18.9   13.2   111 
## 10   42     28     34     22.5   28     15.6   12     21.1   15     120.
## # ... with 497 more rows, and 16 more variables: che.gi &lt;dbl&gt;,
## #   wai.gi &lt;dbl&gt;, nav.gi &lt;dbl&gt;, hip.gi &lt;dbl&gt;, thi.gi &lt;dbl&gt;, bic.gi &lt;dbl&gt;,
## #   for.gi &lt;dbl&gt;, kne.gi &lt;dbl&gt;, cal.gi &lt;dbl&gt;, ank.gi &lt;dbl&gt;, wri.gi &lt;dbl&gt;,
## #   age &lt;int&gt;, wgt &lt;dbl&gt;, hgt &lt;dbl&gt;, sex &lt;fct&gt;, resid &lt;dbl&gt;</code></pre>
<p>Notice there is now an additional column called <code>resid</code>.</p>
<p>Which we can then visualize, for example, here the residuals are plotted against height:</p>
<pre class="r"><code>add_residuals(bdims, fit_hgt_sex_1) %&gt;% 
  ggplot(aes(hgt, resid, color = sex)) +
    geom_ref_line(h = 0) +
    geom_point() </code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-22-1.png" width="576" /></p>
<p>If the model has failed to capture an important relationship between weight and height we’d expect to see a pattern in the average residual as a function of height. A line at zero has been added to help with the comparison. Looks pretty good here.</p>
<p><strong>Your turn</strong> Look at the residuals against the variable <code>wai.gi</code>. Does our model fail to capture something?</p>
<pre class="r"><code>add_residuals(data = bdims, model = fit_hgt_sex_1) %&gt;% 
  ggplot(aes(wai.gi, resid, color = sex)) +
    geom_ref_line(h = 0) +
    geom_point() </code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-23-1.png" width="576" /></p>
</div>
<div id="more-complicated-models" class="section level2">
<h2>More complicated models</h2>
<p>As models get more complicated:</p>
<ul>
<li><p>Visualizing more than three variables at a time is hard! A good strategy is to pick a couple of variables to visualize at a time, and make more than one plot. Or fix a variable at a meaningful value.</p></li>
<li><p>Use a mathematical examination of the model to influence your choice of interesting views. E.g. A parameter that only shifts the intercept might be omitted from a plot in favor of a parameter that changes the slope.</p></li>
<li><p>Make sure your grid has all the variables required for your model. If you pass a model to the <code>.model</code> argument in <code>data_grid()</code>, it will pick “typical” values for any needed variables that you don’t specify.</p></li>
</ul>
<p>As an example let’s add <code>wai.gi</code>, waist girth, to our model. There are numerous ways we could do this, but let’s look at adding it as a main (linear) effect as well as through an interaction with <code>sex</code>: <span class="math display">\[
\begin{aligned}
\text{weight}_i &amp;= \beta_0 + \beta_1 \text{height}_i + \beta_2 \text{male}_i +
\beta_3\text{wai.gi}_i  + \\ 
&amp; \beta_4\left(\text{height}_i \times \text{male}_i\right)  + \beta_5\left(\text{wai.gi}_i \times \text{male}_i\right)  + \epsilon_i, \quad i = 1, \ldots, 507
\end{aligned}
\]</span></p>
<pre class="r"><code>fit_wai &lt;- lm(wgt ~ hgt*sex + wai.gi*sex, data = bdims)</code></pre>
<p>We’ve now got a model with three variables and five parameters. First we need to make sure our data grid contains value for all three variables (notice I’m specifying the sequences a little differently),</p>
<pre class="r"><code>(grid_wai &lt;- data_grid(bdims,
  hgt = seq_range(hgt, n = 15, pretty = TRUE),
  wai.gi = seq_range(wai.gi, n = 5, pretty = TRUE), 
  sex
))</code></pre>
<pre><code>## # A tibble: 192 x 3
##      hgt wai.gi sex   
##    &lt;int&gt;  &lt;int&gt; &lt;fct&gt; 
##  1   145     50 female
##  2   145     50 male  
##  3   145     60 female
##  4   145     60 male  
##  5   145     70 female
##  6   145     70 male  
##  7   145     80 female
##  8   145     80 male  
##  9   145     90 female
## 10   145     90 male  
## # ... with 182 more rows</code></pre>
<p>Then we can add predictions as per usual</p>
<pre class="r"><code>pred_wai &lt;- grid_wai %&gt;%  
  add_predictions(fit_wai)</code></pre>
<p>A useful way to start, is to fix one of the values, let’s look at holding height constant at 170cm, which we can easily do by filtering the prediction grid, and looking at the predicted weight as a function of waist girth:</p>
<pre class="r"><code>pred_wai %&gt;% 
  filter(hgt == 170) %&gt;% 
  ggplot(aes(wai.gi, pred, color = sex)) +
    geom_line() +
  labs(title = &quot;Height = 170cm&quot;)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-27-1.png" width="576" /></p>
<p>For someone of height 170cm, we can see this model shows increasing waist girth is associated with increasing mean weight (lines have positive slope), and that for females the increase is slightly larger per unit increase in waist girth (female line has steeper slope).</p>
<p>But we only looked at one value of height, we could now repeat this view for our grid of heights:</p>
<pre class="r"><code>ggplot(pred_wai, aes(wai.gi, pred, color = sex)) +
  geom_line() +
  facet_wrap(~ hgt, labeller = label_both)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-28-1.png" width="576" /></p>
<p>And we see that increasing waist girth is associated with increasing mean weight for all heights, and that for females the increase is slightly larger per unit increase in waist girth for all heights. It’s a little harder to see if the slopes are changing over the different heights. But here’s an alternate view:</p>
<pre class="r"><code>ggplot(pred_wai, aes(wai.gi, pred, color = hgt)) +
  geom_line(aes(group = hgt)) +
  facet_wrap(~ sex)</code></pre>
<p><img src="/lab/lab-4_files/figure-html/unnamed-chunk-29-1.png" width="576" /></p>
<p>Now it’s easier to see the slope between the predicted weight and waist girth is the same for all heights (the lines are parallel within gender).</p>
<p><strong>Your turn</strong>: Fit a model where there is also an interaction between waist girth and height and repeat the above plots.</p>
</div>
<div id="wrapping-up" class="section level2">
<h2>Wrapping up</h2>
<ul>
<li><p>Combining the functions <code>data_grid()</code> and <code>add_predictions()</code> is a useful way to visualize a model.</p></li>
<li><p>Residuals are a useful way to look for things our model fails to capture.</p></li>
</ul>
</div>
