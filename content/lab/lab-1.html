---
title: Lab 1
date: "2019-01-09"
weeks: "1"
weeks_weight: 2
types: "lab"
---



<div id="learning-objectives" class="section level2">
<h2>Learning Objectives</h2>
<p>For all homeworks in this class I’ll expect you to submit a PDF that has been “Knit” from an <strong>Rmarkdown</strong> document. This ensures your work has a minimal level of reproducibility and that the TA and I can see exactly how you reached your answers. It’s also a great skill to learn and used extensively in industry.</p>
<p>By the end of today’s lab, you should be able to:</p>
<ul>
<li>Navigate to the class workspace on RStudio cloud and make a copy of a project</li>
<li>“Knit” an Rmarkdown document to PDF</li>
<li>Combine code, results, plots, tables, math and narrative in an Rmarkdown document</li>
<li>Debug common rendering errors</li>
</ul>
</div>
<div id="rstudio.cloud" class="section level2">
<h2>rstudio.cloud</h2>
<p>rstudio.cloud is a web-based version of R and RStudio. Today you’ll start from a project I’ve created, but by the end you will have created your own project ready for Homework 1.</p>
<div id="getting-started" class="section level3">
<h3>Getting started</h3>
<p>The TA will walk you through these steps to get started.</p>
<ol start="0" style="list-style-type: decimal">
<li><p>Get a free rstudio.cloud account, by signing up at <a href="https://rstudio.cloud/">rstudio.cloud</a>.</p></li>
<li><p>Join the class workspace by following this link: <a href="https://rstudio.cloud/spaces/7804/join?access_code=ftD%2FVMvYR1HRsRFQ%2Fwdczrh9csCpaGWfH5%2Fzvu3n">ST552 Winter 2019</a>.</p></li>
<li><p>Click on the project “Lab 1” to open it.</p></li>
<li><p>You’ll see a flashing “Temporary Copy” sign, save a copy to get your own version of this project. You only need to do this once, then you’ll be able to re-access your version from the class workspace.</p></li>
</ol>
<p>You are now in your own copy of the project, and it should look much like RStudio where you have used it before. I’ve included some files in this project, you should see them in the Files pane.</p>
<p><a href="#why"><em>(Why are we using rstudio.cloud?)</em></a></p>
</div>
</div>
<div id="rmarkdown" class="section level2">
<h2>Rmarkdown</h2>
<p>We’ll use <a href="http://rmarkdown.rstudio.com/">Rmarkdown</a> to generate our reports. Rmarkdown files combine markdown (a kind of plain text markup language) with chunks of R code. When compiled, the R code in the file is evaluated, then the results are woven into the markdown. Markdown is flexible enough that it can then be turned into a pdf (via LaTeX), a Word document, or an html file (for hosting on the web for instance, like this lab!).</p>
<p>Let’s learn by example. The TA will walk you through this part:</p>
<ol style="list-style-type: decimal">
<li><p>Open <code>01-getting-started-rmarkdown.Rmd</code></p></li>
<li><p>Skim through the file. This is just a plain text file with some special syntax. TA will point out:</p>
<ul>
<li><p>YAML header</p>
<pre><code>---
# Some options that control the output
---</code></pre></li>
<li><p>code chunks</p>
<pre><code>```{r}
# R CODE HERE

```</code></pre></li>
<li><p>Markdown syntax in the plain text parts, e.g. <code>## Section Headings</code> and <code>`code`</code>.</p></li>
</ul></li>
<li><p>Hit the Knit button to “Knit” this Rmarkdown to PDF. All going well a PDF file will appear (you’ll also find it in the Files pane as <code>01-getting-started-rmarkdown.pdf</code>). Give it a quick skim. TA will point out:</p>
<ul>
<li>code chunks are “echoed”,</li>
<li>the results from code chunks are included shown after the code</li>
<li>… same for figures</li>
<li>the special Markdown syntax has generated formatting like sections and text that looks like code.</li>
</ul></li>
<li><p>You don’t have to “Knit” a document to see the results of running code, if your cursor is inside a code chunk, you can hit Crtl/Cmd + Enter to run the current line, or Crtl/Cmd + Shift + Enter to run the whole chunk. In fact, this is usually how you work as you build an Rmarkdown document.</p></li>
</ol>
<p><strong>Your turn</strong> Make some changes in the Rmarkdown re-Knit and check the changes propagated. You might like to:</p>
<ul>
<li>Change the author to your name</li>
<li>Add some text outside a code chunk</li>
<li>Change the labels in a plot</li>
</ul>
</div>
<div id="debugging-problems-with-knitting" class="section level2">
<h2>Debugging problems with Knitting</h2>
<p>Errors when you Knit can be a little hard to track down because the error could be a code error, or an error from converting to the output you’ve chosen. If you run into an error, a good first step is to rule out code errors.</p>
<p>Rule out code errors, by going to Run -&gt; “Restart R and Run All Chunks”. Any code errors should raise their heads and you can fix them before trying to Knit again.</p>
<p>The code in an Rmarkdown document is run in a clean R session, so the most common mistakes are forgetting to load a package, or relying on objects you have in your workspace but that aren’t defined in your document.</p>
<p><strong>Your Turn</strong> Open <code>02-errors.Rmd</code> and Knit it. You’ll get an error, try the strategy outlined above to track down the error, fix it, and re-Knit. <em>(There’s more than one problem you you might need to iterate through this process)</em>.</p>
</div>
<div id="starting-from-scratch" class="section level2">
<h2>Starting from scratch</h2>
<p>Let’s get you ready to embark on a new Rmarkdown file:</p>
<ol style="list-style-type: decimal">
<li><p>Create a new Rmarkdown file: File -&gt; New File -&gt; R markdown. Provide a title (how about “Homework 1”?), your name as the author, and select PDF output. A new file will open with some contents designed to remind you how to use Rmarkdown. Save it with an informative name.</p></li>
<li><p>You probably don’t want this template contents, so delete everything below the header, and start a new section called <code>Q1</code>.</p></li>
<li><p>You can easily insert a code chunk with the shortcut Ctrl/Cmd + Alt/Option + I. Then you can write R code inside it. How about loading the <code>faraway</code> package?</p></li>
<li><p>Take a look at <a href="https://oregonstate.instructure.com/courses/1711326/assignments/7458672">Homework 1</a>. This first task is to complete an initial analysis of the <code>pima</code> dataset. Perhaps you might start with looking at a few values. Create a new code chunk, and include:</p>
<pre class="r"><code>head(pima)</code></pre>
<p>Practice running the code with a the keyboard shortcut. What do you notice about the data? You’ll probably find looking at <code>?pima</code> useful. You might make a few notes to yourself in the Rmarkdown.</p></li>
<li><p>It’s a good time to check our file still Knits. Knit it to check.</p></li>
</ol>
<p>And that’s basically how you proceed to complete your homework…</p>
</div>
<div id="looking-ahead" class="section level2">
<h2>Looking ahead</h2>
<p>Sometimes I’ll provide you an entire project, like I did today, because I can make sure you have the right packages and provide some files. But, you’ll also want to maintain your own projects for submitting homeworks.</p>
<p>The easiest approach to begin with, is to create a new project in the <strong>class workspace</strong> (this ensures you’ll have all the needed packages pre-installed), and do all your homeworks inside as individual Rmarkdown documents. You can use folders inside a project to help organise things.</p>
<p>If you create a project in your private workspace, you will have to install the packages you need.</p>
<hr />
<div id="why" class="section level3">
<h3>Why are we using rstudio.cloud?</h3>
<p>Because it makes my life easier:</p>
<ul>
<li>I can easily pre-install packages for you</li>
<li>I can deliver entire collections of files to you</li>
<li>You’ll all be working in the same environment</li>
</ul>
<p>But hopefully it also makes your life easier:</p>
<ul>
<li>You don’t have to manage an install on your computer of R, RStudio and latex.</li>
<li>You can access your work and pick up where you left off from any computer with internet access.</li>
</ul>
<p>It does have some downsides:</p>
<ul>
<li>If you don’t have internet, or for some reason the cloud goes down, you can’t access your work</li>
<li>You have to manage getting files on and off the cloud</li>
</ul>
<p>You are welcome to use your own local install of R and RStudio but getting everything you need installed will be up to you.</p>
</div>
</div>
