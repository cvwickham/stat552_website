---
title: Lab 7 - Diagnostics
date: "2019-02-19"
weeks: "7"
weeks_weight: 2
types: "lab"
no_pdf: true
links:
  - [lab/lab-7-solutions.pdf, "Some solutions"]
output:
  blogdown::html_page:
    df_print: tibble
---



<pre class="r"><code>library(tidyverse)
library(broom)</code></pre>
<pre class="r"><code>data(gala, package = &quot;faraway&quot;)

fit_gala &lt;- lm(Species ~ Area + Elevation + Nearest + Scruz + Adjacent,
  data = gala)</code></pre>
<div id="residual-plots" class="section level2">
<h2>Residual plots</h2>
<p>We could pull out things from the model “by hand”:</p>
<pre class="r"><code>residuals(fit_gala)
fitted(fit_gala)</code></pre>
<p>But I prefer to keep the residuals and fitted values lined up together with the data. One way to do this is with the <code>augment()</code> function in the broom package. The first argument is a model, the <code>data</code> argument is optional but I’d usually supply it to ensure you get column that were in the data, but not necessarily in the model:</p>
<pre class="r"><code>augment(fit_gala, data = gala)</code></pre>
<pre><code>## # A tibble: 30 x 15
##    .rownames Species Endemics  Area Elevation Nearest Scruz Adjacent
##    &lt;chr&gt;       &lt;dbl&gt;    &lt;dbl&gt; &lt;dbl&gt;     &lt;dbl&gt;   &lt;dbl&gt; &lt;dbl&gt;    &lt;dbl&gt;
##  1 Baltra         58       23 25.1        346     0.6   0.6     1.84
##  2 Bartolome      31       21  1.24       109     0.6  26.3   572.  
##  3 Caldwell        3        3  0.21       114     2.8  58.7     0.78
##  4 Champion       25        9  0.1         46     1.9  47.4     0.18
##  5 Coamano         2        1  0.05        77     1.9   1.9   904.  
##  6 Daphne.M…      18       11  0.34       119     8     8       1.84
##  7 Daphne.M…      24        0  0.08        93     6    12       0.34
##  8 Darwin         10        7  2.33       168    34.1 290.      2.85
##  9 Eden            8        4  0.03        71     0.4   0.4    18.0 
## 10 Enderby         2        2  0.18       112     2.6  50.2     0.1 
## # … with 20 more rows, and 7 more variables: .fitted &lt;dbl&gt;, .se.fit &lt;dbl&gt;,
## #   .resid &lt;dbl&gt;, .hat &lt;dbl&gt;, .sigma &lt;dbl&gt;, .cooksd &lt;dbl&gt;,
## #   .std.resid &lt;dbl&gt;</code></pre>
<p>Check out all the new columns: <code>.fitted</code>, <code>.se.fit</code>, <code>.resid</code>, <code>.sigma</code>, <code>.cooksd</code>, <code>.std.resid</code>. Take a look at the “Value” section in <code>?augment.lm</code> to find out what they are.</p>
<p>We’ll save the result so we can use it to produce plots:</p>
<pre class="r"><code>gala_diags &lt;- augment(fit_gala, data = gala)</code></pre>
<p>Then we can use our usual plotting techniques to take a look.</p>
<ul>
<li><p>Residuals plotted against the fitted values:</p>
<pre class="r"><code>ggplot(gala_diags, aes(.fitted, .resid)) +
  geom_hline(yintercept = 0, size = 2, color = &quot;white&quot;) +
  geom_point()</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-6-1.png" width="576" /></p>
<p>The <code>geom_hline()</code> call is adding a horizontal reference line at zero to help your eye look for patterns. You don’t want this reference line to overpower the data, so here I made it white.</p></li>
<li><p>Residuals against the various explanatory variables:</p>
<pre class="r"><code>ggplot(gala_diags, aes(Area, .resid)) +
  geom_hline(yintercept = 0, size = 2, color = &quot;white&quot;) +
  geom_point()</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-7-1.png" width="576" /></p>
<pre class="r"><code>ggplot(gala_diags, aes(Elevation, .resid)) +
  geom_hline(yintercept = 0, size = 2, color = &quot;white&quot;) +
  geom_point()</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-7-2.png" width="576" /></p>
<pre class="r"><code>ggplot(gala_diags, aes(Nearest, .resid)) +
  geom_hline(yintercept = 0, size = 2, color = &quot;white&quot;) +
  geom_point()</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-7-3.png" width="576" /></p>
<pre class="r"><code>ggplot(gala_diags, aes(Scruz, .resid)) +
  geom_hline(yintercept = 0, size = 2, color = &quot;white&quot;) +
  geom_point()</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-7-4.png" width="576" /></p></li>
<li><p>A Q-Q plot of the residuals:</p>
<pre class="r"><code>ggplot(gala_diags, aes(sample = .resid)) +
  geom_qq_line() +
  geom_qq() </code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-8-1.png" width="384" /></p></li>
</ul>
<p><strong>Your turn</strong> What problems do you see? Which is the biggest problem? <em>(Remember the rough hierarchy from class)</em></p>
<p>Have you got any ideas about how to fix the problem? Try your ideas and reexamine the plots.</p>
</div>
<div id="calibrating-your-eyes" class="section level2">
<h2>Calibrating your eyes</h2>
<p>It’s useful to look at plots where the assumptions are satisfied to help learn what patterns can be attributed to sampling variability. We’ll make use of the nullabor package that helps streamline the process:</p>
<pre class="r"><code>library(nullabor)</code></pre>
<p>As we saw in class one way to train your eye is to look at plots where assumptions are satisfied. Here are 12 residual versus fitted value plots where the residuals were simulated to be i.i.d Normal:
<img src="/lab/lab-7_files/figure-html/unnamed-chunk-10-1.png" width="672" /></p>
<p>Another approach, known as a lineup, is to embed the true data among the simulated ones. Take a look at these 12 residual versus fitted value plots. In eleven panels, the residuals have been simulated i.i.d from a Normal distribution, and in the remaining panel the residuals are those from the <code>fit_gala</code>:</p>
<pre><code>## decrypt(&quot;ESh1 N8L8 JR t7pJLJ7R eT&quot;)</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-11-1.png" width="672" /></p>
<p><strong>Can you spot the true data?</strong> If you want to check your guess copy and paste the <code>decrypt(..)</code> output into R. If the true data stands out, that is evidence the pattern you see in the residuals is beyond that which might be attributable to sampling variation.</p>
<p><strong>Your turn</strong> Consider a square root transform for the response <code>Species</code>:</p>
<pre class="r"><code>fit_gala_sqrt &lt;- lm(sqrt(Species) ~ Area + Elevation + Nearest + Scruz + Adjacent,
  data = gala)
gala_sqrt_diag &lt;- augment(fit_gala_sqrt, data = gala)</code></pre>
<p>Try conducting a lineup to see if this fixes the undesirable patterns seen in the residual versus fitted value plot.</p>
<p>Does it also fix problems in the Q-Q plot?</p>
</div>
<div id="practice" class="section level2">
<h2>Practice</h2>
<p>Examine the residual plots for the following 3 model fits. Can you fix any problems you find?</p>
<pre class="r"><code>data(cheddar, package = &quot;faraway&quot;)
fit_cheddar &lt;- lm(taste ~ Acetic + H2S + Lactic, data = cheddar)</code></pre>
<pre class="r"><code>data(teengamb, package = &quot;faraway&quot;)
fit_teen &lt;- lm(gamble ~ income + status + income, data = teengamb)
# I purposely left out sex from this model. Can you tell if that is a
# problem from just examing the residual plots?</code></pre>
<pre class="r"><code>data(cornnit, package = &quot;faraway&quot;)
fit_corn &lt;- lm(yield ~ nitrogen, data = cornnit)</code></pre>
</div>
<div id="case-influence-statistics" class="section level2">
<h2>Case influence statistics</h2>
<p>Of the metrics we discussed in class, the leverage and Cook’s Distance are available as part of the output from <code>augment()</code> in the <code>.hat</code> and <code>.cooksd</code> columns respectively. So, it’s relatively easy to examine them, e.g.</p>
<pre class="r"><code>gala_sqrt_diag %&gt;% 
  arrange(desc(.hat)) %&gt;% 
  select(.rownames, .hat)</code></pre>
<pre><code>## # A tibble: 30 x 2
##    .rownames     .hat
##    &lt;chr&gt;        &lt;dbl&gt;
##  1 Isabela      0.969
##  2 Fernandina   0.950
##  3 Darwin       0.466
##  4 Genovesa     0.430
##  5 SanCristobal 0.375
##  6 Wolf         0.333
##  7 SanSalvador  0.268
##  8 Pinta        0.247
##  9 SantaCruz    0.178
## 10 Coamano      0.169
## # … with 20 more rows</code></pre>
<p>Where we see the islands of Isabela and Fernandina have highest leverage.</p>
<p>Faraway suggests a “half-Normal” plot for displaying leverage and Cook’s distance. This is a Q-Q plot of the leverages but our theoretical distribution is now a half-Normal, not a Normal. One way to achieve this in ggplot:</p>
<pre class="r"><code>qhnorm &lt;-  function(p) qnorm(0.5 + p/2)
ggplot(gala_sqrt_diag, aes(sample = .hat)) +
  stat_qq(distribution = qhnorm)</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-17-1.png" width="672" /></p>
<p>If you want the points labelled, it’s harder, here’s some code I have:</p>
<pre class="r"><code>get_theoretical &lt;- function(sample, distribution, dparams = list()){
  n &lt;- length(sample)
  quantiles &lt;- ppoints(n)
  do.call(distribution, c(list(p = quote(quantiles)), dparams))[rank(sample)]
}

library(ggrepel)
ggplot(gala_sqrt_diag, aes(x = get_theoretical(.hat, qhnorm), y = .hat)) +
  geom_point() +
  geom_text_repel(aes(label = ifelse(.hat &gt; 0.2, .rownames, NA))) +
  ylab(&quot;Leverages&quot;) + xlab(&quot;Half-Normal quantiles&quot;) </code></pre>
<pre><code>## Warning: Removed 22 rows containing missing values (geom_text_repel).</code></pre>
<p><img src="/lab/lab-7_files/figure-html/unnamed-chunk-18-1.png" width="672" /></p>
<p>For examining outliers in class we talked about <strong>Studentized</strong> Residuals. These aren’t part of the output of <code>augment.lm()</code>, but you can add them yourself:</p>
<pre class="r"><code>gala_sqrt_diag$.student.resid &lt;- rstudent(fit_gala_sqrt)</code></pre>
</div>
