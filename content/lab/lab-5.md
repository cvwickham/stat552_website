---
title: "Lab 5"
date: "2019-02-06"
weeks: "5"
no_link: true
weeks_weight: 2
types: "lab"
links:
  - ["lab/CompQ1F2015.pdf", "Q1 2015 Methods Comprehensive Exam"]
---