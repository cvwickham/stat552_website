---
title: Lab 2
date: "2019-01-16"
weeks: "2"
weeks_weight: 2
types: "lab"
---



<p>You can find this document in its raw Rmarkdown form in the <a href="https://rstudio.cloud/spaces/7804/project/167666">this project on rstudio.cloud</a>.</p>
<div id="learning-objectives" class="section level2">
<h2>Learning Objectives</h2>
<ul>
<li>Describe the differences between data frames, tibbles and matrices for storing rectangular data</li>
<li>Construct numeric matrices from data or using common structures</li>
<li>Use latex in an Rmarkdown document to typeset math</li>
<li>Perform matrix algebra in R</li>
</ul>
</div>
<div id="tibblesdata-framesmatrices" class="section level2">
<h2>Tibbles/Data frames/Matrices</h2>
<p>There are a number of ways to store rectangular (observations in rows, variables in columns) data in R. Data frames are the most common because you can have different data types in each column, e.g. most of the data examples we’ve seen in class are data frames:</p>
<pre class="r"><code>str(Sleuth3::ex0727)</code></pre>
<pre><code>## &#39;data.frame&#39;:    21 obs. of  2 variables:
##  $ Mass : num  3.33 4.62 5.43 5.73 6.12 6.29 6.45 6.51 6.65 6.75 ...
##  $ Tcell: num  0.252 0.263 0.251 0.251 0.183 0.213 0.332 0.203 0.252 0.342 ...</code></pre>
<pre class="r"><code>str(faraway::gala)</code></pre>
<pre><code>## &#39;data.frame&#39;:    30 obs. of  7 variables:
##  $ Species  : num  58 31 3 25 2 18 24 10 8 2 ...
##  $ Endemics : num  23 21 3 9 1 11 0 7 4 2 ...
##  $ Area     : num  25.09 1.24 0.21 0.1 0.05 ...
##  $ Elevation: num  346 109 114 46 77 119 93 168 71 112 ...
##  $ Nearest  : num  0.6 0.6 2.8 1.9 1.9 8 6 34.1 0.4 2.6 ...
##  $ Scruz    : num  0.6 26.3 58.7 47.4 1.9 ...
##  $ Adjacent : num  1.84 572.33 0.78 0.18 903.82 ...</code></pre>
<pre class="r"><code>str(HistData::GaltonFamilies)</code></pre>
<pre><code>## &#39;data.frame&#39;:    934 obs. of  8 variables:
##  $ family         : Factor w/ 205 levels &quot;001&quot;,&quot;002&quot;,&quot;003&quot;,..: 1 1 1 1 2 2 2 2 3 3 ...
##  $ father         : num  78.5 78.5 78.5 78.5 75.5 75.5 75.5 75.5 75 75 ...
##  $ mother         : num  67 67 67 67 66.5 66.5 66.5 66.5 64 64 ...
##  $ midparentHeight: num  75.4 75.4 75.4 75.4 73.7 ...
##  $ children       : int  4 4 4 4 4 4 4 4 2 2 ...
##  $ childNum       : int  1 2 3 4 1 2 3 4 1 2 ...
##  $ gender         : Factor w/ 2 levels &quot;female&quot;,&quot;male&quot;: 2 1 1 1 2 2 1 1 2 1 ...
##  $ childHeight    : num  73.2 69.2 69 69 73.5 72.5 65.5 65.5 71 68 ...</code></pre>
<p><strong>Your turn</strong> What data types are in the columns of <code>GaltonFamilies</code>? What other data types are common as variables in data frames in R?</p>
<p>Tibbles are a modern re-imagining of data frames, very common in the <a href="https://www.tidyverse.org/">tidyverse</a> set of R packages. One particularly useful property of tibbles is slightly nicer printing, especially of large data sets. Some built in data sets are tibbles, e.g.</p>
<pre class="r"><code>ggplot2::diamonds</code></pre>
<pre><code>## # A tibble: 53,940 x 10
##    carat cut       color clarity depth table price     x     y     z
##    &lt;dbl&gt; &lt;ord&gt;     &lt;ord&gt; &lt;ord&gt;   &lt;dbl&gt; &lt;dbl&gt; &lt;int&gt; &lt;dbl&gt; &lt;dbl&gt; &lt;dbl&gt;
##  1 0.23  Ideal     E     SI2      61.5    55   326  3.95  3.98  2.43
##  2 0.21  Premium   E     SI1      59.8    61   326  3.89  3.84  2.31
##  3 0.23  Good      E     VS1      56.9    65   327  4.05  4.07  2.31
##  4 0.290 Premium   I     VS2      62.4    58   334  4.2   4.23  2.63
##  5 0.31  Good      J     SI2      63.3    58   335  4.34  4.35  2.75
##  6 0.24  Very Good J     VVS2     62.8    57   336  3.94  3.96  2.48
##  7 0.24  Very Good I     VVS1     62.3    57   336  3.95  3.98  2.47
##  8 0.26  Very Good H     SI1      61.9    55   337  4.07  4.11  2.53
##  9 0.22  Fair      E     VS2      65.1    61   337  3.87  3.78  2.49
## 10 0.23  Very Good H     VS1      59.4    61   338  4     4.05  2.39
## # ... with 53,930 more rows</code></pre>
<p><em>(If you are working in an Rmarkdown notebook and just run the code in this chunk, you might not see any difference between the display of a data frame and a tibble, that’s because RStudio is using it’s own display for both)</em>.</p>
<p>You can also turn a data frame into a tibble with <code>as_tibble()</code>, from the tibble package which is loaded as part of the tidyverse package:</p>
<pre class="r"><code>library(tidyverse)
galton &lt;- as_tibble(HistData::GaltonFamilies)
galton</code></pre>
<pre><code>## # A tibble: 934 x 8
##    family father mother midparentHeight children childNum gender
##    &lt;fct&gt;   &lt;dbl&gt;  &lt;dbl&gt;           &lt;dbl&gt;    &lt;int&gt;    &lt;int&gt; &lt;fct&gt; 
##  1 001      78.5   67              75.4        4        1 male  
##  2 001      78.5   67              75.4        4        2 female
##  3 001      78.5   67              75.4        4        3 female
##  4 001      78.5   67              75.4        4        4 female
##  5 002      75.5   66.5            73.7        4        1 male  
##  6 002      75.5   66.5            73.7        4        2 male  
##  7 002      75.5   66.5            73.7        4        3 female
##  8 002      75.5   66.5            73.7        4        4 female
##  9 003      75     64              72.1        2        1 male  
## 10 003      75     64              72.1        2        2 female
## # ... with 924 more rows, and 1 more variable: childHeight &lt;dbl&gt;</code></pre>
<p><strong>Your turn</strong>: Describe the differences between the way a data frame and a tibble print.</p>
<p>If you are interested you can read more about tibbles, and their differences from data frames, in Section 3.6 <em>Data frames and tibbles</em> of <a href="https://adv-r.hadley.nz/vectors-chap.html#vectors-chap">Advanced R</a>.</p>
<p>While data frames and tibbles are the most useful structure for data analysis, we’ll use matrices to help us learn about linear models. In contrast to data frames, matrices in R are of only one data type. For this class, we’ll only work with matrices that have numeric entries.</p>
<p>To create a matrix from scratch you can use the <code>matrix()</code> function, passing in a vector of values and dimensions (I find it easiest to set <code>byrow = TRUE</code> and layout the values in a rectangular form), e.g.</p>
<pre class="r"><code>matrix(c(
    1, 0,
    2, 1, 
    1, 0
  ), 
  ncol = 2,
  byrow = TRUE)</code></pre>
<pre><code>##      [,1] [,2]
## [1,]    1    0
## [2,]    2    1
## [3,]    1    0</code></pre>
<p>However, most of the time in this class, columns will be based on data. You can pull the columns out (by name using <code>$</code>) and join them together with <code>cbind()</code>:</p>
<pre class="r"><code>cbind(galton$midparentHeight, galton$childHeight)</code></pre>
<p>Or subset the data, then try <code>as.matrix()</code>:</p>
<pre class="r"><code>as.matrix(galton[, c(&quot;midparentHeight&quot;, &quot;childHeight&quot;)])</code></pre>
<p><strong>But be careful</strong>, it’s up to you to make sure you end up with the desired data types.</p>
<p><strong>Your turn</strong> What kind of matrices do these two commands produce? I.e. what type of data is inside? How did they handle the fact that the <code>gender</code> column was a factor?</p>
<pre class="r"><code>cbind(galton$children, galton$gender)
as.matrix(galton[, c(&quot;children&quot;, &quot;gender&quot;)])</code></pre>
<p>Neither is probably what we want if we want to include gender in a design matrix, but we’ll talk more about that at a later time.’</p>
<p>Learning how R handles linear models is mostly about learning how R creates design matrices from formula that are specified relative to a tibble/data frame.</p>
</div>
<div id="math-in-rmarkdown" class="section level2">
<h2>Math in Rmarkdown</h2>
<p>Including math in Rmarkdown is easy, you can put latex commands inside dollar signs <code>$</code>.</p>
<p>For example, using single dollar signs, includes math inline, e.g. <span class="math inline">\(\sigma\)</span> (<code>$\sigma$</code>) includes the Greek letter sigma. Double dollar signs puts math in a <em>displayed equation</em>, e.g. <span class="math display">\[
\overline{X} = \frac{1}{n}\sum_i^n x_i
\]</span></p>
<pre><code>$$
\overline{X} = \frac{1}{n}\sum_i^n x_i
$$</code></pre>
<p>Learning all the possible latex commands is much harder. A reasonable overview can be found at this <a href="http://115.159.88.104/2017/09/23/MathJax_basic/#Basic-Syntax">MathJax Basic Tutorial</a>.</p>
<p>For any formula on the class page, you should be able to right-click -&gt; “Show Math As” -&gt; “Tex commands”, and copy and paste into your own document (remember to surround it in <code>$</code> or <code>$$</code>).</p>
<p><strong>Your turn</strong> Find an example of a model specification from last weeks notes on the website (the HTML versions, not the PDFs). Try to copy it into your own Rmarkdown document: <span class="math display">\[
\]</span></p>
</div>
<div id="matrix-algebra-in-r" class="section level2">
<h2>Matrix algebra in R</h2>
<p>Head to <a href="http://www.statmethods.net/advstats/matrix.html" class="uri">http://www.statmethods.net/advstats/matrix.html</a> to see a list of many of the matrix functions available in R, and that you’ll need to complete your homework this week.</p>
<p><strong>Your turn</strong> Skim the page and make notes on how to:</p>
<ul>
<li>multiply two matrices together,</li>
<li>create a diagonal matrix,</li>
<li>extract the diagonal from a matrix,</li>
<li>transpose a matrix</li>
<li>create a matrix full of only one value (this isn’t on that page, hint: what does <code>matrix(1, nrow = 2, ncol = 3)</code> return).</li>
<li>invert a matrix.</li>
</ul>
<p>To <strong>practice</strong> create the following matrices with as little typing as possible: (<em>You might like to look at the source for this lab to see how the math is typeset</em>) <span class="math display">\[
I_{10 \times 10} 
\]</span></p>
<p><span class="math display">\[
D = 
\begin{pmatrix}
1 &amp; 0 &amp; 0 &amp; \ldots &amp; 0\\
0 &amp; 2 &amp; 0 &amp; \ldots &amp; 0\\
0 &amp; 0 &amp; 3 &amp; \ldots &amp; 0\\
\vdots &amp; \vdots &amp; \vdots &amp; \ddots &amp; 0\\
0 &amp; 0 &amp; 0 &amp; \ldots &amp; 10
\end{pmatrix}
\]</span> <span class="math display">\[
O = \pmb{1}_{10 \times 10} \quad (\text{a } 10 \times 10 \text{ matrix full of ones})
\]</span></p>
<p><span class="math display">\[
X = \left[
\begin{matrix}
1 &amp; 1\\
1 &amp; 2 \\
1 &amp; 3 \\
\vdots &amp; \vdots \\
1 &amp; 10
\end{matrix}\right] \quad
\]</span></p>
<p>Then <strong>calculate:</strong> <span class="math display">\[
X^T, \quad D^{-1}, \text{and } X^TX
\]</span></p>
</div>
<div id="simulation-of-normal-random-variables-in-r" class="section level2">
<h2>Simulation of Normal random variables in R</h2>
<p>You probably already know this, but for completeness, to simulate a realization of <span class="math inline">\(n\)</span> independent Normal random variables with mean 0 and standard deviation 1 in R use <code>rnorm()</code>:</p>
<pre class="r"><code>n &lt;- 10 # for example
rnorm(n)</code></pre>
<pre><code>##  [1]  0.60889975 -0.19138996  0.01755698  0.09124131  0.42197315
##  [6]  1.00143668 -0.26411662  1.24313495  0.58255142 -0.89598511</code></pre>
<p><code>rnorm</code> has arguments <code>mean</code> and <code>sd</code> if you need a different mean and standard deviation.</p>
<p>Want dependence? Start with uncorrelated observations, and transform them (<a href="http://math.stackexchange.com/questions/446093/generate-correlated-normal-random-variables">check the first answer</a>) or use the function <code>rmvnorm</code> in the <code>mvtnorm</code> package.</p>
<p>We can simulate output from a linear model by combining matrix algebra with simulated errors, you’ll do this in Homework 2.</p>
</div>
