---
title: Learning Objectives
---

## Week 1 Review of simple linear regression

State the model for simple linear regression in mathematical notation.

Identify the response variable, explanatory variable, parameters, and error terms in the simple linear regression model.

Describe which parts of the linear regression model are fixed and which are random.

State the assumptions on the errors in simple linear regression both mathematically and in words.

Interpret the slope and intercept parameters in the context of a data set.

Describe how estimates of the parameters in simple linear regression are found.

Define fitted values and residuals both mathematically and in words.

Describe the properties of the least squares estimates for the intercept and slope, under moment and i.i.d assumptions on the errors.

Describe the distribution of the least squares estimates for the intercept and slope, under a Normal distribution assumption on the errors.

Construct a confidence interval for either the slope or intercept parameters.

Construct a confidence interval for either the slope or intercept parameters using `lm()` output from R.

Identify whether a confidence interval or prediction interval is more appropriate to answer a question of interest.

Define $R^2$ both mathematically and in words.

Produce a reproducible report that integrates narrative, math and code using Rmarkdown.

Identify a code style guide and rewrite code to meet it.

## Week 2 Matrix setup of multiple linear regression

State the multiple regression model in matrix form along with the assumptions on the errors and design matrix.

Describe the entries in the design matrix given a model and study description.

Derive the least squares estimates.

Define fitted values and residuals.

Describe the difference between random errors and residuals.

Derive the mean and variance-covariance matrix of the least squares estimates in multiple linear regression.

State the Gauss-Markov theorem and discuss it's consequences in practice.

State the form and properties of the estimate for the  variance of the errors.

Fit a multiple linear regression model using matrix algebra in R.

Fit a multiple linear regression model using `lm()` in R.

Describe why using `lm()` is preferable to performing the matrix algebra $\left(X^TX\right)^{-1}X^TY$.

