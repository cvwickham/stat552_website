---
title: Home
---

# ST 552

**Lectures** MWF 3-3:50pm, Weniger Hall 287   
**Labs**  Wednesdays 4-4:50pm  Kidder Hall 033  

**Instructor**: Charlotte Wickham, 255 Weniger  
[charlotte.wickham@stat.oregonstate.edu](mailto:charlotte.wickham@stat.oregonstate.edu)

**Instructor Office Hours**  
Monday & Wednesday 2-2:50pm Weniger 255

**TA** [Trevor Ruiz](mailto:Trevor.Ruiz@oregonstate.edu)  

**Textbook** [Faraway, J.J., 2015. Linear models with R](https://search.library.oregonstate.edu/primo-explore/fulldisplay?docid=CP71241609370001451&context=L&vid=OSU&search_scope=everything&tab=default_tab&lang=en_US)
